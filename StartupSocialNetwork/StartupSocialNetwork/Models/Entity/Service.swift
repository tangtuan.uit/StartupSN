//
//  Service.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/22/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

class Service: Post {
    
    var describeService: String?
    var nameStore: String?
    var localtionStore: String?
    dynamic var like: Int = 0
    dynamic var comment: Int = 0
    dynamic var share: Int = 0
    
}

extension Service {
    override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id":"_id",
            "time": "create_date",
            "location": "location",
            "type": "type",
            "describeService": "describeService",
            "nameStore": "nameStore",
            "localtionStore": "localtionStore",
            "like": "like",
            "comment": "comment",
            "share": "share"
        ]
    }
}
