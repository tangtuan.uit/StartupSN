//
//  Experience.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/22/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

class Experience: Post {
    
    dynamic var isQuestion: Bool = false
    var content: String?
    dynamic var like: Int = 0
    dynamic var comment: Int = 0
    dynamic var share: Int = 0
}

extension Experience {
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id":"_id",
            "time": "create_date",
            "location": "location",
            "type": "type",
            "isQuestion": "isQuestion",
            "content": "content",
            "like": "like",
            "comment": "comment",
            "share": "share",
        ]
    }
}
