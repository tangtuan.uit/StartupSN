//
//  User.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/3/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation
import Mantle

class User: MTLModel {
    
    var id: String?
    var name: String?
    var email: String?
    var phone: String?
    var area: String?
    dynamic var gender: Bool = true
    var state: String?
    var birthday: String?
    var image: String?
    var about: String?
    dynamic var followers: Int = 0
    dynamic var following: Int = 0
    dynamic var post: Int = 0
    
    func coverToDictionary() -> Dictionary<String, Any> {
        var result = Dictionary<String, Any>()
        if self.id != nil {
            result["id"] = self.id
        }
        if self.name != nil {
            result["name"] = self.name
        }
        if self.email != nil {
            result["email"] = self.email
        }
        if self.phone != nil {
            result["phone"] = self.phone
        }
        if self.area != nil {
            result["area"] = self.area
        }
        if self.birthday != nil {
            result["birthday"] = self.birthday
        }
        if self.about != nil {
            result["about"] = self.about
        }
        if self.image != nil {
            result["image"] = self.image
        }

        result["gender"] = self.gender
        return result
    }
    
}

extension User: MTLJSONSerializing {
    static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id": "_id",
            "name": "name",
            "email": "email",
            "phone": "phone",
            "area": "area",
            "gender": "gender",
            "image": "image",
            "state": "state",
            "about": "about",
            "followers": "followers",
            "following": "following",
            "post": "post"
        ]
    }
}
