//
//  Event.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/18/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

class Event: Post {
    
    var title: String?
    var describeEvent: String?
    var organizer: String?
    var phone: String?
    var dayStart: String?
    var dayEnd: String?
    var timeStart: String?
    var timeEnd: String?
    var locationEvent: String?
    dynamic var care: Int = 0
    var joned: [String]?
    dynamic var share: Int = 0
    
}

extension Event {
    override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id":"_id",
            "time": "create_date",
            "location": "location",
            "type": "type",
            "title": "title",
            "describeEvent": "describeEvent",
            "organizer": "organizer",
            "phone": "phone",
            "dayStart": "dayStart",
            "dayEnd": "dayEnd",
            "timeStart": "timeStart",
            "timeEnd": "timeEnd",
            "locationEvent": "locationEvent",
            "care": "care",
            "joned": "joned",
            "share": "share"
        ]
    }
}
