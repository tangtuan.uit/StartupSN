//
//  Group.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/29/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation
import Mantle

class Group: MTLModel {
    
    var id: String?
    var id_users: [String]?
    var user: User?
    var message: Message?
    var time: String?
    
}

extension Group: MTLJSONSerializing {
    static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id":"_id",
            "id_users":"users",
            "time":"update_date"
        ]
    }
}
