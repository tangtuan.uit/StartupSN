//
//  Image.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 1/21/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation
import Mantle

class Image: MTLModel {
    
    var id: String?
    var name: String?
    dynamic var width: Int = 0
    dynamic var height: Int = 0
    var path: String?
    
}

extension Image: MTLJSONSerializing {
    static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id": "_id",
            "name": "filename",
            "width": "width",
            "height": "height",
            "path": "path"
        ]
    }
}


