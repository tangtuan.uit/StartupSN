//
//  Message.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/24/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation
import Mantle

class Message: MTLModel {
    
    var id: String?
    var id_group: String?
    var id_send: String?
    var user: User?
    var content: String?
    dynamic var seen: Bool = false
    var time: String?
    
}

extension Message: MTLJSONSerializing {
    static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id": "_id",
            "id_group": "id_group",
            "id_send": "id_send",
            "content":"content",
            "seen": "seen",
            "time": "create_date"
        ]
    }
}
