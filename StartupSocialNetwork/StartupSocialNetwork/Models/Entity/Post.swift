//
//  Post.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/14/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation
import Mantle


class Post: MTLModel {
    
    var id: String?
    var user: User?
    var time: String?
    var location: String?
    var type: String?
    var images: [Image]?

}

extension Post: MTLJSONSerializing {
    
    class func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [:]
    }
    
    class func `class`(forParsingJSONDictionary JSONDictionary: [AnyHashable : Any]!) -> AnyClass! {
        if JSONDictionary["type"] as! String == "startup" {
            return Startup.self
        } else if JSONDictionary["type"] as! String == "event" {
            return Event.self
        } else if JSONDictionary["type"] as! String == "experience" {
            return Experience.self
        } else if JSONDictionary["type"] as! String == "service" {
            return Service.self
        } else if JSONDictionary["type"] as! String == "status" {
            return Status.self
        }
        return self
    }
    
}
