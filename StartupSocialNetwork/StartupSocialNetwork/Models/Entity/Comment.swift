//
//  Comment.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/19/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation
import Mantle

class Comment: MTLModel {
    
    var id: String?
    var post: String?
    var user: User?
    var content: String?
    var time: String?
}

extension Comment: MTLJSONSerializing {
    static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id": "_id",
            "post": "id_post",
            "content": "content",
            "time": "create_date"
        ]
    }
}
