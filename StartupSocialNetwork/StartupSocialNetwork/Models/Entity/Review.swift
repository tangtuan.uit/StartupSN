//
//  Review.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 5/24/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation
import Mantle

class Review: MTLModel {
    
    var id: String?
    var post: String?
    var user: User?
    var content: String?
    var time: String?
    dynamic var vote: Int = 0
    
}

extension Review: MTLJSONSerializing {
    static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id": "_id",
            "post": "id_post",
            "content": "content",
            "vote": "vote",
            "time": "create_date"
        ]
    }
}
