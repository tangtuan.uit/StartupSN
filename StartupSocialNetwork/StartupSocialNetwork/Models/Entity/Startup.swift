//
//  Startup.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/16/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

class Startup : Post {
    
    var idea: String?
    var team: String?
    var fields: String?
    var userObject: String?
    var orientedDevelopment: String?
    var describeProduct: String?
    var review: Review?
    dynamic var numberReview: Int = 0
    dynamic var like: Int = 0
    dynamic var comment: Int = 0
    dynamic var share: Int = 0
    
}

extension Startup {
    override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id":"_id",
            "time": "create_date",
            "location": "location",
            "type": "type",
            "idea": "idea",
            "team": "team",
            "fields": "fields",
            "userObject": "userObject",
            "orientedDevelopment": "orientedDevelopment",
            "describeProduct": "describeProduct",
            "numberReview": "number_review",
            "like": "like",
            "comment": "comment",
            "share": "share"
        ]
    }
}

