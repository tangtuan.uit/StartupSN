//
//  Status.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/23/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

class Status: Post {
    
    var content: String?
    dynamic var like: Int = 0
    dynamic var comment: Int = 0
    dynamic var share: Int = 0

}

extension Status {
    override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "id":"_id",
            "time": "create_date",
            "location": "location",
            "type": "type",
            "content": "content",
            "like": "like",
            "comment": "comment",
            "share": "share"
        ]
    }
}
