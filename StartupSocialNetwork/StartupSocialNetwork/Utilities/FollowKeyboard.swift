//
//  FollowKeyboard.swift
//  FollowKeyboard
//
//  Created by Danis on 16/1/21.
//  Copyright © 2016年 danis. All rights reserved.
//

import UIKit

public enum FollowKeyboardType : Equatable{
    case Show
    case Hide
}

public func ==(lhs: FollowKeyboardType, rhs: FollowKeyboardType) -> Bool {
    switch (lhs,rhs) {
    case (.Show,.Show):
        return true
    case (.Hide, .Hide):
        return true
    default:
        return false
    }
}

public typealias FollowKeyboardAnimationsBlock = (_ keyboardFrame: CGRect, _ duration: Double, _ type: FollowKeyboardType) -> Void
public typealias FollowKeyboardCompletionBlock = (_ finished: Bool) -> Void

private let AnimationsBlockKey = "FKAnimationsBlock"
private let CompletionBlockKey = "FKCompletionBlock"

public class FollowKeyboard: NSObject {
    
    private var animationsBlock: FollowKeyboardAnimationsBlock?
    private var completionBlock: FollowKeyboardCompletionBlock?
    
    public override init() {
        super.init()
    }
    
    public func followKeyboard(withAnimations animationsBlock: FollowKeyboardAnimationsBlock?, completionBlock: FollowKeyboardCompletionBlock?) {
        
        self.animationsBlock = animationsBlock
        self.completionBlock = completionBlock
        
        NotificationCenter.default.addObserver(self, selector: #selector(FollowKeyboard.onKeyboardWillShowNotification(notifcation:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FollowKeyboard.onKeyboardWillHideNotification(notifcation:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    public func unfollowKeyboard() {
        
        self.animationsBlock = nil
        self.completionBlock = nil
        
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func onKeyboardWillShowNotification(notifcation: NSNotification) {
        commitAnimationsWithNotification(notification: notifcation, type: .Show)
    }
    
    func onKeyboardWillHideNotification(notifcation: NSNotification) {
        commitAnimationsWithNotification(notification: notifcation, type: .Hide)
    }
    
    private func commitAnimationsWithNotification(notification: NSNotification, type: FollowKeyboardType) {
        let keyboardFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let curve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).intValue
        let duration = (notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        UIView.animate(withDuration: duration, delay: 0, options: [.beginFromCurrentState], animations: { () -> Void in
            UIView.setAnimationCurve(UIViewAnimationCurve(rawValue: curve)!)
            
            self.animationsBlock?(keyboardFrame, duration, type)
            
            }, completion: completionBlock)
        
    }
}
