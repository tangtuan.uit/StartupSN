//
//  PhotoCollectionLayout.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/15/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class PhotoCollectionLayout: UICollectionViewLayout {
    
    var heightCollectionView: CGFloat!
    var arrImage: [Image]!
    
    private var cache = [UICollectionViewLayoutAttributes]()
    private var contentHeight: CGFloat = 0
    private var width: CGFloat {
        get {
            return (collectionView?.bounds.width)!
        }
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: width, height: contentHeight)
    }
    
    override func prepare() {
        if cache.isEmpty {
            let sizeCollectionView = CGSize(width: width, height: self.heightCollectionView)
            let arrLayout = Helper.sharedInstance.setArrayColletionViewLayout(collectionView: sizeCollectionView, arrImage: arrImage)
            for item in 0..<collectionView!.numberOfItems(inSection: 0) {
                let indexPath = NSIndexPath(item: item, section: 0)
                let frame = CGRect(x: arrLayout[item].xOffset!, y: arrLayout[item].yOffset!, width: arrLayout[item].width!, height: arrLayout[item].height!)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath as IndexPath)
                attributes.frame = frame
                cache.append(attributes)
            }
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        
        return layoutAttributes
    }
    
}

struct PhotoLayout {
    
    var xOffset: CGFloat?
    var yOffset: CGFloat?
    var width: CGFloat?
    var height: CGFloat?
    
    init(xOffset: CGFloat, yOffset: CGFloat, width: CGFloat, height: CGFloat) {
        self.xOffset = xOffset
        self.yOffset = yOffset
        self.width = width
        self.height = height
    }
    
}
