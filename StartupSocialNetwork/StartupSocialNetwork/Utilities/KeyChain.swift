//
//  KeyChain.swift
//  Piminium
//
//  Created by Tang Tuan on 12/26/16.
//  Copyright © 2016 Pim. All rights reserved.
//

import Foundation
import UICKeyChainStore

class KeyChain {
    
    class func checkNilKeyChain() -> Bool {
        let token: String? = UICKeyChainStore.string(forKey: kAuthToken)
        let id: String? = UICKeyChainStore.string(forKey: kIdUser)
        if token == nil && id == nil {
            return true
        }
        return false
    }
    
    class func checkKeyChainStore(user: User) -> Bool {
        
        let id: String? = UICKeyChainStore.string(forKey: kIdUser)
        let token: String? = UICKeyChainStore.string(forKey: kAuthToken)
        
        if id != nil && token != nil {
            return true
        }
        return false
    }
    
    
    class func saveKeyChainStore(user: User) {
        
        UICKeyChainStore.setString(user.id, forKey: kIdUser)
//        UICKeyChainStore.setString(user.email, forKey: kEmail)

    }
    
    class func removeKeyChainStore() {
        
        UICKeyChainStore.removeItem(forKey: kIdUser)
        UICKeyChainStore.removeItem(forKey: kAuthToken)
    }
    
}
