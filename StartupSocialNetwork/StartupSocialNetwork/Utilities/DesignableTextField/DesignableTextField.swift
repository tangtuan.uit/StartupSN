//
//  DesignableTextField.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 1/19/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableTextField: UITextField {
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            self.updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            self.updateView()
        }
    }
    
    @IBInspectable var topPadding: CGFloat = 0 {
        didSet {
            self.updateView()
        }
    }
    
    @IBInspectable var leftPaddingText: CGFloat = 0 {
        didSet {
            self.updateView()
        }
    }
    
    @IBInspectable var heightBorder: CGFloat = 0 {
        didSet {
            self.updateView()
        }
    }
    
    @IBInspectable var bottomBorderColor: UIColor? {
        didSet {
            self.updateView()
        }
    }
    
    func updateView() {
        
        // set left image for text field
        if let image = self.leftImage {
            self.leftViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: self.leftPadding, y: self.topPadding, width: 18, height: 18)) // postion of image view
            imageView.image = image
            
            let width = self.leftPadding + 24  + self.leftPaddingText// set point y of text
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20)) // position of text
            view.addSubview(imageView) // add imageView into view
            self.leftView = view
        } else {
            self.leftViewMode = .never
        }
        
        // set color for placeholder
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: tintColor])
        
        // set border bottom for text field
        self.borderStyle = UITextBorderStyle.none;
        let border = CALayer()
        let width = self.heightBorder // width of border
        border.borderColor = self.bottomBorderColor?.cgColor // color of border
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

}
