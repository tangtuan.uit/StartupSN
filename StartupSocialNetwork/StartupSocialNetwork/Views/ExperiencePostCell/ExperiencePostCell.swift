//
//  ExperiencePostCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/15/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol ExperiencePostCellDelegate: class {
    func openCommentAtIndexFromExperience(indexPath: NSIndexPath)
}

class ExperiencePostCell: UICollectionViewCell {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewButton: UIView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var textViewDescribe: UITextView!
    @IBOutlet weak var labelDetail: UILabel!
    
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var heightLabelDetail: NSLayoutConstraint!
    @IBOutlet weak var bottomImage: NSLayoutConstraint!
    @IBOutlet weak var imageContent: UIImageView!
    
    var currentIndexPath: NSIndexPath!
    weak var delegate: ExperiencePostCellDelegate?
    
    override func awakeFromNib() {
        
        self.layer.cornerRadius = 5
        self.viewHeader.layer.cornerRadius = 5
        self.viewButton.layer.cornerRadius = 5
        
        self.imageAvatar.setCircularImage()
        self.contentView.isUserInteractionEnabled = false
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ExperiencePostCell.handleTextViewDescribeTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelDetail.isUserInteractionEnabled = true
        self.labelDetail.addGestureRecognizer(gestureRecognizer)
    }
    
    func setupViewExperienceWithPost(post: Post) {
        
        if post.user?.image != nil {
            let url = Helper.sharedInstance.setUrlForImage(path: (post.user?.image)!)
            Helper.sharedInstance.displayImageWithURL(imageView: self.imageAvatar, url: url)
        } else {
            self.imageAvatar.image = #imageLiteral(resourceName: "default_avatar")
        }
        
        let postExperience = post as! Experience
        let height = Helper.sharedInstance.getHeightTextForPost(width: self.frame.width - 16, text: "Kinh nghiệm\n" + postExperience.content!, sizeFont: 14)
        
        if postExperience.images == nil {
            self.bottomImage.constant = 0
            self.heightImage.constant = 0
            self.labelContent.numberOfLines = 14
            if height < 238 {
                self.heightLabelDetail.constant = 0
            } else {
                self.heightLabelDetail.constant = 17
            }
        } else {
            
            let url = Helper.sharedInstance.setUrlForImage(path: (postExperience.images?[0].path!)!)
            Helper.sharedInstance.displayImageWithURL(imageView: self.imageContent, url: url)
            
            self.bottomImage.constant = 8
            self.labelContent.numberOfLines = 8
            if height < 136 {
                self.heightLabelDetail.constant = 0
            } else {
                self.heightLabelDetail.constant = 17
            }
        }
        
        let date = Helper.sharedInstance.stringToDate(date: postExperience.time!)
        let time = Helper.sharedInstance.getTimePost(date: date)
        
        self.labelName.setUsernamForStartupPost(username: (postExperience.user?.name)!, time: time)
        self.labelContent.setTextForTextViewExperience(post: postExperience)
    }
    
    func handleTextViewDescribeTap() {
        print(1234)
    }
    
    @IBAction func handleComment(_ sender: Any) {
        if self.delegate != nil && self.currentIndexPath != nil {
            self.delegate!.openCommentAtIndexFromExperience(indexPath: self.currentIndexPath)
        }
    }

}
