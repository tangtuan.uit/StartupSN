//
//  ChatBubbleViewCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/24/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class ChatBubbleViewCell: UICollectionViewCell {
    
    let textViewMessage: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.textColor = UIColor.black
        textView.backgroundColor = UIColor.clear
        textView.numberOfLines = 0
//        textView.isEditable = false
//        textView.isScrollEnabled = false
        return textView
    }()
    
    let textBubbleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.textBubbleView)
        self.addSubview(self.textViewMessage)
        self.addSubview(self.profileImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
