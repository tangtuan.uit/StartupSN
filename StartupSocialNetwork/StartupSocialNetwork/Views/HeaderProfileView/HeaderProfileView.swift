//
//  HeaderProfileView.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/1/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol HeaderProfileViewDelegate: class {
    func openUpdateProfileViewController()
}

class HeaderProfileView: UICollectionReusableView {
    
    @IBOutlet weak var imageViewCover: UIImageView!
    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var labelFollowers: UILabel!
    @IBOutlet weak var labelFollowing: UILabel!
    @IBOutlet weak var labelShares: UILabel!
    @IBOutlet weak var buttonEditProfile: UIButton!
    
    @IBOutlet weak var labelName: UILabel!
    
    weak var delegate: HeaderProfileViewDelegate?
    
    override func awakeFromNib() {
        
        self.imageViewCover.addBlurEffect()
        self.imageViewAvatar.setCircularAvatar()
        
        self.labelFollowers.setTextForLabelHeaderView(number: 0, text: "Người quan tâm")
        self.labelFollowing.setTextForLabelHeaderView(number: 0, text: "Đang quan tâm")
        self.labelShares.setTextForLabelHeaderView(number: 0, text: "Lượt đăng bài")
        
        self.buttonEditProfile.addAllBorder(color: .white, radius: 5, borderWidth: 2)
    }
    
    func setupHeaderForUser(user: User) {
        
        if user.image != nil {
            
            DispatchQueue.global(qos: .background).async {
                let url = Helper.sharedInstance.setUrlForImage(path: user.image!)
                DispatchQueue.main.async {
                    self.imageViewAvatar.sd_setImage(with: url)
                    self.imageViewCover.sd_setImage(with: url)
                }
            }

        }

        self.labelName.text = user.name
        
        self.labelFollowers.setTextForLabelHeaderView(number: user.followers, text: "Người quan tâm")
        self.labelFollowing.setTextForLabelHeaderView(number: user.following, text: "Đang quan tâm")
        self.labelShares.setTextForLabelHeaderView(number: user.post, text: "Lượt đăng bài")
    }
    
    @IBAction func handleEditProfile(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.openUpdateProfileViewController()
        }
    }
    
    
}
