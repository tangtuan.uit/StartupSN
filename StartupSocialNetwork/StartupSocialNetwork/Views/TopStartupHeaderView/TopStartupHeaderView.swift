//
//  TopStartupHeaderView.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 7/6/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import Charts

class TopStartupHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var barChartView: BarChartView!
    
    var months: [String]!
    
    override func awakeFromNib() {
        months = ["Jan", "Feb", "Mar", "Apr", "May"]
        let unitsSold = [13.0, 8.0, 5.0, 4.0, 4.0]
        
        setChart(dataPoints: months, values: unitsSold)
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        barChartView.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: Double(values[i]) )
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Top 5 dự án")
        chartDataSet.colors = [UIColor(red: 51/255, green: 102/255, blue: 102/255, alpha: 1)]
        let chartData = BarChartData(dataSet: chartDataSet)
        barChartView.data = chartData
        barChartView.chartDescription?.text = ""
    }
    
}
