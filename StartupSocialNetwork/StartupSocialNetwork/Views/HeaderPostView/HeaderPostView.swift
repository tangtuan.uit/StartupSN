//
//  HeaderPostView.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/10/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class HeaderPostView: UICollectionReusableView {
    
    let viewHeader: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        return view
    }()
    
    let imageFollow: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "follow")
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.viewHeader)
        
        self.viewHeader.translatesAutoresizingMaskIntoConstraints = false
        
        self.addConstraint(NSLayoutConstraint(item: self.viewHeader, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leadingMargin, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self.viewHeader, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailingMargin, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self.viewHeader, attribute: .top, relatedBy: .equal, toItem: self, attribute: .topMargin, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self.viewHeader, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottomMargin, multiplier: 1, constant: 8))
        
        self.viewHeader.addSubview(self.imageFollow)
        
        self.imageFollow.translatesAutoresizingMaskIntoConstraints = false
        
        self.addConstraint(NSLayoutConstraint(item: self.imageFollow, attribute: .leading, relatedBy: .equal, toItem: self.viewHeader, attribute: .leadingMargin, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self.imageFollow, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 54))
        self.addConstraint(NSLayoutConstraint(item: self.imageFollow, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 70))
        self.addConstraint(NSLayoutConstraint(item: self.imageFollow, attribute: .centerY, relatedBy: .equal, toItem: self.viewHeader, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
