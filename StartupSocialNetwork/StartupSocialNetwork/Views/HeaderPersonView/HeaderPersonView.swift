//
//  HeaderPersonView.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 5/5/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol HeaderPersonViewDelegate: class {
    func openChatViewController(user: User)
}

class HeaderPersonView: UICollectionReusableView {
    
    @IBOutlet weak var imageViewCover: UIImageView!
    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var labelFollowers: UILabel!
    @IBOutlet weak var labelFollowing: UILabel!
    @IBOutlet weak var labelShares: UILabel!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var buttonFollow: UIButton!
    
    weak var delegate: HeaderPersonViewDelegate?
    
    var idUser: String!
    var idFollow: String = ""
    var user: User!
    
    override func awakeFromNib() {
        
        self.imageViewCover.addBlurEffect()
        self.imageViewAvatar.setCircularAvatar()
        
        self.labelFollowers.setTextForLabelHeaderView(number: 0, text: "Người quan tâm")
        self.labelFollowing.setTextForLabelHeaderView(number: 0, text: "Đang quan tâm")
        self.labelShares.setTextForLabelHeaderView(number: 0, text: "Lượt đăng bài")
        
//        self.buttonFollow.addAllBorder(color: .white, radius: 5, borderWidth: 2)
        self.buttonFollow.layer.cornerRadius = 5
    }

    func setupHeaderForUser(user: User, follow: String) {
        
        self.user = user
        self.idUser = user.id
        self.idFollow = follow
        
        if user.image != nil {
            self.imageViewAvatar.sd_setImage(with: Helper.sharedInstance.setUrlForImage(path: user.image!))
            self.imageViewCover.sd_setImage(with: Helper.sharedInstance.setUrlForImage(path: user.image!))
        }
        self.labelName.text = user.name
        self.setTextFollowUser()
        
        self.setupButtonFollow(follow: self.idFollow)
    }
    
    @IBAction func handleButtonFollow(_ sender: Any) {
        if !self.idFollow.isEmpty {
            //unfollow
            let id = self.idFollow
            self.idFollow = ""
            self.setupButtonFollow(follow: self.idFollow)
            APIClient.sharedInstance.unfollow(id, user_current!, self.user, completionHandle: { (success) in
                if !success {
                    self.idFollow = id
                    self.setupButtonFollow(follow: self.idFollow)
                } else {
                    user_current?.following -= 1
                    self.user.followers -= 1
                }
                self.setTextFollowUser()
            }, failure: { (error) in
                self.setupButtonFollow(follow: self.idFollow)
            })
        } else {
            //follow
            self.idFollow = "follow"
            self.setupButtonFollow(follow: self.idFollow)
            APIClient.sharedInstance.follow(user_current!, self.user, completionHandle: { (success, id) in
                if success {
                    self.idFollow = id
                    self.setupButtonFollow(follow: self.idFollow)
                    user_current?.following += 1
                    self.user.followers += 1
                } else {
                    self.idFollow = ""
                }
                self.setTextFollowUser()
            }, failure: { (error) in
                self.idFollow = ""
                self.setupButtonFollow(follow: self.idFollow)
            })
        }
    }
    
    func setTextFollowUser() {
        self.labelFollowers.setTextForLabelHeaderView(number: self.user.followers, text: "Người quan tâm")
        self.labelFollowing.setTextForLabelHeaderView(number: self.user.following, text: "Đang quan tâm")
        self.labelShares.setTextForLabelHeaderView(number: self.user.post, text: "Lượt đăng bài")
    }
    
    func setupButtonFollow(follow: String) {
        if !self.idFollow.isEmpty {
            self.buttonFollow.backgroundColor = UIColor(red: 91/255, green: 212/255, blue: 54/255, alpha: 1)
            self.buttonFollow.setTitle("Đang quan tâm", for: .normal)
        } else {
            self.buttonFollow.backgroundColor = UIColor(red: 0/255, green: 153/255, blue: 204/255, alpha: 1)
            self.buttonFollow.setTitle("Quan tâm", for: .normal)
        }
    }
    
    @IBAction func handleSentMessage(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.openChatViewController(user: self.user)
        }
    }
}
