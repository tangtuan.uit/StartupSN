//
//  StartupViewInput.swift
//  TestViewPost
//
//  Created by Tang Tuan on 3/29/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol StartupViewInputDelegate: class {
    func openInputViewControllerFromStartup(type: String)
    func openImagePickerViewController()
    func deleteImagePost(index: Int)
}

class StartupViewInput: UICollectionViewCell {

    @IBOutlet weak var collectionViewImage: UICollectionView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    
    @IBOutlet weak var labelTarget: PaddingLabel!

    @IBOutlet weak var labelIdea: PaddingLabel!
    @IBOutlet weak var labelTeam: PaddingLabel!
    @IBOutlet weak var labelFields: PaddingLabel!
    @IBOutlet weak var labelUser: PaddingLabel!
    @IBOutlet weak var labelDevelopment: PaddingLabel!
    @IBOutlet weak var labelProduct: PaddingLabel!
    
    weak var delegate: StartupViewInputDelegate?
    
    var arrImage: [UIImage] = [UIImage]()

    override func awakeFromNib() {

        self.collectionViewImage.delegate = self
        self.collectionViewImage.dataSource = self

        self.contentView.isUserInteractionEnabled = false
        
        let nibImage = UINib(nibName: "ImageCell", bundle: nil)
        self.collectionViewImage.register(nibImage, forCellWithReuseIdentifier: "ImageCell")
        
        self.heightCollectionView.constant = ((self.frame.width - 16) - 12) / 4
        
        let nibInput = UINib(nibName: "InputImage", bundle: nil)
        self.collectionViewImage.register(nibInput, forCellWithReuseIdentifier: "InputCell")
        
        self.arrImage.append(#imageLiteral(resourceName: "picture"))
        
        self.labelTarget.text =  "Hình ảnh sản phẩm chi tiết góp phần làm tăng cơ hội tiếp cận các nhà đầu tư"
        
        self.setupLabelIdea()
        self.setupLabelTeam()
        self.setupLabelFields()
        self.setupLabelUser()
        self.setupLabelDevelopment()
        self.setupLabelProduct()
    }
    
    func setTextForStartupPost(post: Startup) {
        Helper.sharedInstance.setTextForFieldStartup(text: post.idea, title: kTitleIdea, colorTitle: .darkGray, label: self.labelIdea)
        Helper.sharedInstance.setTextForFieldStartup(text: post.team, title: kTitleTeam, colorTitle: .darkGray, label: self.labelTeam)
        Helper.sharedInstance.setTextForFieldStartup(text: post.fields, title: kTitleFields, colorTitle: .darkGray, label: self.labelFields)
        Helper.sharedInstance.setTextForFieldStartup(text: post.userObject, title: kTitleUser, colorTitle: .darkGray, label: self.labelUser)
        Helper.sharedInstance.setTextForFieldStartup(text: post.orientedDevelopment, title: kTitleDevelopment,  colorTitle: .darkGray, label: self.labelDevelopment)
        Helper.sharedInstance.setTextForFieldStartup(text: post.describeProduct, title: kTitleProduct,  colorTitle: .darkGray, label: self.labelProduct)
    }
    
    func setImageForPost(images: [UIImage]) {
        self.arrImage = images
        self.collectionViewImage.reloadData()
        
        let indexPath = NSIndexPath(item: self.arrImage.count, section: 0)
        if self.arrImage.count > 0 {
            self.collectionViewImage.scrollToItem(at: indexPath as IndexPath, at: .right, animated: true)
        }        
    }

}

extension StartupViewInput: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImage.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = ((self.frame.width - 16) - 12) / 4
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item == self.arrImage.count {
            let cell = self.collectionViewImage.dequeueReusableCell(withReuseIdentifier: "InputCell", for: indexPath) as! InputImage
            cell.delegate = self
            return cell
        } else {
            let cell = self.collectionViewImage.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
            cell.delegate = self
            cell.currentIndexPath = indexPath as NSIndexPath!
            cell.setuoImageForCell(image: self.arrImage[indexPath.item])
            return cell
        }
    }
}

extension StartupViewInput: InputImageDelegate {
    func openImagePickerViewController() {
        self.delegate?.openImagePickerViewController()
    }
}

extension StartupViewInput: ImageCellDelegate {
    func deleteImage(indexPath: NSIndexPath) {
        self.delegate?.deleteImagePost(index: indexPath.row)
    }
}

extension StartupViewInput {
    
    func setupLabelIdea() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StartupViewInput.handleLabelIdeaTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelIdea.isUserInteractionEnabled = true
        self.labelIdea.addGestureRecognizer(gestureRecognizer)
        
    }
    
    func setupLabelTeam() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StartupViewInput.handleLabelTeamTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelTeam.isUserInteractionEnabled = true
        self.labelTeam.addGestureRecognizer(gestureRecognizer)
        
    }
    
    func setupLabelFields() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StartupViewInput.handleLabelFieldsTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelFields.isUserInteractionEnabled = true
        self.labelFields.addGestureRecognizer(gestureRecognizer)
        
    }
    
    func setupLabelUser() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StartupViewInput.handleLabelUserTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelUser.isUserInteractionEnabled = true
        self.labelUser.addGestureRecognizer(gestureRecognizer)
        
    }
    
    func setupLabelDevelopment() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StartupViewInput.handleLabelDevelopmentTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelDevelopment.isUserInteractionEnabled = true
        self.labelDevelopment.addGestureRecognizer(gestureRecognizer)
        
    }
    
    func setupLabelProduct() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StartupViewInput.handleLabelProductTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelProduct.isUserInteractionEnabled = true
        self.labelProduct.addGestureRecognizer(gestureRecognizer)
        
    }
}

extension StartupViewInput {
    
    func handleLabelIdeaTap() {
        if self.delegate != nil {
            self.delegate?.openInputViewControllerFromStartup(type: kSUIdea)
        }
    }
    
    func handleLabelTeamTap() {
        if self.delegate != nil {
            self.delegate?.openInputViewControllerFromStartup(type: kSUTeam)
        }
    }
    
    func handleLabelFieldsTap() {
        if self.delegate != nil {
            self.delegate?.openInputViewControllerFromStartup(type: kSUFields)
        }
    }
    
    func handleLabelDevelopmentTap() {
        if self.delegate != nil {
            self.delegate?.openInputViewControllerFromStartup(type: kSUDevelopment)
        }
    }
    
    func handleLabelUserTap() {
        if self.delegate != nil {
            self.delegate?.openInputViewControllerFromStartup(type: kSUUser)
        }
    }
    
    func handleLabelProductTap() {
        if self.delegate != nil {
            self.delegate?.openInputViewControllerFromStartup(type: kSUProduct)
        }
    }
    
}
