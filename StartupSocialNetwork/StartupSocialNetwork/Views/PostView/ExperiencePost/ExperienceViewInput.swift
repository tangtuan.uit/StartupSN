//
//  ExperienceViewInput.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/5/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol ExperienceViewInputDelegate: class {
    func openImagePickerFromExperience()
    func openInputViewControllerFormExperience(type: String)
}

class ExperienceViewInput: UICollectionViewCell {
    
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var buttonTypePost: UIButton!
    @IBOutlet weak var labelContent: PaddingLabel!
    
    let arrCategory = [kMCExperience, kMCSupport]
    
    weak var delegate: ExperienceViewInputDelegate?
    
    override func awakeFromNib() {
        self.contentView.isUserInteractionEnabled = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ExperienceViewInput.handleInputImageTap))
        self.imagePost.isUserInteractionEnabled = true
        self.imagePost.addGestureRecognizer(tap)
        
        self.setupLabelContent()
        

        
    }
    
    func setupLabelContent() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ExperienceViewInput.handleLabelContentTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelContent.isUserInteractionEnabled = true
        self.labelContent.addGestureRecognizer(gestureRecognizer)
        
    }
    
    func setImageForPost(images: [UIImage]) {
        if images.count > 0 {
            self.imagePost.image = images[0]
        } else {
            self.imagePost.image = #imageLiteral(resourceName: "add_image")
        }
    }
    
    func setTextForExperiencePost(post: Experience) {
        Helper.sharedInstance.setTextForFieldStartup(text: post.content, title: kTitleContentExperience, colorTitle: .darkGray, label: self.labelContent)
    }
    
    func handleInputImageTap() {
        if self.delegate != nil {
            self.delegate?.openImagePickerFromExperience()
        }
    }
    
    func handleLabelContentTap() {
        if self.delegate != nil {
            self.delegate?.openInputViewControllerFormExperience(type: kSUContentExperience)
        }
    }
    
    @IBAction func handleSelectTypeCategory(_ sender: Any) {
        
        FTPopOverMenu.showForSender(sender: sender as! UIView, with: self.arrCategory, done: { (index) in
            switch index {
            case 0:
                self.buttonTypePost.setTitle(kMCExperience, for: .normal)
                postExperience?.isQuestion = false
            case 1:
                self.buttonTypePost.setTitle(kMCSupport, for: .normal)
                postExperience?.isQuestion = true
            default:
                break
            }
        }) {
            print("cancel")
        }

    }
    
}
