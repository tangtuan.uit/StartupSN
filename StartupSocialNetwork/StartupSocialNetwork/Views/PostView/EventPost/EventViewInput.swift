//
//  EventPost.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/31/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol EventViewInputDelegate: class {
    func openImagePickerForEvent()
    func openInputViewControllerFormEvent(type: String)
}

class EventViewInput: UICollectionViewCell, UITextFieldDelegate, WWCalendarTimeSelectorProtocol {
    
    @IBOutlet weak var imageCover: UIImageView!
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var buttonImagePicker: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var textFieldNameEvent: UITextField!
    @IBOutlet weak var textFieldOrganizer: DesignableTextField!
    @IBOutlet weak var textFieldPhone: DesignableTextField!
    @IBOutlet weak var textFieldLocation: DesignableTextField!
    
    @IBOutlet weak var buttonDayStart: UIButton!
    @IBOutlet weak var buttonDayEnd: UIButton!
    @IBOutlet weak var buttonTimeStart: UIButton!
    @IBOutlet weak var buttonTimeEnd: UIButton!
    
    @IBOutlet weak var labelDescribe: PaddingLabel!
    
    @IBOutlet weak var heightImageCover: NSLayoutConstraint!
    
    weak var delegate: EventViewInputDelegate?
    
    private lazy var toolbar: FormToolbar = {
        return FormToolbar(inputs: self.inputs)
    }()
    
    private var inputs: [FormInput] {
        return [self.textFieldNameEvent as FormInput, self.textFieldOrganizer as FormInput, self.textFieldPhone as FormInput, self.textFieldLocation as FormInput]
    }
    
    private weak var activeInput: FormInput?
    
    fileprivate var singleDate: Date = Date()
    
    var viewConrtoller: UIViewController!
    
    var arrImage: [UIImage] = [UIImage]()
    
    var typeTime: TypeTimeEvent!
    
    var dayStart: String = ""
    var dayEnd: String = ""
    var timeStart: String = ""
    var timeEnd: String = ""
    
    override func awakeFromNib() {
        
        self.contentView.isUserInteractionEnabled = false
        
        self.buttonImagePicker.setCircleButton()
        
        // setup textfield
        self.textFieldNameEvent.delegate = self
        self.textFieldOrganizer.delegate = self
        self.textFieldPhone.delegate = self
        self.textFieldLocation.delegate = self
        
        self.textFieldNameEvent.setColorPlaceholder(text: "Tên sự kiện", color: colorPlaceholder)
        self.textFieldOrganizer.setColorPlaceholder(text: "Đơn vị tổ chức", color: colorPlaceholder)
        self.textFieldPhone.setColorPlaceholder(text: "Điện thoại", color: colorPlaceholder)
        self.textFieldLocation.setColorPlaceholder(text: "Địa điểm tổ chức", color: colorPlaceholder)
        
        self.toolbar = FormToolbar(inputs: [self.textFieldNameEvent, self.textFieldOrganizer, self.textFieldPhone, self.textFieldLocation])
        
        // Register notification
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        self.setupLabelDescribe()
    }
    
    func setupLabelDescribe() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EventViewInput.handleLabelDescribeTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelDescribe.isUserInteractionEnabled = true
        self.labelDescribe.addGestureRecognizer(gestureRecognizer)
        
    }
    
    func setupHeightImageCover(images: [UIImage]) {
        if images.count > 0 {
            self.heightImageCover.constant = 196
            self.imageCover.image = images[0]
        } else {
            self.heightImageCover.constant = 0
            self.imageCover.image = nil
        }
    }
    
    func setupDateTimePicker(index: Int) {
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        
        switch index {
        case 0:
            selector.optionStyles.showDateMonth(true)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showYear(false)
            selector.optionStyles.showTime(false)
        case 1:
            selector.optionStyles.showDateMonth(false)
            selector.optionStyles.showMonth(false)
            selector.optionStyles.showYear(false)
            selector.optionStyles.showTime(true)
        default:
            break
        }
        
        self.viewConrtoller.present(selector, animated: false, completion: nil)

    }
    
    func setDataForInput() {
        
        if postEvent?.title == nil || (postEvent?.title?.isEmpty)! {
            self.textFieldNameEvent.text = ""
        }
        if postEvent?.organizer == nil || (postEvent?.organizer?.isEmpty)! {
            self.textFieldOrganizer.text = ""
        }
        if postEvent?.phone == nil || (postEvent?.phone?.isEmpty)! {
            self.textFieldPhone.text = ""
        }
        if postEvent?.locationEvent == nil || (postEvent?.locationEvent?.isEmpty)! {
            self.textFieldLocation.text = ""
        }
        if postEvent?.dayStart == nil || (postEvent?.dayStart?.isEmpty)! {
            self.buttonDayStart.setTitle("Ngày bắt đầu", for: .normal)
        }
        if postEvent?.dayEnd == nil || (postEvent?.dayEnd?.isEmpty)! {
            self.buttonDayEnd.setTitle("Ngày kết thúc", for: .normal)
        }
        if postEvent?.timeStart == nil || (postEvent?.timeStart?.isEmpty)! {
            self.buttonTimeStart.setTitle("Thời gian bắt đầu", for: .normal)
        }
        if postEvent?.timeEnd == nil || (postEvent?.timeEnd?.isEmpty)! {
            self.buttonTimeEnd.setTitle("Thời gian kết thúc", for: .normal)
        }
        if postEvent?.describeEvent == nil || (postEvent?.describeEvent?.isEmpty)! {
            self.labelDescribe.text = kTitleDescribe
        }
    
        self.setDataForEvent()

    }
    
//    func setTextForStartupPost(post: Event) {
//        self.setTextForFieldEvent(textField: self.textFieldNameEvent, text: post.title)
//        self.setTextForFieldEvent(textField: self.textFieldOrganizer, text: post.organizer)
//        self.setTextForFieldEvent(textField: self.textFieldPhone, text: post.phone)
//        self.setTextForFieldEvent(textField: self.textFieldLocation, text: post.locationEvent)
//    }
    
    func setDataForEvent() {
        postEvent?.title = self.textFieldNameEvent.text
        postEvent?.organizer = self.textFieldOrganizer.text
        postEvent?.phone = self.textFieldPhone.text
        postEvent?.locationEvent = self.textFieldLocation.text
        postEvent?.dayStart = self.dayStart
        postEvent?.dayEnd = self.dayEnd
        postEvent?.timeStart = self.timeStart
        postEvent?.timeEnd = self.timeEnd
        
        Helper.sharedInstance.setTextForFieldStartup(text: postEvent?.describeEvent, title: kTitleDescribe, colorTitle: .darkGray, label: self.labelDescribe)
    }
    
//    func setTextForFieldEvent(textField: UITextField, text: String?) {
//        if text != nil && !(text?.isEmpty)! {
//            textField.text = ""
//        } else {
//            textField.text = text
//        }
//    }
    
    @IBAction func handleDateStart(_ sender: Any) {
        self.setupDateTimePicker(index: 0)
        self.typeTime = .dayStart
    }
    
    @IBAction func handleDateEnd(_ sender: Any) {
        self.setupDateTimePicker(index: 0)
        self.typeTime = .dayEnd
    }
    
    @IBAction func handleTimeStart(_ sender: Any) {
        self.setupDateTimePicker(index: 1)
        self.typeTime = .timeStart
    }
    
    @IBAction func handleTimeEnd(_ sender: Any) {
        self.setupDateTimePicker(index: 1)
        self.typeTime = .timeEnd
    }
    
    func handleLabelDescribeTap() {
        self.setDataForEvent()
        if self.delegate != nil {
            self.delegate?.openInputViewControllerFormEvent(type: kSUDescribe)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        toolbar.update()
        activeInput = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        toolbar.goForward()
        return true
    }
    
    @IBAction func handleImagePicker(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.openImagePickerForEvent()
        }
    }
    
    @objc func controlDidChange(_ control: UISegmentedControl) {
        toolbar.direction = control.selectedSegmentIndex == 0 ? .leftRight : .upDown
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 16.0, right: 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.scrollView.contentInset = .zero
        self.scrollView.scrollIndicatorInsets = .zero
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        self.setTimeForTitleButton(date: date)
        
    }
    
    func setTimeForTitleButton(date: Date) {
        if self.typeTime == .dayStart {
            self.buttonDayStart.setTitle(date.currentTimeZoneDay(), for: .normal)
            self.dayStart = Helper.sharedInstance.getiso8601ForDate(date: date)
        } else if typeTime == .dayEnd {
            self.buttonDayEnd.setTitle(date.currentTimeZoneDay(), for: .normal)
            self.dayEnd = Helper.sharedInstance.getiso8601ForDate(date: date)
        } else if typeTime == .timeStart {
            self.buttonTimeStart.setTitle(date.currentTimeZoneHour(), for: .normal)
            self.timeStart = Helper.sharedInstance.getiso8601ForDate(date: date)
        } else {
            self.buttonTimeEnd.setTitle(date.currentTimeZoneHour(), for: .normal)
            self.timeEnd = Helper.sharedInstance.getiso8601ForDate(date: date)
        }
    }
    
}

enum TypeTimeEvent {
    case dayStart
    case dayEnd
    case timeStart
    case timeEnd
}
