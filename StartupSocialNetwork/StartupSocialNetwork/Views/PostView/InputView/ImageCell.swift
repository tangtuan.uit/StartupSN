//
//  ImageCell.swift
//  TestViewPost
//
//  Created by Tang Tuan on 3/29/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol ImageCellDelegate: class {
    func deleteImage(indexPath: NSIndexPath)
}

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var buttonDelete: UIButton!
    
    var currentIndexPath: NSIndexPath!
    weak var delegate: ImageCellDelegate?
    
    override func awakeFromNib() {

        self.imageView.backgroundColor = .blue
        self.imageView.layer.cornerRadius = 5
        
        self.contentView.isUserInteractionEnabled = false
        
        self.buttonDelete.setCircleButton()
        
    }
    
    func setuoImageForCell(image: UIImage) {
        self.imageView.image = image
    }
    
    @IBAction func handleDeleteImage(_ sender: Any) {
        if self.delegate != nil && self.currentIndexPath != nil {
            self.delegate!.deleteImage(indexPath: self.currentIndexPath)
        }
    }
}

