//
//  InputImage.swift
//  TestViewPost
//
//  Created by Tang Tuan on 3/29/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol InputImageDelegate: class {
    func openImagePickerViewController()
}

class InputImage: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    weak var delegate: InputImageDelegate?
    
    override func awakeFromNib() {
        
        self.contentView.isUserInteractionEnabled = false
        
        self.imageView.layer.cornerRadius = 5
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(InputImage.handleInputImageTap))
        self.imageView.isUserInteractionEnabled = true
        self.imageView.addGestureRecognizer(tap)
    }
    
    func handleInputImageTap() {
        if self.delegate != nil {
            self.delegate?.openImagePickerViewController()
        }
    }
}
