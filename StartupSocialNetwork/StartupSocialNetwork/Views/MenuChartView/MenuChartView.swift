//
//  MenuChartView.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 7/6/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

protocol MenuChartViewDelegate: class {
    func openTrendViewController()
    func openTopStartupViewController()
}

class MenuChartView: NSObject {
    
    weak var delegate: MenuChartViewDelegate?
    
    let blackView = UIView()
    
    override init() {
        super.init()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        self.tableView.separatorStyle = .none
    }
    
    let tableView: UITableView = {
        let tb = UITableView()
        tb.backgroundColor = .white
        tb.separatorStyle = .none
        tb.bounces = false
        return tb
    }()
    
    func handleShowMenuChart() {
        
        if let window = UIApplication.shared.keyWindow {
            self.blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleDissmiss) ))
            
            window.addSubview(self.blackView)
            window.addSubview(self.tableView)
            
            self.blackView.frame = window.frame
            self.blackView.alpha = 0
            
            let y = window.frame.height - 100
            self.tableView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: 100)
            
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                self.tableView.frame = CGRect(x: 0, y: y, width: window.frame.width, height: self.tableView.frame.height)
            }, completion: nil)
        }
    }
    
    func handleDissmiss() {
        UIView.animate(withDuration: 0.4) {
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.tableView.frame = CGRect(x: 0, y: window.frame.height, width: self.tableView.frame.width, height: self.tableView.frame.height)
            }
        }
    }
    
    func handleClickMenu(row: Int) {
        
        UIView.animate(withDuration: 0.4, animations: { 
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.tableView.frame = CGRect(x: 0, y: window.frame.height, width: self.tableView.frame.width, height: self.tableView.frame.height)
            }
        }) { (success) in
            if row == 0 {
                if self.delegate != nil {
                    self.delegate?.openTopStartupViewController()
                }
            } else {
                if self.delegate != nil {
                    self.delegate?.openTrendViewController()
                }
            }
        }

    }

}

extension MenuChartView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell")
        if indexPath.row == 0 {
            cell?.textLabel?.text = "Top dự án khởi nghiệp"
        } else {
            cell?.textLabel?.text = "Xu hướng khởi nghiệp"
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.handleClickMenu(row: indexPath.row)
    }
}
