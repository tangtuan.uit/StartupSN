//
//  StatusViewCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/23/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

class StatusViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewButton: UIView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var textViewDescribe: UITextView!
    @IBOutlet weak var labelDetail: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    override func awakeFromNib() {
        
    }
    
}
