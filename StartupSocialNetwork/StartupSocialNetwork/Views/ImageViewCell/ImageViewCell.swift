//
//  ImageViewCell.swift
//  Piminium
//
//  Created by Tang Tuan on 11/18/16.
//  Copyright © 2016 Pim. All rights reserved.
//

import UIKit
import SDWebImage

class ImageViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var labelPhoto: UILabel!
    @IBOutlet weak var viewPhoto: UIView!
    
    override func awakeFromNib() {
        
    }
    
    func setImageForCell(image: Image, item: Int) {
        
        let url = NSURL(string: image.path!)
        self.imagePost.sd_setImage(with: url as URL!)
        
        if item == 3 {
            self.viewPhoto.isHidden = false
        }
    }
    
    func setImageForCellPhotosProfile(image: Image, item: Int) {
        
        let url = NSURL(string: image.path!)
        self.imagePost.sd_setImage(with: url as URL!)
        
        if item == 3 {
            self.viewPhoto.isHidden = false
            self.labelPhoto.font = UIFont.systemFont(ofSize: 12)
            self.labelPhoto.text = "VIEW ALL"
        }
    }
}
