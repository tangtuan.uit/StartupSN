//
//  EventViewDetail.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/21/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class EventViewDetail: UICollectionReusableView {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var buttonShare: UIButton!
    @IBOutlet weak var buttonJoin: UIButton!
    @IBOutlet weak var buttonCare: UIButton!
    @IBOutlet weak var labelJoin: UILabel!
    @IBOutlet weak var labelCare: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    
    var joined: Bool!
    var iJoined: Int = 0
    
    override func awakeFromNib() {
        
        self.buttonCare.setupButton()
        self.buttonJoin.setupButton()
        self.buttonShare.setupButton()
    
    }
    
    func setupViewForPostEvent(post: Post) {
        
        let postEvent = post as! Event
        
        self.iJoined = (postEvent.joned?.count)!
        
        self.labelDate.setTextForDateEventPost()
        self.labelTitle.setTitleForEventDetail(post: postEvent)
        self.labelCare.setTextForCareAndJoin(text: "Quan tâm", number: postEvent.care)
        self.labelJoin.setTextForCareAndJoin(text: "Tham gia", number: (postEvent.joned?.count)!)
        self.labelContent.setTextForTextViewEvent(post: postEvent)
        
        
        
        if (postEvent.joned?.contains((user_current?.id)!))! {
            self.joined = true
            self.buttonJoin.setImage(#imageLiteral(resourceName: "ic_joined"), for: .normal)
            self.buttonJoin.setTitle("Joined", for: .normal)
        } else {
            self.joined = false
            self.buttonJoin.setImage(#imageLiteral(resourceName: "ic_join"), for: .normal)
            self.buttonJoin.setTitle("Join", for: .normal)
        }
    }
    
    @IBAction func handleJoin(_ sender: Any) {
        if self.joined {
            //unjoin
            
            let alert = FCAlertView()
            alert.delegate = self
            
            alert.titleColor = alert.flatRed
            alert.subTitleColor = alert.flatGray
            alert.titleFont = UIFont.boldSystemFont(ofSize: 18)
            alert.subtitleFont = UIFont.systemFont(ofSize: 16)
            alert.firstButtonBackgroundColor = UIColor(white: 0.95, alpha: 1)
            alert.secondButtonBackgroundColor = alert.flatRed
            alert.secondButtonTitleColor = .white
            alert.cornerRadius = 8
            alert.makeAlertTypeWarning()
            alert.dismissOnOutsideTouch = false
            alert.hideSeparatorLineView = true
            alert.hideDoneButton = true
            alert.showAlert(withTitle: "Hủy tham gia",
                            withSubtitle: "Bạn có muốn hủy tham gia sự kiện này?",
                            withCustomImage: nil,
                            withDoneButtonTitle: nil,
                            andButtons: ["Bỏ qua", "Hủy"])
            
        } else {
            //join
            
            let alert = FCAlertView()
            alert.delegate = self
            
            alert.titleColor = alert.flatGray
            alert.subTitleColor = alert.flatGray
            alert.titleFont = UIFont.boldSystemFont(ofSize: 18)
            alert.subtitleFont = UIFont.systemFont(ofSize: 16)
            alert.firstButtonBackgroundColor = UIColor(white: 0.95, alpha: 1)
            alert.secondButtonBackgroundColor = alert.flatOrange
            alert.secondButtonTitleColor = .white
            alert.cornerRadius = 8
            alert.makeAlertTypeCaution()
            alert.dismissOnOutsideTouch = false
            alert.hideSeparatorLineView = true
            alert.hideDoneButton = true
            alert.showAlert(withTitle: "Tham gia sự kiện",
                            withSubtitle: "Bạn có muốn tham gia sự kiện này không?",
                            withCustomImage: nil,
                            withDoneButtonTitle: nil,
                            andButtons: ["Bỏ qua", "Xác nhận"])
            
        }
    }
}

extension EventViewDetail: FCAlertViewDelegate {
    func fcAlertView(_ alertView: FCAlertView!, clickedButtonIndex index: Int, buttonTitle title: String!) {
        
        if title == "Xác nhận" {
            self.iJoined += 1
        } else {
            self.iJoined -= 1
        }
        self.joined = !self.joined
        self.labelJoin.setTextForCareAndJoin(text: "Tham gia", number: self.iJoined)
    }

}
