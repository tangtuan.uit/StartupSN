//
//  StartupReviewCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 5/22/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol StartupReviewCellDelegate: class {
    func openDetailAtIndex(indexPath: NSIndexPath)
    func openCommentAtIndexFromStartup(indexPath: NSIndexPath)
    func openViewProfileFromStartup(indexPath: NSIndexPath)
}

class StartupReviewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var heightLabelDetail: NSLayoutConstraint!
    @IBOutlet weak var viewSliderShow: ImageSlideshow!
    
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var labelNumberReview: UILabel!
    
    @IBOutlet weak var viewReivew: UIView!
    @IBOutlet weak var imageAvatarReviewer: UIImageView!
    @IBOutlet weak var labelNameReviewer: UILabel!
    @IBOutlet weak var labelContentReview: UILabel!
    
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var imageAvatarInput: UIImageView!
    
    var postStartup: Startup!
    weak var delegate: StartupReviewCellDelegate?
    var currentIndexPath: NSIndexPath!
    var viewFullScreen: UIViewController!
    
    override func awakeFromNib() {
        
    }
    
    func setupView(isDetail: Bool, post: Startup) {
        
        self.postStartup = post
        
        
        self.imageAvatar.setCircularImage()
        if post.user?.image != nil {
            self.imageAvatar.sd_setImage(with: Helper.sharedInstance.setUrlForImage(path: (post.user?.image)!))
        } else {
            self.imageAvatar.image = #imageLiteral(resourceName: "default_avatar")
        }
        
        let date = Helper.sharedInstance.stringToDate(date: post.time!)
        let time = Helper.sharedInstance.getTimePost(date: date)
        
        self.labelName.setUsernamForStartupPost(username: (post.user?.name)!, time: time)
        self.contentView.isUserInteractionEnabled = false
        
        // set tap for label detail
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StartupPostViewCell.handleTextViewDescribeTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelDetail.isUserInteractionEnabled = true
        self.labelDetail.addGestureRecognizer(gestureRecognizer)
        
        // sey tap for label name
        let gestureRecognizerLabelName = UITapGestureRecognizer(target: self, action: #selector(StartupPostViewCell.handleTextViewNameTap))
        gestureRecognizerLabelName.numberOfTapsRequired = 1
        self.labelName.isUserInteractionEnabled = true
        self.labelName.addGestureRecognizer(gestureRecognizerLabelName)
        
        if isDetail {
            self.setupViewForDetail(post: self.postStartup)
        } else {
            self.setupViewForCell(post: self.postStartup)
        }
        
        if (user_current?.state) != "expert" {
//            self.buttonVote.isHidden = false
        }
        
        self.setupTapViewReview()
        self.setupTapViewInput()
        self.setupViewReview()
    }
    
    func setupViewForCell(post: Startup) {
        self.labelContent.setTextForTextViewDescribe(text: (post.idea!))
        self.labelContent.numberOfLines = 8
    }
    
    func setupViewForDetail(post: Startup) {
        self.heightLabelDetail.constant = 0
        self.labelContent.setTextForTextViewDetail(post: post)
        self.labelContent.numberOfLines = 0
    }
    
    func setupViewReview() {
        self.imageAvatarReviewer.setCircularImage()
        if self.postStartup .review?.user?.image != nil {
            self.imageAvatarReviewer.sd_setImage(with: Helper.sharedInstance.setUrlForImage(path: (self.postStartup.review?.user?.image)!))
        } else {
            self.imageAvatarReviewer.image = #imageLiteral(resourceName: "default_avatar")
        }
        self.labelNameReviewer.text = self.postStartup.review?.user?.name
        self.labelContentReview.text = self.postStartup.review?.content
        
        self.imageAvatarInput.setCircularImage()
        if self.postStartup .review?.user?.image != nil {
            self.imageAvatarInput.sd_setImage(with: Helper.sharedInstance.setUrlForImage(path: (user_current?.image)!))
        } else {
            self.imageAvatarInput.image = #imageLiteral(resourceName: "default_avatar")
        }
    }
    
    func setupTapViewReview() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(StartupReviewCell.handleViewReviewTap))
        self.viewReivew.addGestureRecognizer(tap)
    }
    
    func setupTapViewInput() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(StartupReviewCell.handleViewInputTap))
        self.viewInput.addGestureRecognizer(tap)
    }
    
    func handleTextViewDescribeTap() {
        if self.delegate != nil && self.currentIndexPath != nil {
            self.delegate!.openDetailAtIndex(indexPath: self.currentIndexPath)
        }
    }
    
    func handleTextViewNameTap() {
        if self.delegate != nil && self.currentIndexPath != nil {
            self.delegate!.openViewProfileFromStartup(indexPath: self.currentIndexPath)
        }
    }
    
    func handleViewReviewTap() {
        print(123)
    }
    
    func handleViewInputTap() {
        print(456)
    }
    
    func setupSliderShow(arrImage : [Image]) {
        
        self.viewSliderShow.backgroundColor = UIColor.white
        self.viewSliderShow.slideshowInterval = 5.0
        self.viewSliderShow.pageControlPosition = PageControlPosition.underScrollView
        self.viewSliderShow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        self.viewSliderShow.pageControl.pageIndicatorTintColor = UIColor.black
        self.viewSliderShow.contentScaleMode = UIViewContentMode.scaleAspectFill
        self.viewSliderShow.currentPageChanged = { page in
            
        }
        
        var sdWebImage = [SDWebImageSource]()
        for image in arrImage {
            sdWebImage.append(SDWebImageSource(urlString: Helper.sharedInstance.setStringUrlForImage(path: image.path!))!)
        }
        
        // try out other sources such as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        self.viewSliderShow.setImageInputs(sdWebImage)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(StartupPostViewCell.didTap))
        self.viewSliderShow.addGestureRecognizer(recognizer)
    }
    
    func didTap() {
        self.viewSliderShow.presentFullScreenController(from: viewFullScreen)
    }
    
}
