//
//  PostViewCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/13/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import SDWebImage

protocol StartupPostViewCellDelegate: class {
    func openDetailAtIndex(indexPath: NSIndexPath)
    func openCommentAtIndexFromStartup(indexPath: NSIndexPath)
    func openViewProfileFromStartup(indexPath: NSIndexPath)
    func openInputReviewFromStartup(indexPath: NSIndexPath)
}

class StartupPostViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewButton: UIView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var buttonComment: UIButton!
    @IBOutlet weak var buttonShare: UIButton!
    @IBOutlet weak var buttonVote: UIButton!

    @IBOutlet weak var labelContent: UILabel!
    
    @IBOutlet weak var heightLabelDetail: NSLayoutConstraint!
    
    var currentIndexPath: NSIndexPath!
    weak var delegate: StartupPostViewCellDelegate?
    
    var viewFullScreen: UIViewController!
    
    @IBOutlet weak var viewSliderShow: ImageSlideshow!
    
    var arrImg: [Image]!
    var isLike: Bool!
    var postStartup: Startup!
    
    
    override func awakeFromNib() {
        
    }
    
    func setupView(isDetail: Bool, post: Post, liked: Bool) {
        
        self.postStartup = post as! Startup
        
        // set radius for view header and footer
        self.layer.cornerRadius = 5
        self.viewHeader.layer.cornerRadius = 5
        self.viewButton.layer.cornerRadius = 5
        
        
        
        
        self.buttonLike.setTitle(" \(self.postStartup.like)", for: .normal)
        if liked {
            self.buttonLike.setImage(#imageLiteral(resourceName: "ic_liked"), for: .normal)
        } else {
            self.buttonLike.setImage(#imageLiteral(resourceName: "ic_like"), for: .normal)
        }
        
        self.isLike = liked
        
        self.buttonComment.setTitle(" \(self.postStartup.comment)", for: .normal)
        
        let date = Helper.sharedInstance.stringToDate(date: post.time!)
        let time = Helper.sharedInstance.getTimePost(date: date)
        
        self.labelName.setUsernamForStartupPost(username: (post.user?.name)!, time: time)
        self.contentView.isUserInteractionEnabled = false
        
        // set tap for label detail
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StartupPostViewCell.handleTextViewDescribeTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelDetail.isUserInteractionEnabled = true
        self.labelDetail.addGestureRecognizer(gestureRecognizer)
        
        // sey tap for label name
        let gestureRecognizerLabelName = UITapGestureRecognizer(target: self, action: #selector(StartupPostViewCell.handleTextViewNameTap))
        gestureRecognizerLabelName.numberOfTapsRequired = 1
        self.labelName.isUserInteractionEnabled = true
        self.labelName.addGestureRecognizer(gestureRecognizerLabelName)
        
        if isDetail {
            self.setupViewForDetail(post: self.postStartup)
        } else {
            self.setupViewForCell(post: self.postStartup)
        }
        
        if (user_current?.state) == "expert" {
            self.buttonVote.isHidden = false
        }
        
        self.imageAvatar.setCircularImage()
        if post.user?.image != nil {
            let url = Helper.sharedInstance.setUrlForImage(path: (post.user?.image)!)
            Helper.sharedInstance.displayImageWithURL(imageView: self.imageAvatar, url: url)
        } else {
            self.imageAvatar.image = #imageLiteral(resourceName: "default_avatar")
        }
        
        
    
        
    }
    
    func setupViewForCell(post: Startup) {
        self.labelContent.setTextForTextViewDescribe(text: (post.idea!))
        self.labelContent.numberOfLines = 8
    }
    
    func setupViewForDetail(post: Startup) {
        self.heightLabelDetail.constant = 0
        self.labelContent.setTextForTextViewDetail(post: post)
        self.labelContent.numberOfLines = 0
    }
    
    func handleTextViewDescribeTap() {
        if self.delegate != nil && self.currentIndexPath != nil {
            self.delegate!.openDetailAtIndex(indexPath: self.currentIndexPath)
        }
    }
    
    func handleTextViewNameTap() {
        if self.delegate != nil && self.currentIndexPath != nil {
            self.delegate!.openViewProfileFromStartup(indexPath: self.currentIndexPath)
        }
    }
    
    @IBAction func handleButtonMore(_ sender: Any) {
        print(123)
    }
    
    func setupSliderShow(arrImage : [Image]) {
        
        self.viewSliderShow.backgroundColor = UIColor.white
        self.viewSliderShow.slideshowInterval = 5.0
        self.viewSliderShow.pageControlPosition = PageControlPosition.underScrollView
        self.viewSliderShow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        self.viewSliderShow.pageControl.pageIndicatorTintColor = UIColor.black
        self.viewSliderShow.contentScaleMode = UIViewContentMode.scaleAspectFill
        self.viewSliderShow.currentPageChanged = { page in

        }
        
        var sdWebImage = [SDWebImageSource]()
        for image in arrImage {
            sdWebImage.append(SDWebImageSource(urlString: Helper.sharedInstance.setStringUrlForImage(path: image.path!))!)
        }
        
        // try out other sources such as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        self.viewSliderShow.setImageInputs(sdWebImage)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(StartupPostViewCell.didTap))
        self.viewSliderShow.addGestureRecognizer(recognizer)
    }
    
    func didTap() {
        self.viewSliderShow.presentFullScreenController(from: viewFullScreen)
    }
    
    @IBAction func handleButonLike(_ sender: Any) {
        if !self.isLike {
            self.setupLike(iLike: self.postStartup.like)
            APIClient.sharedInstance.like(self.postStartup.id!, (user_current?.id)!, completionHandle: { (success) in
                if (success) {
                    self.isLike = true
                    self.postStartup.like += 1
                } else {
                    self.setupUnLike(iLike: (self.postStartup.like + 1))
                }
            }, failure: { (error) in
                print(error)
                self.setupUnLike(iLike: (self.postStartup.like + 1))
            })
        } else {
            self.setupUnLike(iLike: self.postStartup.like)
            APIClient.sharedInstance.unlike(self.postStartup.id!, (user_current?.id)!, completionHandle: { (success) in
                    if (success) {
                        self.isLike = false
                        self.postStartup.like -= 1
                    } else {
                        self.setupLike(iLike: (self.postStartup.like - 1))
                    }
            }, failure: { (error) in
                print(error)
                self.setupLike(iLike: (self.postStartup.like - 1))
            })
        }
    }
    
    @IBAction func handleButonComment(_ sender: Any) {
        if self.delegate != nil && self.currentIndexPath != nil {
            self.delegate!.openCommentAtIndexFromStartup(indexPath: self.currentIndexPath)
        }
    }
    
    @IBAction func handleButonShare(_ sender: Any) {
        
    }
    
    @IBAction func handleButonVote(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.openInputReviewFromStartup(indexPath: self.currentIndexPath)
        }
    }
    
    func setupLike(iLike: Int) {
        self.buttonLike.setTitle(" \(iLike + 1)", for: .normal)
        self.buttonLike.setImage(#imageLiteral(resourceName: "ic_liked"), for: .normal)
    }
    
    func setupUnLike(iLike: Int) {
        self.buttonLike.setTitle(" \(iLike - 1)", for: .normal)
        self.buttonLike.setImage(#imageLiteral(resourceName: "ic_like"), for: .normal)
    }
}

extension StartupPostViewCell: FCAlertViewDelegate {
    
}

