//
//  EventPostViewCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/13/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

protocol EventPostViewCellDelegate: class {
    func openViewPeopleJoin(indexPath: NSIndexPath)
}

class EventPostViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewButton: UIView!
    
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelPostLocation: UILabel!
    
    @IBOutlet weak var imageCover: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTitleEvent: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelPeopleCare: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    
    weak var delegate: EventPostViewCellDelegate?
    var indexPath: NSIndexPath!
    
    @IBOutlet weak var buttonJoin: UIButton!
    
    override func awakeFromNib() {
        
        
        self.contentView.isUserInteractionEnabled = false
        
        self.layer.cornerRadius = 5
        self.viewHeader.layer.cornerRadius = 5
        self.viewButton.layer.cornerRadius = 5
        
        self.setupLabelPeopleJoin()
        
    }
    
    func setupViewEventWithPost(post: Post) {
        
        let postEvent = post as! Event
        
        self.imageAvatar.setCircularImage()
        
        if postEvent.user?.image != nil {
            let url = Helper.sharedInstance.setUrlForImage(path: (post.user?.image)!)
            Helper.sharedInstance.displayImageWithURL(imageView: self.imageAvatar, url: url)
        } else {
            self.imageAvatar.image = #imageLiteral(resourceName: "default_avatar")
        }
        
        self.labelUsername.setUsernamForEventPost(username: (postEvent.user?.name)!)
        self.labelTitleEvent.text = postEvent.title
        self.labelDate.setTextForDateEventPost()
        self.labelPhone.setImageForLabel(image: #imageLiteral(resourceName: "ic_phone"), text: postEvent.phone!)
        self.labelTime.setImageForLabel(image: #imageLiteral(resourceName: "ic_time"), text: postEvent.timeStart! + " - " + postEvent.timeEnd!)
        self.labelLocation.setImageForLabel(image: #imageLiteral(resourceName: "ic_loaction"), text: postEvent.locationEvent!)
        self.labelPeopleCare.setImageForLabel(image: #imageLiteral(resourceName: "ic_people"), text: "\(postEvent.joned?.count ?? 0) người tham gia")
        
        let url = Helper.sharedInstance.setUrlForImage(path: (post.images?[0].path!)!)
        Helper.sharedInstance.displayImageWithURL(imageView: self.imageCover, url: url)
        
        if (postEvent.joned?.contains((user_current?.id)!))! {
            self.buttonJoin.setImage(#imageLiteral(resourceName: "ic_joined"), for: .normal)
            self.buttonJoin.setTitle("Đã tham gia", for: .normal)
        } else {
            self.buttonJoin.setImage(#imageLiteral(resourceName: "ic_join"), for: .normal)
            self.buttonJoin.setTitle("Tham gia", for: .normal)
        }
    }
    
    func setupLabelPeopleJoin() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EventPostViewCell.handleShowPeopleJoin))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelPeopleCare.isUserInteractionEnabled = true
        self.labelPeopleCare.addGestureRecognizer(gestureRecognizer)
    }
    
    func handleShowPeopleJoin() {
        if self.delegate != nil {
            self.delegate?.openViewPeopleJoin(indexPath: self.indexPath)
        }
    }
}
