//
//  CommentViewCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/22/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class CommentViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    
    override func awakeFromNib() {
        self.imageAvatar.setCircularImage()
    }
    
    func setupCommentForCell(comment: Comment) {
        
        if comment.user?.image != nil {
            self.imageAvatar.sd_setImage(with: Helper.sharedInstance.setUrlForImage(path: (comment.user?.image)!))
        } else {
            self.imageAvatar.image = #imageLiteral(resourceName: "default_avatar")
        }
        
        self.labelName.text = comment.user?.name
        
        self.labelContent.text = comment.content
        
        if comment.time == nil {
            self.labelTime.text = "đang gửi"
        } else {
            let date = Helper.sharedInstance.stringToDate(date: comment.time!)
            self.labelTime.text = Helper.sharedInstance.getTimePost(date: date)
        }
    }
    
}
