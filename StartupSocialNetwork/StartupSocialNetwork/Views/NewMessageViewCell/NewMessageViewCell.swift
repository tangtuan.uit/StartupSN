//
//  NewMessageViewCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/27/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class NewMessageViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var buttonCheck: UIButton!
    
    var isCheck: Bool = false
    
    override func awakeFromNib() {
        
        self.contentView.isUserInteractionEnabled = false
        
        self.imageViewAvatar.setCircularImage()
    }
    
    func setupUserForCell(user: User) {
        
        if user.image != nil {
            self.imageViewAvatar.sd_setImage(with: Helper.sharedInstance.setUrlForImage(path: (user.image)!))
        } else {
            self.imageViewAvatar.image = #imageLiteral(resourceName: "default_avatar")
        }
        
        self.labelName.text = user.name
        self.labelEmail.text = user.email
    }
    
    @IBAction func handleCheck(_ sender: Any) {
        if !isCheck {
            self.buttonCheck.setImage(#imageLiteral(resourceName: "ic_checked"), for: .normal)
            self.isCheck = true
        } else {
            self.buttonCheck.setImage(#imageLiteral(resourceName: "ic_uncheck"), for: .normal)
            self.isCheck = false
            
        }
    }
}
