//
//  OptionViewCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/7/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class OptionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelText: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    func setupCellForProfile(user: User) {
        self.labelText.setTextForCellProfile(name: user.name!, username: user.email!)
        
        if user.image != nil {
            DispatchQueue.global(qos: .background).async {
                let url = Helper.sharedInstance.setUrlForImage(path: user.image!)
                DispatchQueue.main.async {
                    self.imageCell.sd_setImage(with: url)
                }
            }
           
        }
    }
    
    func setupCellForSearchStartup(post: Post) {
        let startup = post as! Startup
        self.labelText.setTextForCellProfile(name: startup.idea!, username: "")
        
        if startup.images?[0].path != nil {
            DispatchQueue.global(qos: .background).async {
                let url = Helper.sharedInstance.setUrlForImage(path: (startup.images?[0].path!)!)
                DispatchQueue.main.async {
                    self.imageCell.sd_setImage(with: url)
                }
            }
            
        }
    }
}
