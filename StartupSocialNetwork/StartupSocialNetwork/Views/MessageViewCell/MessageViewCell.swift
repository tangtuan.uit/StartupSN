//
//  MessageViewCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/22/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class MessageViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var imgOnline: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.imgAvatar.setCircularImage()
        self.imgOnline.setCircularImage()
        
        //self.isUserInteractionEnabled = true
    }
    
    func setupGroupForCell(group: Group) {
        
        if group.user != nil {
            if group.user?.image != nil {
                self.imgAvatar.sd_setImage(with: Helper.sharedInstance.setUrlForImage(path: (group.user?.image)!))
            } else {
                self.imgAvatar.image = #imageLiteral(resourceName: "default_avatar")
            }
            
            self.labelName.text = group.user?.name
            
            if group.message?.id_send == user_current?.id {
                self.labelContent.text =  "Bạn: " + (group.message?.content)!
            } else {
                self.labelContent.text = group.message?.content
            }
            
        }
        
    }
    
}
