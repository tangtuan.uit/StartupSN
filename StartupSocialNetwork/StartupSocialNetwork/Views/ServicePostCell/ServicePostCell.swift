//
//  ServicePostCell.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/15/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class ServicePostCell: UICollectionViewCell {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewButton: UIView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    @IBOutlet weak var labelLocaltion: UILabel!
    
    @IBOutlet weak var viewSliderShow: ImageSlideshow!
    
    @IBOutlet weak var heightLabelDetail: NSLayoutConstraint!
    
    var viewFullScreen: UIViewController!
    
    override func awakeFromNib() {
        self.imageAvatar.setCircularImage()
    }
    
    func setupViewServicePost(post: Post!) {
        let postService = post as! Service
        
        let date = Helper.sharedInstance.stringToDate(date: post.time!)
        let time = Helper.sharedInstance.getTimePost(date: date)
        self.labelName.setUsernamForStartupPost(username: (postService.user?.name)!, time: time)
        self.labelLocaltion.setLabelLocationService(post: postService)
        self.labelContent.text = postService.describeService
        let height = Helper.sharedInstance.getHeightTextForPost(width: self.frame.width - 16, text: postService.describeService!, sizeFont: 14)
        if height <= 136 {
            self.heightLabelDetail.constant = 0
        } else {
            self.heightLabelDetail.constant = 17
        }
        
        self.imageAvatar.setCircularImage()
        if post.user?.image != nil {
            let url = Helper.sharedInstance.setUrlForImage(path: (post.user?.image)!)
            Helper.sharedInstance.displayImageWithURL(imageView: self.imageAvatar, url: url)
        } else {
            self.imageAvatar.image = #imageLiteral(resourceName: "default_avatar")
        }
        
    }
    
    func setupSliderShow(arrImage : [Image]) {
        
        self.viewSliderShow.backgroundColor = UIColor.white
        self.viewSliderShow.slideshowInterval = 5.0
        self.viewSliderShow.pageControlPosition = PageControlPosition.underScrollView
        self.viewSliderShow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        self.viewSliderShow.pageControl.pageIndicatorTintColor = UIColor.black
        self.viewSliderShow.contentScaleMode = UIViewContentMode.scaleAspectFill
        self.viewSliderShow.currentPageChanged = { page in
            
        }
        
        var sdWebImage = [SDWebImageSource]()
        for image in arrImage {
            sdWebImage.append(SDWebImageSource(urlString: Helper.sharedInstance.setStringUrlForImage(path: image.path!))!)
        }
        
        // try out other sources such as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        self.viewSliderShow.setImageInputs(sdWebImage)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(StartupPostViewCell.didTap))
        self.viewSliderShow.addGestureRecognizer(recognizer)
    }
    
    func didTap() {
        self.viewSliderShow.presentFullScreenController(from: viewFullScreen)
    }

}
