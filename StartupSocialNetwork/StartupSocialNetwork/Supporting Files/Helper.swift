//
//  Helper.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/9/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

class Helper {
    
    static let sharedInstance: Helper = { Helper() }()
    
    func getTabBarSelected(typeView: TypeView) -> Int {
        if typeView == .newsFeed {
            return 0
        } else if typeView == .message {
            return 1
        } else if typeView == .notification {
            return 3
        } else {
            return 4
        }
    }

    func getWidthButtonCategoryPost(arrString: [String]) -> Int {
        var max = 0
        
        for i in 1 ..< arrString.count {
            if arrString[i].characters.count > arrString[max].characters.count {
                max = i
            }
        }
        
        let widthButton = (arrString[max].characters.count * 10) + 32
        return widthButton
    }
    
    func checkImageHorizontal(image: Image) -> Bool {
        
        if image.height > image.width {
            return false
        }
        
        return true
    }
    
    // set heigth of image
    func setHeightImage(widthView: CGFloat, image: Image) -> CGFloat {
        
        var height = (widthView * CGFloat(image.height)) / CGFloat(image.width)
        
        if height > (widthView * 4 / 3) {
            height = widthView * 4 / 3
        }
        
        return height
    }

    // set height of collection view
    func setHeightCollectionView(widthView: CGFloat, arrImage: [Image]) -> CGFloat {
        if arrImage.isEmpty {
            return 0
        } else if arrImage.count == 1 {
            let height = self.setHeightImage(widthView: widthView, image: arrImage[0])
            return height
        } else if arrImage.count == 2 {
            let imgFirst = self.checkImageHorizontal(image: arrImage[0])
            let imgSecond = self.checkImageHorizontal(image: arrImage[1])
            if ((imgFirst && imgSecond) || (!imgFirst && !imgSecond)) {
                return widthView - 2
            } else {
                return (widthView / 2) - 2
            }
        } else {
            return widthView
        }
    }
    
    func getiso8601ForDate(date: Date) -> String {
        let stringFromDate = date.iso8601    // "2017-03-22T13:22:13.933Z"
        let dateFromString = stringFromDate.dateFromISO8601
        return dateFromString!.iso8601      // "2017-03-22T13:22:13.933Z"
    }
    
    func getHeightTextForPost(width: CGFloat, text: String, sizeFont: CGFloat) -> CGFloat {
        let rect = NSString(string: text).boundingRect(with: CGSize(width: width, height: 10000), options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: sizeFont)], context: nil)
        return rect.height
    }
    
    func getHeightTextForEventDetail(width: CGFloat, post: Post) -> CGFloat {
        let postEvent = post as! Event
        let text = "Thông tin sự kiện\n\n" + "Diễn ra vào thứ 7 ngày 1 tháng 4\n\nBắt đầu từ \(postEvent.timeStart!) đến \(postEvent.timeEnd!)" + "\n\nLiên hệ: " + postEvent.phone! + "\n\nĐịa điểm: " + postEvent.locationEvent! + "\n\nChi tiết sự kiện" + "\n\(postEvent.describeEvent!)"
        let rect = NSString(string: text).boundingRect(with: CGSize(width: width, height: 10000), options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
        return rect.height
    }
    
    func setStringUrlForImage(path: String) -> String {
        let urlString = path.replace(target: "public", withString: referenceURL)
        return urlString
    }
    
    func setUrlForImage(path: String) -> URL {
        let urlString = path.replace(target: "public", withString: referenceURL)
        let url = URL(string: urlString)
        return url!
    }
    
    func setTypePostWithString(type: String) -> TypePost {
        if type == kMCStartup {
            return .startup
        } else if type == kMCEvent {
            return .event
        } else if type == kMCShare {
            return .experience
        } else if type == kMCService {
            return .service
        }
        return .status
    }
    
    func getHeightStartupPost(post: Startup, width: CGFloat) -> CGFloat {
        
        var height: CGFloat!
        
        let textIdea = kTitleIdea + self.addTitleForTextStartupPost(text: post.idea)
        let heightIdea = self.getHeightTextForPost(width: width, text: textIdea, sizeFont: 15)
        
        let textTeam = kTitleTeam + self.addTitleForTextStartupPost(text: post.team)
        let heightTeam = self.getHeightTextForPost(width: width, text: textTeam, sizeFont: 15)
        
        let textFields = kTitleFields + self.addTitleForTextStartupPost(text: post.fields)
        let heightFields = self.getHeightTextForPost(width: width, text: textFields, sizeFont: 15)
        
        let textUser = kTitleUser + self.addTitleForTextStartupPost(text: post.userObject)
        let heightUser = self.getHeightTextForPost(width: width, text: textUser, sizeFont: 15)
        
        let textDevelopment = kTitleDevelopment + self.addTitleForTextStartupPost(text: post.orientedDevelopment)
        let heightDevelopment = self.getHeightTextForPost(width: width, text: textDevelopment, sizeFont: 15)
        
        let textProduct = kTitleProduct + self.addTitleForTextStartupPost(text: post.describeProduct)
        let heightProduct = self.getHeightTextForPost(width: width, text: textProduct, sizeFont: 15)
        
        height = heightIdea + heightTeam + heightUser + heightFields + heightDevelopment + heightProduct
        
        return height
    }
    
    func getHeightEventPost(post: Event, width: CGFloat) -> CGFloat {
        
        let textIdea = kTitleDescribe + self.addTitleForTextStartupPost(text: post.describeEvent)
        let height = self.getHeightTextForPost(width: width, text: textIdea, sizeFont: 15)
        
        return height
    }
    
    func getHeightEventExperience(post: Experience, width: CGFloat) -> CGFloat {
        
        let textIdea = kTitleContentExperience + self.addTitleForTextStartupPost(text: post.content)
        let height = self.getHeightTextForPost(width: width, text: textIdea, sizeFont: 15)
        
        return height
    }
    
    func addTitleForTextStartupPost(text: String?) -> String {
        let newText = (text == nil) ? "" : "\n\(text!)"
        return newText
    }
    
    func setTextForFieldStartup(text: String?, title: String, colorTitle: UIColor, label: UILabel) {
        if text != nil && !(text?.isEmpty)! {
            if title == kTitleAbout {
                label.setLabelTextForAboutUser(title: title, text: text!)
            } else {
                label.setLabelTextForPost(title: title, text: text!, colorTitle: colorTitle)
            }
            
        } else {
            label.text = title
        }
    }
    
    func stringToDate(date: String) -> Date {
        let formatter = DateFormatter()
        
        // Format 1
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let parsedDate = formatter.date(from: date) {
            return parsedDate
        }
        
        // Format 2
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:SSSZ"
        if let parsedDate = formatter.date(from: date) {
            return parsedDate
        }
        
        // Couldn't parsed with any format. Just get the date
        let splitedDate = date.components(separatedBy: "T")
        if splitedDate.count > 0 {
            formatter.dateFormat = "yyyy-MM-dd"
            if let parsedDate = formatter.date(from: splitedDate[0]) {
                return parsedDate
            }
        }
        
        // Nothing worked!
        return Date()
    }
    
    func getTimePost(date: Date) -> String {
        
        let day = Calendar.current.component(.day, from: Date()) - Calendar.current.component(.day, from: date)
        
        var hour = Calendar.current.component(.hour, from: Date()) - Calendar.current.component(.hour, from: date)
        
        var min = Calendar.current.component(.minute, from: Date()) - Calendar.current.component(.minute, from: date)
        
        if min < 0 {
            min = min + 60
        }
        
        if hour < 0 {
            hour = hour + 24
        }
        
        if day != 0 {
            if day == 1 {
                return "Hôm qua lúc \(date.currentTimeZoneHour())"
            } else {
                return "\(date.dayForPost()) tháng \(date.monthForPost()) lúc \(date.currentTimeZoneHour())"
            }
        }
        
        if hour > 0 {
            if hour == 1 && min < 50 {
                return "\(min) phút"
            } else {
                return "\(hour) giờ"
            }
        }
        
        if min > 0 {
            return "\(min) phút"
        } else {
            return "vừa xong"
        }
    }
    
    // search group in array group
    func searchGroupChat(groups: [Group], id: String) -> (find: Bool, index: Int) {
        for i in 0 ..< groups.count {
            if groups[i].id == id {
                return(true, i)
            }
        }
        return(false, 0)
    }
    
    // set height for post cell
    func setHeightForPostCell(posts: [Post]) -> [CGSize] {
        
        var result: [CGSize] = [CGSize]()
        
        for p in posts {
            if p.type == "startup" {
                let post = p as! Startup
                let height = Helper.sharedInstance.getHeightTextForPost(width: widthDynamicTextPost, text: "Describe Startup\n" + post.idea!, sizeFont: 14)
                if (height + 423) > 554 {
                    result.append(CGSize(width: widthCellCollectionViewPost, height: 554))
                } else {
                    result.append(CGSize(width: widthCellCollectionViewPost, height: height + 423))
                }
            } else if p.type == "event" {
                
                result.append(CGSize(width: widthCellCollectionViewPost, height: 370 - 16))
                
            } else if p.type == "experience" {
                let post = p as! Experience
                let height = Helper.sharedInstance.getHeightTextForPost(width: widthDynamicTextPost, text: "Kinh nghiệm\n" + post.content!, sizeFont: 14)
                if post.images == nil {
                    if height <= 238 {
                        result.append(CGSize(width: widthCellCollectionViewPost, height: height + 106 - 8))
                    } else {
                        result.append(CGSize(width: widthCellCollectionViewPost, height: 238 + 106 + 17 - 10))
                    }
                } else {
                    if height <= 136 {
                        result.append(CGSize(width: widthCellCollectionViewPost, height: height + 410 - 4 + 4))
                    } else {
                        result.append(CGSize(width: widthCellCollectionViewPost, height: 136 + 410 + 17))
                    }
                }
            } else {
                let post = p as! Service
                let height = Helper.sharedInstance.getHeightTextForPost(width: widthDynamicTextPost, text: post.describeService!, sizeFont: 14)
                if height <= 136 {
                    result.append(CGSize(width: widthCellCollectionViewPost, height: height + 448))
                } else {
                    result.append(CGSize(width: widthCellCollectionViewPost, height: 136 + 448 + 17))
                }
            }
        }
        return result
    }
    
    func setCellForPost(viewController: UIViewController, collectionView: UICollectionView, posts: [Post], likes: [Bool], indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if posts[indexPath.item].type == "startup" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StartupCell", for: indexPath as IndexPath) as! StartupPostViewCell
            cell.delegate = viewController as? StartupPostViewCellDelegate
            cell.currentIndexPath = indexPath as NSIndexPath!
            cell.setupView(isDetail: false, post: posts[indexPath.item], liked: likes[indexPath.item])
            cell.setupSliderShow(arrImage: posts[indexPath.item].images!)
            cell.viewFullScreen = viewController
            return cell
        } else if posts[indexPath.item].type == "event" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCell", for: indexPath as IndexPath) as! EventPostViewCell
            cell.delegate = viewController as? EventPostViewCellDelegate
            cell.indexPath = indexPath as NSIndexPath
            cell.setupViewEventWithPost(post: posts[indexPath.item])
            return cell
        } else if posts[indexPath.item].type == "experience" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExperienceCell", for: indexPath as IndexPath) as! ExperiencePostCell
            cell.currentIndexPath = indexPath as NSIndexPath!
            cell.delegate = viewController as? ExperiencePostCellDelegate
            cell.setupViewExperienceWithPost(post: posts[indexPath.item])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ServicePostCell
            cell.setupViewServicePost(post: posts[indexPath.item])
            cell.setupSliderShow(arrImage: posts[indexPath.item].images!)
            cell.viewFullScreen = viewController
            return cell
        }
    }
    
    func setupCellPostForCollectionView(collectionView: UICollectionView) {
        let nibPost = UINib(nibName: "StartupPostViewCell", bundle: nil)
        collectionView.register(nibPost, forCellWithReuseIdentifier: "StartupCell")
        
        let nibEvent = UINib(nibName: "EventPostViewCell", bundle: nil)
        collectionView.register(nibEvent, forCellWithReuseIdentifier: "EventCell")
        
        let nibExperience = UINib(nibName: "ExperiencePostCell", bundle: nil)
        collectionView.register(nibExperience, forCellWithReuseIdentifier: "ExperienceCell")
        
        let nibService = UINib(nibName: "ServicePostCell", bundle: nil)
        collectionView.register(nibService, forCellWithReuseIdentifier: "ServiceCell")
    }
    
    func getViewReload(type: TypeViewPost) -> String {
        if type == .following {
            return tReloadFollowing
        } else if type == .startup {
            return tReloadStartup
        } else if type == .event {
            return tReloadEvent
        } else if type == .experience {
            return tReloadExperience
        } else if type == .service {
            return tReloadService
        } else {
            return tReloadReview
        }
    }
    
    func displayImageWithURL(imageView: UIImageView, url: URL) {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                imageView.sd_setImage(with: url)
            }
        }
    }

}

extension Helper {
    func showMessagesSuccess(_ message: String, completionHandle:@escaping (_ success: Bool) -> ()) {
        let alert = ISMessages.cardAlert(withTitle: nil,
                                         message: message,
                                         iconImage: nil,
                                         duration: 1,
                                         hideOnSwipe: true,
                                         hideOnTap: true,
                                         alertType: .custom,
                                         alertPosition: .top)
        alert?.messageLabelFont = UIFont.boldSystemFont(ofSize: 14)
        alert?.messageLabelTextColor = .white
        alert?.alertViewBackgroundColor = UIColor(red: 0/255, green: 153/255, blue: 204/255, alpha: 1)
        alert?.show({
            print("Show")
        }, didHide: { (finished) in
            completionHandle(true)
        })
    }
}

extension Helper {
    
    func setArrayColletionViewLayout(collectionView: CGSize, arrImage: [Image]) -> [PhotoLayout] {
        var arrRectLayout = [PhotoLayout]()
        if arrImage.count == 1 {
            arrRectLayout = setOneColletionViewLayout(collectionView: collectionView)
        } else if arrImage.count == 2 {
            arrRectLayout = setTwoColletionViewLayout(collectionView: collectionView, arrImage: arrImage)
        } else if arrImage.count == 3 {
            arrRectLayout = setThreeColletionViewLayout(collectionView: collectionView, arrImage: arrImage)
        } else if arrImage.count == 4 {
            arrRectLayout = setFourColletionViewLayout(collectionView: collectionView, arrImage: arrImage)
        }
        return arrRectLayout
    }
    
    func setOneColletionViewLayout(collectionView: CGSize) -> [PhotoLayout] {
        var arrRectLayout = [PhotoLayout]()
        let layout = PhotoLayout(xOffset: 0, yOffset: 0, width: collectionView.width, height: collectionView.height)
        arrRectLayout.append(layout)
        return arrRectLayout
    }
    
    func setTwoColletionViewLayout(collectionView: CGSize, arrImage: [Image]) -> [PhotoLayout] {
        var arrRectLayout = [PhotoLayout]()
        if (checkImageHorizontal(image: arrImage[0])) && (checkImageHorizontal(image: arrImage[1])) {
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: 0, width: collectionView.width, height: collectionView.height / 2 - 1))
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: collectionView.height / 2 + 1, width: collectionView.width, height: collectionView.height / 2 - 1))
        } else {
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: 0, width: collectionView.width / 2 - 1, height: collectionView.height))
            arrRectLayout.append(PhotoLayout(xOffset: collectionView.width / 2 + 1, yOffset: 0, width: collectionView.width / 2 - 1, height: collectionView.height))
        }
        return arrRectLayout
    }
    
    func setThreeColletionViewLayout(collectionView: CGSize, arrImage: [Image]) -> [PhotoLayout] {
        var arrRectLayout = [PhotoLayout]()
        if (checkImageHorizontal(image: arrImage[0])) {
            arrRectLayout = setThreeVerticalColletionViewLayout(collectionView: collectionView, arrImage: arrImage)
        } else {
            arrRectLayout = setThreeHorizontalColletionViewLayout(collectionView: collectionView, arrImage: arrImage)
        }
        return arrRectLayout
    }
    
    func setThreeHorizontalColletionViewLayout(collectionView: CGSize, arrImage: [Image]) -> [PhotoLayout] {
        var arrRectLayout = [PhotoLayout]()
        if (checkImageHorizontal(image: arrImage[1]) || checkImageHorizontal(image: arrImage[2])) {
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: 0, width: collectionView.width / 2 - 1, height: collectionView.height))
            arrRectLayout.append(PhotoLayout(xOffset: collectionView.width / 2 + 1, yOffset: 0, width: collectionView.width / 2 - 1, height: collectionView.height / 2 - 1))
            arrRectLayout.append(PhotoLayout(xOffset: collectionView.width / 2 + 1, yOffset: collectionView.height / 2 + 1, width: collectionView.width / 2 - 1, height: collectionView.height / 2 - 1))
        } else {
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: 0, width: collectionView.width / 3 * 2 - 1, height: collectionView.height))
            arrRectLayout.append(PhotoLayout(xOffset: collectionView.width / 3 * 2 + 1, yOffset: 0, width: collectionView.width / 3 - 1, height: collectionView.height / 2 - 1))
            arrRectLayout.append(PhotoLayout(xOffset: collectionView.width / 3 * 2 + 1, yOffset: collectionView.height / 2 + 1, width: collectionView.width / 3 - 1, height: collectionView.height / 2 - 1))
        }
        return arrRectLayout
    }
    
    func setThreeVerticalColletionViewLayout(collectionView: CGSize, arrImage: [Image]) -> [PhotoLayout] {
        var arrRectLayout = [PhotoLayout]()
        if (checkImageHorizontal(image: arrImage[1]) || checkImageHorizontal(image: arrImage[2])) {
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: 0, width: collectionView.width, height: collectionView.height / 3 * 2 - 1))
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: collectionView.height / 3 * 2 + 1, width: collectionView.width / 2 - 1, height: collectionView.height / 3 - 1))
            arrRectLayout.append(PhotoLayout(xOffset: collectionView.width / 2 + 1, yOffset: collectionView.height / 3 * 2 + 1, width: collectionView.width / 2 - 1, height: collectionView.height / 3 - 1))
        } else {
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: 0, width: collectionView.width, height: collectionView.height / 2 - 1))
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: collectionView.height / 2 + 1, width: collectionView.width / 2 - 1, height: collectionView.height / 2 - 1))
            arrRectLayout.append(PhotoLayout(xOffset: collectionView.width / 2 + 1, yOffset: collectionView.height / 2 + 1, width: collectionView.width / 2 - 1, height: collectionView.height / 2 - 1))
        }
        return arrRectLayout
    }
    
    func setFourColletionViewLayout(collectionView: CGSize, arrImage: [Image]) -> [PhotoLayout] {
        var arrRectLayout = [PhotoLayout]()
        let width = collectionView.width / 3
        let height = collectionView.height / 3
        if (checkImageHorizontal(image: arrImage[0])) {
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: 0, width: collectionView.width, height: collectionView.height - height - 1))
            for i in 0..<3 {
                arrRectLayout.append(PhotoLayout(xOffset: CGFloat(i) * width + CGFloat(i), yOffset: collectionView.height - height + 1, width: width - 1, height: height - 1))
            }
        } else {
            arrRectLayout.append(PhotoLayout(xOffset: 0, yOffset: 0, width: collectionView.width - width - 1, height: collectionView.height))
            for i in 0..<3 {
                arrRectLayout.append(PhotoLayout(xOffset: collectionView.width - width + 1, yOffset: CGFloat(i) * height + CGFloat(i), width: width - 1, height: height - 1))
            }
        }
        return arrRectLayout
    }
}
