//
//  Defintions.swift
//  PimSocial
//
//  Created by Pim on 8/3/16.
//  Copyright © 2016 Pim. All rights reserved.
//

import Foundation
import UIKit

// URL Sever
let referenceURL: String                =   "http://192.168.1.6:3000"
let avatarDefault: String               =   "https://firebasestorage.googleapis.com/v0/b/piminium.appspot.com/o/avatars%2Favatar-default.png?alt=media&token=1d020198-fac0-424a-88e2-a99857ca9d6c"

// API reference
let referenceLogin: String              =   "/api/login"
let referenceUser: String               =   "/api/user"
let referencePerson: String             =   "/api/person"
let referenceAvatar: String             =   "/api/avatar"
let referenceCheck: String              =   "/api/check"
let referenceUploadAvatar: String       =   "/api/avatar"
let referencePost                       =   "/api/post"
let referenceStartup                    =   "/api/post/startup"
let referenceEvent                      =   "/api/post/event"
let referenceExperience                 =   "/api/post/experience"
let referenceService                    =   "/api/post/service"
let referenceLike                       =   "/api/post/like"
let referenceComment                    =   "/api/post/comment"
let referencePostUser                   =   "/api/post/user"
let referencePostReview                 =   "/api/post/review"
let referenceFollow                     =   "/api/follow"
let referenceFollowing                  =   "/api/following"
let referenceGroup                      =   "/api/group"
let referenceMessage                    =   "/api/message"
let referenceReview                     =   "/api/review"
let referenceSearchStartup              =   "/api/search/startup"

// Get Device Info
let heightDevice: CGFloat               =   CheckDevice.TheCurrentDeviceHeight
let widthDevice: CGFloat                =   CheckDevice.TheCurrentDeviceWidth
let is_iPhone: Bool                     =   CheckDevice.is_iPhone

// Get size collectionView Post
let sizeEdgeInsetsMakePost: CGFloat = is_iPhone ? 8 : 96
let widthCellCollectionViewPost: CGFloat = is_iPhone ? (CheckDevice.TheCurrentDeviceWidth - (sizeEdgeInsetsMakePost * 2)) : (CheckDevice.TheCurrentDeviceWidth - (sizeEdgeInsetsMakePost * 2))
let widthDynamicTextPost: CGFloat = widthCellCollectionViewPost - 16

let sizeEdgeInsetsMake: CGFloat = is_iPhone ? 0 : 96
let widthCellCollectionView: CGFloat = is_iPhone ? (CheckDevice.TheCurrentDeviceWidth - (sizeEdgeInsetsMake * 2)) : (CheckDevice.TheCurrentDeviceWidth - (sizeEdgeInsetsMake * 2))
let widthDynamicText: CGFloat = widthCellCollectionView - 16

// Key database
let kUsers: String                      =   "users"
let kUser: String                       =   "user"
let kIdUser: String                     =   "id"
let kName: String                       =   "name"
let kEmail: String                      =   "email"
let kUsername: String                   =   "username"
let kPassword: String                   =   "password"
let kAuthToken: String                  =   "auth_token"
let kImage: String                      =   "imageURL"
let kUIDUser: String                    =   "uid"
let kFollow: String                     =   "follow"
let kFollowers: String                  =   "followers"
let kFollowing: String                  =   "following"
let kPosts: String                      =   "posts"
let kPost: String                       =   "post"
let kSuccess: String                    =   "success"
let kComments: String                   =   "comments"
let kComment: String                    =   "comment"
let kGroup: String                      =   "group"
let kGroups: String                     =   "groups"
let kMessage: String                    =   "message"
let kMessages: String                   =   "messages"
let kReview: String                     =   "review"

// key type post
let kStartup: String = "startup"
let kEvent: String = "event"
let kExperience: String = "experience"
let kService: String = "service"
let kStatus: String = "status"

// Menu categogy post
let kMCStartup: String = "Khởi nghiệp"
let kMCEvent: String = "Sự kiện"
let kMCShare: String = "Chia sẻ"
let kMCService: String = "Dịch vụ"
let kMCStatus: String = "Trạng thái"
let kMCSupport: String = "Hỗ trợ"
let kMCExperience: String = "Kinh nghiệm"

// key text key for property Startup
let kSUIdea: String = "idea-Startup"
let kSUTeam: String = "team-Startup"
let kSUFields: String = "fields-Startup"
let kSUUser: String = "user-Startup"
let kSUDevelopment: String = "development-Startup"
let kSUProduct: String = "product-Startup"
let kSUDescribe: String = "describe-Startup"

// key text for property Experience
let kSUContentExperience: String = "content-Experience"

// key text for User
let kSUAboutUser: String = "about-User"

// text for property Startup
let kTitleIdea: String = "Ý tưởng khởi nghiệp"
let kTitleTeam: String = "Đội ngũ quản lý"
let kTitleFields: String = "Lĩnh vực hoạt động"
let kTitleUser: String = "Đối tượng người dùng"
let kTitleDevelopment: String = "Định hướng phát triển"
let kTitleProduct: String = "Mô tả sản phẩm"
let kTitleReview: String = "Mô tả đánh giá dự án"

// text for property Startup
let kTitleDescribe: String = "Chi tiết sự kiện"

// text for property Experience
let kTitleContentExperience: String = "Nội dung"

// text for property User
let kTitleAbout: String = "Giới thiệu bản thân"

let NAVBAR_CHANGE_POINT: CGFloat = 0

var tabBarSelected: TypeView = .newsFeed

let attributedTitle = [ NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)]
let attributedContent = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 14)]

let blueText = UIColor(red: 39/255, green: 151/255, blue: 238/255, alpha: 1)
let colorNavigation = UIColor(red: 60/255, green: 179/255, blue: 133/255, alpha: 1)
let colorPlaceholder = UIColor(red: 104/255, green: 104/255, blue: 104/255, alpha: 0.75)
let colorTextFieldPlaceholder = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)

let tReloadFollowing: String = "reload-following"
let tReloadStartup: String = "reload-startup"
let tReloadEvent: String = "reload-event"
let tReloadExperience: String = "reload-experience"
let tReloadService: String = "reload-service"
let tReloadReview: String = "reload-review"

enum TypeView {
    case newsFeed
    case message
    case notification
    case profile
}

enum TypePost {
    case startup
    case event
    case experience
    case service
    case status
    case review
}

enum TypeViewPost {
    case following
    case startup
    case event
    case experience
    case service
}

var user_current: User?

var postStartup: Startup?
var postEvent: Event?
var postExperience: Experience?
var reviewStartup: String?
