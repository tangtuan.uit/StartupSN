//
//  ViewController+Helper.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 1/18/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func setBackgroundViewController(_ image: UIImage) {
        UIGraphicsBeginImageContext(self.view.frame.size)
        image.draw(in: self.view.bounds)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: img)
    }

}
