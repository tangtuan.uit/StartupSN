//
//  NotificationName+Helper.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 5/15/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let sentMessage = Notification.Name("sent-message")
}
