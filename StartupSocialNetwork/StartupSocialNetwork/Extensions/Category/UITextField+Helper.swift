//
//  TextField+Helper.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/1/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

extension UITextField {
    
    func setColorPlaceholder(text: String, color: UIColor) {
        let str = NSAttributedString(string: text, attributes: [NSForegroundColorAttributeName: color])
        self.attributedPlaceholder = str
    }

}
