//
//  UILabel+Helper.swift
//  Piminium
//
//  Created by Pim on 9/2/16.
//  Copyright © 2016 Pim. All rights reserved.
//

import UIKit

extension UILabel {
    
    func setTextForLabelHeaderView(number: Int, text: String) {
        
        let myAttribute1 = [ NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)]
        let myAttrString = NSMutableAttributedString(string: String(number), attributes: myAttribute1)
        
        let myAttribute2 = [ NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 11)]
        let attrString = NSMutableAttributedString(string: "\n" + text, attributes: myAttribute2)
        
        myAttrString.append(attrString)
        
        self.attributedText = myAttrString
    }
    
    func setTextForTitlePost(like: Int, title: String, time: String) {
        
        let likeAttribute = [ NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 14.0)!]
        let myAttrString = NSMutableAttributedString(string: "♥ \(like) likes", attributes: likeAttribute)
        
        if !title.isEmpty {
            let titleAttribute = [ NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont(name: "Roboto-Regular", size: 14.0)!]
            let titleAttrString = NSMutableAttributedString(string: "\n\(title)", attributes: titleAttribute)
            myAttrString.append(titleAttrString)
        }
        
        let timeAttribute = [ NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 12.0)!]
        let timeAttrString = NSMutableAttributedString(string: "\n\(time)" , attributes: timeAttribute)
        myAttrString.append(timeAttrString)
        
        self.attributedText = myAttrString
    }
    
    func setTextForCellProfile(name: String, username: String) {
        let attributedText = NSMutableAttributedString()
        
        let attributedStringName = NSAttributedString(string: name, attributes: [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 16)])
        let attributedStringUsername = NSAttributedString(string: "\n\(username)", attributes: [NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 12)])
        
        attributedText.append(attributedStringName)
        attributedText.append(attributedStringUsername)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1
        
        attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: attributedText.string.characters.count))
        
        self.attributedText = attributedText
    }
    
    func setTextForDateEventPost() {
        
        let myAttribute1 = [ NSForegroundColorAttributeName: UIColor.orange, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18)]
        let myAttrString = NSMutableAttributedString(string: "Mar", attributes: myAttribute1)
        
        let myAttribute2 = [ NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 32)]
        let attrString = NSMutableAttributedString(string: "\n16", attributes: myAttribute2)
        
        let myAttribute3 = [ NSForegroundColorAttributeName: UIColor.red, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18)]
        let attrString2 = NSMutableAttributedString(string: "\n2017", attributes: myAttribute3)
        
        myAttrString.append(attrString)
        myAttrString.append(attrString2)
        
        self.attributedText = myAttrString
    }
    
    func setImageForLabel(image: UIImage, text: String) {
        
        let attributedText = NSMutableAttributedString()
        let attachmentPhone = NSTextAttachment()
        attachmentPhone.image = image
        attachmentPhone.bounds = CGRect(x: 0, y: -3, width: 16, height: 16)
        
        attributedText.append(NSAttributedString(attachment: attachmentPhone))
        
        let attributedStringPhone = NSAttributedString(string: " " + text, attributes: [NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        
        attributedText.append(attributedStringPhone)
        
        self.attributedText = attributedText
    }
    
    func setUsernamForEventPost(username: String) {
        
        let attributedText = NSMutableAttributedString()
        let attributedStringUsername = NSAttributedString(string: username, attributes: [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)])
        let attributedStringText = NSAttributedString(string: " đã thêm một ", attributes: [NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        
        let attributedStringText2 = NSAttributedString(string: "sự kiện", attributes: [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)])
        
        let attributedStringLocation = NSAttributedString(string: "\n12 phút • Tân Bình", attributes: [NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 12)])
        
        attributedText.append(attributedStringUsername)
        attributedText.append(attributedStringText)
        attributedText.append(attributedStringText2)
        attributedText.append(attributedStringLocation)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2
        
        attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: attributedText.string.characters.count))
        
        self.attributedText = attributedText
    }
    
    func setUsernamForStartupPost(username: String, time: String) {
        
        let attributedText = NSMutableAttributedString()
        let attributedStringUsername = NSAttributedString(string: username, attributes: [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)])
        
        let attributedStringLocation = NSAttributedString(string: "\n \(time)", attributes: [NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 12)])
        
        attributedText.append(attributedStringUsername)
        attributedText.append(attributedStringLocation)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2
        
        attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: attributedText.string.characters.count))
        
        self.attributedText = attributedText
    }
    
    func setTextForCareAndJoin(text: String, number: Int) {
        
        let attributedText = NSMutableAttributedString()
        let attributedStringText = NSAttributedString(string: text, attributes: [NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 12)])
        let attributedStringNumber = NSAttributedString(string: "\n\(number)", attributes: [NSForegroundColorAttributeName: UIColor.blue, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 28)])
        attributedText.append(attributedStringText)
        attributedText.append(attributedStringNumber)
        self.attributedText = attributedText
    }
    
    func setTitleForEventDetail(post: Event) {
        let myAttrString = NSMutableAttributedString()
        let attTitleDescribe = NSMutableAttributedString(string: post.title!, attributes: [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 17)])
        myAttrString.append(attTitleDescribe)
        let attDescrible = NSMutableAttributedString(string: "\nĐược tổ chức bởi ", attributes: [NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        myAttrString.append(attDescrible)
        let attName = NSMutableAttributedString(string: post.organizer!, attributes: [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)])
        myAttrString.append(attName)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        
        myAttrString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: myAttrString.string.characters.count))
        
        self.attributedText = myAttrString
        
    }
    
    func setTextForTextViewDescribe(text: String) {
        let textDescribe = "Ý tưởng"
        let myAttribute1 = [ NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)]
        let myAttrString = NSMutableAttributedString(string: textDescribe, attributes: myAttribute1)
        
        let myAttribute2 = [ NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        let attrString = NSMutableAttributedString(string: "\n" + text, attributes: myAttribute2)
        
        myAttrString.append(attrString)
        
        self.attributedText = myAttrString
    }
    
    func setTextForTextViewDetail(post: Startup) {
        
        let myAttrString = NSMutableAttributedString()
        
        // set Describle Startup
        let attTitleDescribe = NSMutableAttributedString(string: "Ý tưởng:", attributes: attributedTitle)
        myAttrString.append(attTitleDescribe)
        let attDescribleStartup = NSMutableAttributedString(string: "\n" + post.idea!, attributes: attributedContent)
        myAttrString.append(attDescribleStartup)
        
        // set My Team
        let attTitleMyTeam = NSMutableAttributedString(string: "\n\nĐội ngũ quản lý:", attributes: attributedTitle)
        myAttrString.append(attTitleMyTeam)
        let attmyTeam = NSMutableAttributedString(string: "\n" + post.team!, attributes: attributedContent)
        myAttrString.append(attmyTeam)
        
        // set fields
        let attTitleFields = NSMutableAttributedString(string: "\n\nLĩnh vực hoạt động:", attributes: attributedTitle)
        myAttrString.append(attTitleFields)
        let attFields = NSMutableAttributedString(string: "\n" + post.fields!, attributes: attributedContent)
        myAttrString.append(attFields)
        
        // set user object
        let attTitleUserObject = NSMutableAttributedString(string: "\n\nĐối tượng người dùng:", attributes: attributedTitle)
        myAttrString.append(attTitleUserObject)
        let attUserObject = NSMutableAttributedString(string: "\n" + post.userObject!, attributes: attributedContent)
        myAttrString.append(attUserObject)
        
        // set user object
        let attTitleOrientedDevelopment = NSMutableAttributedString(string: "\n\nĐịnh hướng pháp triển:", attributes: attributedTitle)
        myAttrString.append(attTitleOrientedDevelopment)
        let attOrientedDevelopment = NSMutableAttributedString(string: "\n" + post.orientedDevelopment!, attributes: attributedContent)
        myAttrString.append(attOrientedDevelopment)
        
        // set user object
        let attTitleDescribeProduct = NSMutableAttributedString(string: "\n\nMô tả dự án khởi nghiệp:", attributes: attributedTitle)
        myAttrString.append(attTitleDescribeProduct)
        let attDescribeProduct = NSMutableAttributedString(string: "\n" + post.describeProduct!, attributes: attributedContent)
        myAttrString.append(attDescribeProduct)
        
        self.attributedText = myAttrString
    }
    
    func setTextForTextViewEvent(post: Event) {
        let myAttrString = NSMutableAttributedString()
        
        let attTitle = NSMutableAttributedString(string: "Thông tin sự kiện", attributes: attributedTitle)
        myAttrString.append(attTitle)
        
        // set Describle Startup
        let attTitleAction = NSMutableAttributedString(string: "\n\nDiễn ra vào: ", attributes: attributedTitle)
        myAttrString.append(attTitleAction)
        let attTitleDate = NSMutableAttributedString(string: "thứ 7 ngày 1 tháng 4", attributes: attributedContent)
        myAttrString.append(attTitleDate)
        
        let attDescribleStart = NSMutableAttributedString(string: "\n\nBắt đầu từ: ", attributes: attributedTitle)
        myAttrString.append(attDescribleStart)
        let attDescribleTime = NSMutableAttributedString(string: post.timeStart! + " đến " + post.timeEnd!, attributes: attributedContent)
        myAttrString.append(attDescribleTime)
        
        // set contact
        let attContact = NSMutableAttributedString(string: "\n\nLiên hệ: ", attributes: attributedTitle)
        myAttrString.append(attContact)
        let attPhone = NSMutableAttributedString(string: post.phone!, attributes: attributedContent)
        myAttrString.append(attPhone)
        
        // set My Team
        let attTitleLocation = NSMutableAttributedString(string: "\n\nĐịa điểm: ", attributes: attributedTitle)
        myAttrString.append(attTitleLocation)
        let attLocation = NSMutableAttributedString(string: post.locationEvent!, attributes: attributedContent)
        myAttrString.append(attLocation)
        
        // set fieldsC
        let attTitleDetail = NSMutableAttributedString(string: "\n\nChi tiết sự kiện", attributes: attributedTitle)
        myAttrString.append(attTitleDetail)
        let attdescribeEvent = NSMutableAttributedString(string: "\n" + post.describeEvent!, attributes: attributedContent)
        myAttrString.append(attdescribeEvent)
        
        self.attributedText = myAttrString
    }
    
    func setTextForTextViewExperience(post: Experience) {
        let myAttrString = NSMutableAttributedString()
        let textTitle: String!
        
        if post.isQuestion {
            textTitle = "Câu hỏi"
        } else {
            textTitle = "Kinh nghiệm"
        }
        
        let attTitle = NSMutableAttributedString(string: textTitle, attributes: attributedTitle)
        myAttrString.append(attTitle)
        

        let attTitleContent = NSMutableAttributedString(string: "\n" + post.content!, attributes: attributedContent)
        myAttrString.append(attTitleContent)
        
        self.attributedText = myAttrString
        
    }
    
    func setLabelLocationService(post: Service) {
        let myAttrString = NSMutableAttributedString()
        
        let attTitle = NSMutableAttributedString(string: post.nameStore!, attributes: [ NSForegroundColorAttributeName: blueText, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 13)])
        myAttrString.append(attTitle)
        
        
        let attTitleContent = NSMutableAttributedString(string: "\n" + post.localtionStore!, attributes: [ NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 12)])
        myAttrString.append(attTitleContent)
        
        self.attributedText = myAttrString
    }
    
    func setLabelTextForPost(title: String, text: String, colorTitle: UIColor) {
        let myAttrString = NSMutableAttributedString()
        
        let attTitle = NSMutableAttributedString(string: title, attributes: [ NSForegroundColorAttributeName: colorTitle, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 15)])
        myAttrString.append(attTitle)
        
        let attTitleContent = NSMutableAttributedString(string: "\n" + text, attributes: [ NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 15)])
        myAttrString.append(attTitleContent)
        
        self.attributedText = myAttrString
    }
    
    func setLabelTextForAboutUser(title: String, text: String ) {
        let myAttrString = NSMutableAttributedString()
        
        let attTitle = NSMutableAttributedString(string: title, attributes: [ NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 13)])
        myAttrString.append(attTitle)
        
        let attTitleContent = NSMutableAttributedString(string: "\n" + text, attributes: [ NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        myAttrString.append(attTitleContent)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2
        
        myAttrString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: myAttrString.string.characters.count))
        
        self.attributedText = myAttrString
    }

    
}








