//
//  String+Helper.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/28/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation

extension String {
    func replace(target: String, withString: String) -> String {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
}
