//
//  UIImageView+Helper.swift
//  Piminium
//
//  Created by Pim on 9/2/16.
//  Copyright © 2016 Pim. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setCircularAvatar() {
        self.layer.frame = self.layer.frame.insetBy(dx: 0, dy: 0)
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = self.frame.height/2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
        self.layer.borderWidth = 4
        self.contentMode = UIViewContentMode.scaleAspectFill
    }
    
    func setCircularImage() {
        self.layer.frame = self.layer.frame.insetBy(dx: 0, dy: 0)
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = self.frame.height/2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
        self.layer.borderWidth = 0
        self.contentMode = UIViewContentMode.scaleAspectFill
    }
    
    func addBlurEffect()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
}
