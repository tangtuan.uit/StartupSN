//
//  APIClient+Helper.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/28/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Foundation
import Alamofire
import Mantle

extension APIClient {
    
    func getPostsFromJson(json: AnyObject) -> (post: [Post], likeds: [Bool]) {
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        var users: [User] = [User]()
        
        // get array user for post
        if json.value(forKey: kUsers) != nil {
            let arrUser = json.value(forKey: kUsers) as! NSArray
            for user in arrUser {
                let userObject = user as! Dictionary<String, AnyObject>
                do {
                    let userInfo = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
                    users.append(userInfo)
                } catch let error as NSError {
                    print(error)
                }
            }
        }
        
        // get array bool
        if json.value(forKey: "liked") != nil {
            let arrLiked = json.value(forKey: "liked") as! NSArray
            for like in arrLiked {
                likeds.append(like as! Bool)
            }
        }
        
        // get array post
        if json.value(forKey: kPosts) != nil {
            let arrPost = json.value(forKey: kPosts) as! NSArray
            for post in arrPost {
                let postObject = post as! Dictionary<String, AnyObject>
                do {
                    let postInfo = try MTLJSONAdapter.model(of: Post.self, fromJSONDictionary: postObject) as! Post
                    for user in users {
                        let id = postObject["id_user"] as! String
                        if user.id == id {
                            postInfo.user = user
                            break
                        }
                    }
                    var images: [Image] = [Image]()
                    let arrImage = postObject["images"] as! NSArray
                    if arrImage.count > 0 {
                        for image in arrImage {
                            let imageObject = image as! Dictionary<String, AnyObject>
                            do {
                                let imageInfo = try MTLJSONAdapter.model(of: Image.self, fromJSONDictionary: imageObject) as! Image
                                images.append(imageInfo)
                            } catch let error as NSError {
                                print(error)
                            }
                        }
                        postInfo.images = images
                    }
                    posts.append(postInfo)
                } catch let error as NSError {
                    print(error)
                }
            }
        }
        
        return (posts, likeds)
    }
    
    func getPostFromJson(json: AnyObject) -> Post {
        var post: Post = Post()
        var user: User = User()
        // get array user for post
  
        let userObject = json.value(forKey: kUser) as! Dictionary<String, AnyObject>
        do {
            let userInfo = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
            user = userInfo
        } catch let error as NSError {
            print(error)
        }
        
        // get array post
        let postObject = json.value(forKey: kPost) as! Dictionary<String, AnyObject>
        do {
            let postInfo = try MTLJSONAdapter.model(of: Post.self, fromJSONDictionary: postObject) as! Post
            postInfo.user = user
            var images: [Image] = [Image]()
            let arrImage = postObject["images"] as! NSArray
            if arrImage.count > 0 {
                for image in arrImage {
                    let imageObject = image as! Dictionary<String, AnyObject>
                    do {
                        let imageInfo = try MTLJSONAdapter.model(of: Image.self, fromJSONDictionary: imageObject) as! Image
                        images.append(imageInfo)
                    } catch let error as NSError {
                        print(error)
                    }
                }
                postInfo.images = images
            }
            post = postInfo
        } catch let error as NSError {
            print(error)
        }
        
        return post
    }

    func getPostsReviewFromJson(json: AnyObject) -> [Startup] {
        var posts: [Startup] = [Startup]()
        var users: [User] = [User]()
        var usersReview: [User] = [User]()
        // get array user for post
        let arrUser = json.value(forKey: kUsers) as! NSArray
        for user in arrUser {
            let userObject = user as! Dictionary<String, AnyObject>
            do {
                let userInfo = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
                if (userInfo.state) == "expert" {
                    usersReview.append(userInfo)
                } else {
                    users.append(userInfo)
                }
            } catch let error as NSError {
                print(error)
            }
        }
        
        // get array post
        let arrPost = json.value(forKey: kPosts) as! NSArray
        for post in arrPost {
            let postObject = post as! Dictionary<String, AnyObject>
            do {
                let postInfo = try MTLJSONAdapter.model(of: Post.self, fromJSONDictionary: postObject) as! Post
                for user in users {
                    let id = postObject["id_user"] as! String
                    if user.id == id {
                        postInfo.user = user
                        break
                    }
                }
                var images: [Image] = [Image]()
                let arrImage = postObject["images"] as! NSArray
                if arrImage.count > 0 {
                    for image in arrImage {
                        let imageObject = image as! Dictionary<String, AnyObject>
                        do {
                            let imageInfo = try MTLJSONAdapter.model(of: Image.self, fromJSONDictionary: imageObject) as! Image
                            images.append(imageInfo)
                        } catch let error as NSError {
                            print(error)
                        }
                    }
                    postInfo.images = images
                }
                
                let startup = postInfo as! Startup
                let reviewObject = postObject["review"] as! Dictionary<String, AnyObject>
                do {
                    let reviewInfo = try MTLJSONAdapter.model(of: Review.self, fromJSONDictionary: reviewObject) as! Review
                    startup.review = reviewInfo
                    for user in usersReview {
                        let id = reviewObject["id_user"] as! String
                        if user.id == id {
                            startup.review?.user = user
                            break
                        }
                    }
                }
                catch let error as NSError {
                    print(error)
                }

                posts.append(startup)
            } catch let error as NSError {
                print(error)
            }
        }
        
        return posts
    }
    
}
