//
//  StoryboardManager.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/20/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class StoryboardManager: NSObject {
    
    static let sharedInstance: StoryboardManager = { StoryboardManager() }()
    
    func getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: String,storyboardName: String) ->UIViewController{
        
        let stb = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = stb.instantiateViewController(withIdentifier: viewControllerIdentifier)
        
        return vc
    }
    
    func getViewControllerInitial(storyboardName: String) ->UIViewController {
        
        let mainView: UIStoryboard! = UIStoryboard(name:storyboardName, bundle: nil)
        //let viewcontroller : UIViewController = (mainView.instantiateInitialViewController() as! UIViewController)
        let viewcontroller : UIViewController = mainView.instantiateInitialViewController()!
        return viewcontroller
    }
    
    func pushViewDetail(post: Post, liked: Bool, viewController: UIViewController) {
        let vcDetail = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "DetailViewControllerIdentifier", storyboardName: "Dashboard") as! DetailViewController
        vcDetail.post = post
        vcDetail.liked = liked
        vcDetail.hidesBottomBarWhenPushed = true
        viewController.navigationController?.pushViewController(vcDetail, animated: true)
    }
    
    func pushViewComment(post: Post, viewController: UIViewController) {
        let vcComment = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "CommentViewControllerIdentifier", storyboardName: "Dashboard") as! CommentViewController
        vcComment.post = post
        vcComment.hidesBottomBarWhenPushed = true
        viewController.navigationController?.pushViewController(vcComment, animated: true)
    }
    
    func pushViewProfile(viewController: UIViewController) {
        let vcProfile = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "ProfileViewControllerIdentifier", storyboardName: "Dashboard") as! ProfileViewController
        vcProfile.isTab = false
        viewController.navigationController?.pushViewController(vcProfile, animated: true)
    }
    
    func pushViewPerson(id: String, viewController: UIViewController) {
        let vcPerson = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "PersonViewControllerIdentifier", storyboardName: "Dashboard") as! PersonViewController
//        vcPerson.idUser = id
        vcPerson.navigationController?.pushViewController(vcPerson, animated: true)
    }
}
