//
//  APIClient.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/3/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import Mantle
import Alamofire

class APIClient {
    
    static let sharedInstance: APIClient = { APIClient() }()
    
    func login(_ email: String, _ password: String , completionHandle:@escaping (_ success: Bool, _ user: User) -> (),failure:(_ error:NSError) -> ()) {
        var user = User()
        let url: String = referenceURL + referenceLogin
        let params: Dictionary<String,String> = [
            kEmail: email,
            kPassword: password
        ]
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, user!)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let userObject = json.value(forKey: kUser) as! Dictionary<String, AnyObject>
                do {
                    user = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as? User
                    if !KeyChain.checkKeyChainStore(user: user!) {
                        KeyChain.saveKeyChainStore(user: user!)
                    }
                    completionHandle(true, user!)
                } catch let error as NSError {
                    completionHandle(false, user!)
                    print(error)
                }
            }
        }
    }
    
    func signup(_ user: User, _ password: String , completionHandle:@escaping (_ success: Bool, _ user: User) -> (),failure:(_ error:NSError) -> ()) {
        var result = User()
        let url: String = referenceURL + referenceUser
        var params: Dictionary<String, Any> = user.coverToDictionary()
        
        params["password"] = password
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, result!)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let userObject = json.value(forKey: kUser) as! Dictionary<String, AnyObject>
                do {
                    result = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as? User
                    if !KeyChain.checkKeyChainStore(user: result!) {
                        KeyChain.saveKeyChainStore(user: result!)
                    }
                    completionHandle(true, result!)
                } catch let error as NSError {
                    completionHandle(false, result!)
                    print(error)
                }
            }
        }
    }

    func getUser(_ id: String , completionHandle:@escaping (_ success: Bool, _ user: User) -> (),failure:(_ error:NSError) -> ()) {
        var result = User()
        let url: String = referenceURL + referenceUser
        let params = [
            "id": id
        ]
        
        Alamofire.request(url, method: .get, headers: params).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false, result!)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let userObject = json.value(forKey: kUser) as! Dictionary<String, AnyObject>
                do {
                    result = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as? User
                    completionHandle(true, result!)
                } catch let error as NSError {
                    completionHandle(false, result!)
                    print(error)
                }
            }
        }
    }
    
    func update(_ user: User , completionHandle:@escaping (_ success: Bool, _ result: User) -> (),failure:(_ error:NSError) -> ()) {
        var userInfo = User()
        let url: String = referenceURL + referenceUser
        let param = user.coverToDictionary()
        
        Alamofire.request(url, method: .put, parameters: param).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false, userInfo!)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let userObject = json.value(forKey: kUser) as! Dictionary<String, AnyObject>
                do {
                    userInfo = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as? User
                    completionHandle(true, userInfo!)
                } catch let error as NSError {
                    completionHandle(false, userInfo!)
                    print(error)
                }
            }
        }
    }

    func uploadAvatar(_ image: UIImage? , completionHandle:@escaping (_ success: Bool, _ path: String) -> (),failure:(_ error:NSError) -> ()) {
        

        let url = referenceURL + referenceUploadAvatar
        let urlRequest = try! URLRequest(url: url, method: .post)
        
        if image != nil {
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if image != nil {
                    let data = UIImageJPEGRepresentation(image!, 1)
                    multipartFormData.append(data!,
                                             withName: "avatar",
                                             fileName: "fileName.png",
                                             mimeType: "image/jpg")
                }
            }, with: urlRequest, encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let json = response.result.value as AnyObject
                        if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                            let imageDictionary = json.value(forKey: "image") as AnyObject
                            let path = imageDictionary.value(forKey: "path") as! String
                            completionHandle(true, path)
                        } else {
                            completionHandle(false, "")
                        }
                    }
                case .failure( _):
                    completionHandle(false, "")
                }
            })
        } else {
            completionHandle(false, "")
        }
  
    }
    
    func getPerson(idUser: String, idPerson: String, page: Int, completionHandle:@escaping (_ success: Bool, _ user: User , _ posts: [Post], _ likeds: [Bool], _ follow: String) -> (),failure:(_ error:NSError) -> ()) {
        
        
        var user: User = User()
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        var follow: String = ""
        let header = [
            "id_user": idUser,
            "id_person" : idPerson,
            "page": String(page)
        ]
        
        let url = referenceURL + referencePerson
        
        Alamofire.request(url, method: .get, headers: header).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false, user, posts, likeds, follow)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                
                let userObject = json.value(forKey: kUser) as! Dictionary<String,AnyObject>
                do {
                    user = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
                } catch let error as NSError {
                    print(error)
                    
                }
                
                follow = json.value(forKey: kFollow) as! String
                
                let arrLiked = json.value(forKey: "liked") as! NSArray
                for like in arrLiked {
                    likeds.append(like as! Bool)
                }
                
                let arrPost = json.value(forKey: kPosts) as! NSArray
                for post in arrPost {
                    let postObject = post as! Dictionary<String, AnyObject>
                    do {
                        let postInfo = try MTLJSONAdapter.model(of: Post.self, fromJSONDictionary: postObject) as! Post
                        postInfo.user = user
                        var images: [Image] = [Image]()
                        let arrImage = postObject["images"] as! NSArray
                        if arrImage.count > 0 {
                            for image in arrImage {
                                let imageObject = image as! Dictionary<String, AnyObject>
                                do {
                                    let imageInfo = try MTLJSONAdapter.model(of: Image.self, fromJSONDictionary: imageObject) as! Image
                                    images.append(imageInfo)
                                } catch let error as NSError {
                                    print(error)
                                }
                            }
                            postInfo.images = images
                        }
                        posts.append(postInfo)
                    } catch let error as NSError {
                        print(error)
                    }
                }
                
                completionHandle(true, user, posts, likeds, follow)
            } else {
                completionHandle(false, user, posts, likeds, follow)
            }
        }
    }
    
    // ------------------------------------------------------------------
    // MARK: - API get post from server for client
    // get post
    func getPost(id: String, page: Int, completionHandle:@escaping (_ success: Bool, _ posts: [Post], _ likeds: [Bool]) -> (),failure:(_ error:NSError) -> ()) {
        
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        let header = [
            "id": id,
            "page": String(page)
        ]
        
        let url = referenceURL + referencePost
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, posts, likeds)
                print("error")
                return
            }
                    
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let result = self.getPostsFromJson(json: json)
                posts = result.post
                likeds = result.likeds
                completionHandle(true, posts, likeds)
            } else {
                completionHandle(false, posts, likeds)
            }
            
        }
    }
    
    // Call API get post startup to server
    func getPostStartup(id: String, page: Int, completionHandle:@escaping (_ success: Bool, _ posts: [Post], _ likeds: [Bool]) -> (),failure:(_ error:NSError) -> ()) {
        
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        let header = [
            "id": id,
            "page": String(page)
        ]
        
        let url = referenceURL + referenceStartup
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, posts, likeds)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let result = self.getPostsFromJson(json: json)
                posts = result.post
                likeds = result.likeds
                completionHandle(true, posts, likeds)
            } else {
                completionHandle(false, posts, likeds)
            }
            
        }
    }
    
    // Call API get post event to server
    func getPostEvent(id: String, page: Int, completionHandle:@escaping (_ success: Bool, _ posts: [Post], _ likeds: [Bool]) -> (),failure:(_ error:NSError) -> ()) {
        
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        let header = [
            "id": id,
            "page": String(page)
        ]
        
        let url = referenceURL + referenceEvent
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, posts, likeds)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let result = self.getPostsFromJson(json: json)
                posts = result.post
                likeds = result.likeds
                completionHandle(true, posts, likeds)
            } else {
                completionHandle(false, posts, likeds)
            }
        }
    }
    
    // Call API get post Experience to server
    func getPostExperience(id: String, page: Int, completionHandle:@escaping (_ success: Bool, _ posts: [Post], _ likeds: [Bool]) -> (),failure:(_ error:NSError) -> ()) {
        
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        let header = [
            "id": id,
            "page": String(page)
        ]
        
        let url = referenceURL + referenceExperience
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, posts, likeds)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let result = self.getPostsFromJson(json: json)
                posts = result.post
                likeds = result.likeds
                completionHandle(true, posts, likeds)
            } else {
                completionHandle(false, posts, likeds)
            }
        }
    }
    
    // Call API get post service to server
    func getPostService(id: String, page: Int, completionHandle:@escaping (_ success: Bool, _ posts: [Post], _ likeds: [Bool]) -> (),failure:(_ error:NSError) -> ()) {
        
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        let header = [
            "id": id,
            "page": String(page)
        ]
        
        let url = referenceURL + referenceService
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, posts, likeds)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let result = self.getPostsFromJson(json: json)
                posts = result.post
                likeds = result.likeds
                completionHandle(true, posts, likeds)
            } else {
                completionHandle(false, posts, likeds)
            }
        }
    }
    
    // Call API get post service to server
    func getPostReview(id: String, page: Int, completionHandle:@escaping (_ success: Bool, _ posts: [Startup]) -> (),failure:(_ error:NSError) -> ()) {
        
        var posts: [Startup] = [Startup]()
        let header = [
            "id": id,
            "page": String(page)
        ]
        
        let url = referenceURL + referencePostReview
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, posts)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                posts = self.getPostsReviewFromJson(json: json)                
                completionHandle(true, posts)
            } else {
                completionHandle(false, posts)
            }
        }
    }

    
    //
    func getPostFromUser(id: String, page: Int, user: User, completionHandle:@escaping (_ success: Bool, _ posts: [Post], _ likeds: [Bool]) -> (),failure:(_ error:NSError) -> ()) {
        
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        let header = [
            "id": id,
            "page": String(page)
        ]
        
        let url = referenceURL + referencePostUser
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, posts, likeds)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {

                // get array bool
                let arrLiked = json.value(forKey: "liked") as! NSArray
                for like in arrLiked {
                    likeds.append(like as! Bool)
                }
                
                // get array post
                let arrPost = json.value(forKey: kPosts) as! NSArray
                for post in arrPost {
                    let postObject = post as! Dictionary<String, AnyObject>
                    do {
                        let postInfo = try MTLJSONAdapter.model(of: Post.self, fromJSONDictionary: postObject) as! Post
                        postInfo.user = user
                        var images: [Image] = [Image]()
                        let arrImage = postObject["images"] as! NSArray
                        if arrImage.count > 0 {
                            for image in arrImage {
                                let imageObject = image as! Dictionary<String, AnyObject>
                                do {
                                    let imageInfo = try MTLJSONAdapter.model(of: Image.self, fromJSONDictionary: imageObject) as! Image
                                    images.append(imageInfo)
                                } catch let error as NSError {
                                    print(error)
                                }
                            }
                            postInfo.images = images
                        }
                        posts.append(postInfo)
                    } catch let error as NSError {
                        print(error)
                    }
                }

                completionHandle(true, posts, likeds)
            } else {
                completionHandle(false, posts, likeds)
            }
            
        }
    }

    
    // ------------------------------------------------------------------
    // MARK: - API post from server for client
    // post startup
    func uploadPostStartup(_ id: String, _ post: Startup, _ arrImg: [UIImage], completionHandle:@escaping (_ success: Bool, _ result: Post) -> (),failure:(_ error:NSError) -> ()) {
        
        var postInfo: Post = Post()
        var arrData = [Data]()
        for img in arrImg {
            let data = UIImageJPEGRepresentation(img, 1)
            arrData.append(data!)
        }
        
        let param = [
            "type": post.type!,
            "id_user": id,
            "idea": post.idea!,
            "team": post.team!,
            "fields": post.fields!,
            "userObject": post.userObject!,
            "orientedDevelopment": post.orientedDevelopment!,
            "describeProduct": post.describeProduct!
        ]
        
        let url = referenceURL + referenceStartup
        let urlRequest = try! URLRequest(url: url, method: .post)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if arrImg.count > 0 {
                for data in arrData {
                    multipartFormData.append(data,
                                             withName: "image",
                                             fileName: "fileName.png",
                                             mimeType: "image/jpg")
                }
            }
            for (key, value) in param {
                multipartFormData.append((value.data(using: String.Encoding.utf8)!), withName: key)
            }
        }, with: urlRequest, encodingCompletion: { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let json = response.result.value as AnyObject
                    if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                        postInfo = self.getPostFromJson(json: json)
                        completionHandle(true, postInfo)
                    } else {
                        completionHandle(false, postInfo)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                completionHandle(false, postInfo)
            }
        })
        
    }
    
    // post startup
    func uploadPostEvent(_ id: String, _ post: Event, _ arrImg: [UIImage], completionHandle:@escaping (_ success: Bool, _ result: Post) -> (),failure:(_ error:NSError) -> ()) {
        
        var postInfo: Post = Post()
        var arrData = [Data]()
        for img in arrImg {
            let data = UIImageJPEGRepresentation(img, 1)
            arrData.append(data!)
        }
        
        let param = [
            "type": post.type!,
            "id_user": id,
            "title": post.title!,
            "describeEvent": post.describeEvent!,
            "organizer": post.organizer!,
            "phone": post.phone!,
            "dayStart": post.dayStart!,
            "dayEnd": post.dayEnd!,
            "timeStart": post.timeStart!,
            "timeEnd": post.timeEnd!,
            "locationEvent": post.locationEvent!
        ]
        
        let url = referenceURL + referenceEvent
        let urlRequest = try! URLRequest(url: url, method: .post)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if arrImg.count > 0 {
                for data in arrData {
                    multipartFormData.append(data,
                                             withName: "image",
                                             fileName: "fileName.png",
                                             mimeType: "image/jpg")
                }
            }
            for (key, value) in param {
                multipartFormData.append((value.data(using: String.Encoding.utf8)!), withName: key)
            }
        }, with: urlRequest, encodingCompletion: { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let json = response.result.value as AnyObject
                    if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                        postInfo = self.getPostFromJson(json: json)
                        completionHandle(true, postInfo)
                    } else {
                        completionHandle(false, postInfo)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                completionHandle(false, postInfo)
            }
        })
    }

    // post experience
    func uploadPostExperience(_ id: String, _ post: Experience, _ arrImg: [UIImage], completionHandle:@escaping (_ success: Bool, _ result: Post) -> (),failure:(_ error:NSError) -> ()) {
        
        var postInfo: Post = Post()
        var arrData = [Data]()
        for img in arrImg {
            let data = UIImageJPEGRepresentation(img, 1)
            arrData.append(data!)
        }
        
        let param = [
            "type": post.type!,
            "id_user": id,
            "isQuestion": "\(post.isQuestion)",
            "content": post.content!
        ]
        
        let url = referenceURL + referenceExperience
        let urlRequest = try! URLRequest(url: url, method: .post)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if arrImg.count > 0 {
                for data in arrData {
                    multipartFormData.append(data,
                                             withName: "image",
                                             fileName: "fileName.png",
                                             mimeType: "image/jpg")
                }
            }
            for (key, value) in param {
                multipartFormData.append((value.data(using: String.Encoding.utf8)!), withName: key)
            }
        }, with: urlRequest, encodingCompletion: { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let json = response.result.value as AnyObject
                    if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                        postInfo = self.getPostFromJson(json: json)
                        completionHandle(true, postInfo)
                    } else {
                        completionHandle(false, postInfo)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                completionHandle(false, postInfo)
            }
        })
    }

    // ------------------------------------------------------------------
    // MARK: - API like and unlike
    // like post 
    func like(_ id_post: String, _ id_user: String, completionHandle: @escaping(_ success: Bool) -> (),failure: (_ error: NSError) -> ()) {
        
        let url = referenceURL + referenceLike
        
        let params = [
            "id_post": id_post,
            "id_user": id_user
        ]
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false)
                print("error")
                return
            }
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                completionHandle(true)
            } else {
                completionHandle(false)
            }
        }
    }
    
    func unlike(_ id_post: String, _ id_user: String, completionHandle: @escaping(_ success: Bool) -> (),failure: (_ error: NSError) -> ()) {
        
        let url = referenceURL + referenceLike
        
        let params = [
            "id_post": id_post,
            "id_user": id_user
        ]
        
        Alamofire.request(url, method: .delete, headers: params).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                completionHandle(true)
            } else {
                completionHandle(false)
            }
        }
    }
    
    // ------------------------------------------------------------------
    // MARK: - API like and unlike
    // get comment
    func getComment(_ id_post: String, _ page: Int, completionHandle: @escaping(_ sucess: Bool, _ comments: [Comment]) -> (), failure: (_ error: NSError) ->()) {
        
        var comments: [Comment] = [Comment]()
        var users: [User] = [User]()
        
        let url = referenceURL + referenceComment
        let params = [
            "id_post": id_post,
            "page": String(page)
        ]
        
        Alamofire.request(url, method: .get, headers: params).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, comments)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                
                let arrUser = json.value(forKey: kUsers) as! NSArray
                for user in arrUser {
                    let userObject = user as! Dictionary<String, AnyObject>
                    do {
                        let userInfo = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
                        users.append(userInfo)
                    } catch let error as NSError {
                        print(error)
                    }
                }
                
                let arrComment = json.value(forKey: kComments) as! NSArray
                for comment in arrComment {
                    let commentObject = comment as! Dictionary<String, AnyObject>
                    do {
                        let commentInfo = try MTLJSONAdapter.model(of: Comment.self, fromJSONDictionary: commentObject) as! Comment
                        for user in users {
                            let id = commentObject["id_user"] as! String
                            if user.id == id {
                                commentInfo.user = user
                                break
                            }
                        }
                        comments.append(commentInfo)
                    } catch let error as NSError {
                        print(error)
                    }
                }
                completionHandle(true, comments)
            } else {
                completionHandle(false, comments)
            }
        }
    }
    
    // post comment    
    func postCommet(_ comment: Comment, completionHandle:@escaping (_ success: Bool, _ result: Comment) -> (),failure:(_ error:NSError) -> ()) {
        
        var result: Comment = Comment()
        var user: User = User()
        let url: String = referenceURL + referenceComment
        let params: Dictionary<String, Any> = [
            "id_post": comment.post!,
            "id_user": comment.user!.id!,
            "content": comment.content!
        ]
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, result)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                
                let userObject = json.value(forKey: kUser) as! Dictionary<String,AnyObject>
                do {
                    user = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
                } catch let error as NSError {
                    print(error)
                }
                
                let commentObject = json.value(forKey: kComment) as! Dictionary<String, AnyObject>
                do {
                    result = try MTLJSONAdapter.model(of: Comment.self, fromJSONDictionary: commentObject) as! Comment
                    result.user = user
                } catch let error as NSError {
                    print(error)
                }
                completionHandle(true, result)
            } else {
                completionHandle(false, result)
            }
        }
    }
    
    // ------------------------------------------------------------------
    // MARK: - API like and unlike
    // get following of user
    func getFollowing(_ id: String, _ page: Int, completionHandle:@escaping (_ success: Bool, _ users: [User]) -> (),failure:(_ error:NSError) -> ()) {
        
        var users: [User] = [User]()
        let url: String = referenceURL + referenceFollowing
        let header = [
            "followers": id,
            "page": String(page)
        ]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false, users)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                
                let arrUser = json.value(forKey: kUsers) as! NSArray
                for user in arrUser {
                    let userObject = user as! Dictionary<String, AnyObject>
                    do {
                        let userTemp = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
                        users.append(userTemp)
                    } catch let error as NSError {
                        print(error)
                    }
                }
                completionHandle(true, users)
            } else {
                completionHandle(false, users)
            }

        }
    }
    
    // follow user
    func follow(_ user: User, _ person: User, completionHandle:@escaping (_ success: Bool, _ id: String) -> (),failure:(_ error:NSError) -> ()) {
        
        var id: String = ""
        let url: String = referenceURL + referenceFollow
        let params: Dictionary<String,String> = [
            "followers": person.id!,
            "following": user.id!
            
        ]
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false, id)
                print("error")
                return
            }
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                id = json.value(forKey: "id") as! String
                completionHandle(true, id)
            } else {
                completionHandle(false, id)
            }


        }
    }
    
    // follow user
    func unfollow(_ id: String, _ user: User, _ person: User, completionHandle:@escaping (_ success: Bool) -> (),failure:(_ error:NSError) -> ()) {
        
        let url: String = referenceURL + referenceFollow
        let params: Dictionary<String,String> = [
            "id": id,
            "followers": person.id!,
            "following": user.id!
        ]
        
        Alamofire.request(url, method: .delete, parameters: params, encoding: JSONEncoding.default).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                completionHandle(true)
            } else {
                completionHandle(false)
            }
        }
    }
    
    // ------------------------------------------------------------------
    // MARK: - API like and unlike
    // create group
    func createGroup(_ arrId: [String], completionHandle:@escaping (_ success: Bool, _ gruop: Group) -> (),failure:(_ error:NSError) -> ()) {
        
        let url: String = referenceURL + referenceGroup
        var group: Group = Group()
        let params: Dictionary<String, Any> = [
            "users": arrId
        ]
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false, group)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                
                let groupObject = json.value(forKey: kGroup) as! Dictionary<String, AnyObject>
                do {
                    group = try MTLJSONAdapter.model(of: Group.self, fromJSONDictionary: groupObject) as! Group
                } catch let error as NSError {
                    print(error)
                }
                completionHandle(true, group)
            } else {
                completionHandle(false, group)
            }
        }
    }

    // get group
    func getGroup(_ id: String, _ page: Int, completionHandle:@escaping (_ success: Bool, _ gruops: [Group]) -> (),failure:(_ error:NSError) -> ()) {
        
        let url: String = referenceURL + referenceGroup
        var groups: [Group] = [Group]()
        var users: [User] = [User]()
        var listIDUser: [String] = [String]()
        let params = [
            "id": id,
            "page": String(page)
        ]
        
        Alamofire.request(url, method: .get, headers: params).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false, groups)
                print("error")
                return
            }
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                
                let arrUser = json.value(forKey: kUsers) as! NSArray
                for user in arrUser {
                    let userObject = user as! Dictionary<String, AnyObject>
                    do {
                        let userTemp = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
                        users.append(userTemp)
                        listIDUser.append(userTemp.id!)
                    } catch let error as NSError {
                        print(error)
                    }
                }
                
                let arrGroup = json.value(forKey: kGroups) as! NSArray
                for group in arrGroup {
                    let groupObject = group as! Dictionary<String, AnyObject>
                    do {
                        let groupTemp = try MTLJSONAdapter.model(of: Group.self, fromJSONDictionary: groupObject) as! Group
                        
                        let messageObject = (group as AnyObject).value(forKey: kMessage) as! Dictionary<String,AnyObject>
                        do {
                            groupTemp.message = try MTLJSONAdapter.model(of: Message.self, fromJSONDictionary: messageObject) as? Message
                        } catch let error as NSError {
                            print(error)
                        }
                    
                        for idUser in groupTemp.id_users! {
                            let find = listIDUser.contains(idUser)
                            if find {
                                let index = listIDUser.index(of: idUser)
                                groupTemp.user =  users[index!]
                            }
                        }
                        
                        groups.append(groupTemp)
                        
                    } catch let error as NSError {
                        print(error)
                    }
                }
            
                completionHandle(true, groups)
            } else {
                completionHandle(false, groups)
            }
        }
    }
    
    // ------------------------------------------------------------------
    // MARK: - API like and unlike
    // get message
    func getMessage(_ id: String, _ page: Int, completionHandle:@escaping (_ success: Bool, _ messages: [Message]) -> (),failure:(_ error:NSError) -> ()) {
        
        let url: String = referenceURL + referenceMessage
        var messages: [Message] = [Message]()
        var users: [User] = [User]()
        let params = [
            "id": id,
            "page": String(page)
        ]
        
        Alamofire.request(url, method: .get, headers: params).responseJSON { (response) in
            if (response.result.value == nil) {
                completionHandle(false, messages)
                print("error")
                return
            }
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                
                let arrUser = json.value(forKey: kUsers) as! NSArray
                for user in arrUser {
                    let userObject = user as! Dictionary<String, AnyObject>
                    do {
                        let userTemp = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
                        users.append(userTemp)
                    } catch let error as NSError {
                        print(error)
                    }
                }
                
                let arrMessage = json.value(forKey: kMessages) as! NSArray
                for message in arrMessage {
                    let messageObject = message as! Dictionary<String,AnyObject>
                    do {
                        let messageInfo = try MTLJSONAdapter.model(of: Message.self, fromJSONDictionary: messageObject) as? Message
                        for user in users {
                            let id = messageInfo?.id_send
                            if user.id == id {
                                messageInfo?.user = user
                                break
                            }
                        }
                        messages.append(messageInfo!)
                    } catch let error as NSError {
                        print(error)
                    }
                }
                
                completionHandle(true, messages)
            } else {
                completionHandle(false, messages)
            }
        }
    }
    
    // postReview
    func postReview(_ review: Review, completionHandle:@escaping (_ success: Bool) -> (),failure:(_ error:NSError) -> ()) {

        let url: String = referenceURL + referenceReview
        let params: Dictionary<String, Any> = [
            "id_post": review.post!,
            "id_user": review.user!.id!,
            "content": review.content!,
            "vote": review.vote
        ]
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                completionHandle(true)
            } else {
                completionHandle(false)
            }
        }
    }
    
    // Call API get top post startup to server
    func getTopPostStartup(id: String, completionHandle:@escaping (_ success: Bool, _ posts: [Post], _ likeds: [Bool]) -> (),failure:(_ error:NSError) -> ()) {
        
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        let header = [
            "id": id
        ]
        
        let url = referenceURL + referenceReview
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, posts, likeds)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let result = self.getPostsFromJson(json: json)
                posts = result.post
                likeds = result.likeds
                completionHandle(true, posts, likeds)
            } else {
                completionHandle(false, posts, likeds)
            }
            
        }
    }
    
    func searchPostStartup(id: String, text: String, page: Int, completionHandle:@escaping (_ success: Bool, _ posts: [Post], _ likeds: [Bool]) -> (),failure:(_ error:NSError) -> ()) {
        
        var posts: [Post] = [Post]()
        var likeds: [Bool] = [Bool]()
        let params = [
            "id": id,
            "text": text,
            "page": String(page)
        ]
        
        let url = referenceURL + referenceSearchStartup
        
        Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
            
            if (response.result.value == nil) {
                completionHandle(false, posts, likeds)
                print("error")
                return
            }
            
            let json = response.result.value as AnyObject
            if ((json.value(forKey: kSuccess) as! NSNumber) == 1) {
                let result = self.getPostsFromJson(json: json)
                posts = result.post
                likeds = result.likeds
                completionHandle(true, posts, likeds)
            } else {
                completionHandle(false, posts, likeds)
            }
            
        }
    }


}











