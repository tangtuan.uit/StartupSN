//
//  AdvisoryViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/6/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class AdvisoryViewController: BaseViewController {

    var collectionViewConsultancy: UICollectionView!
    
    var arrPost = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupCollectionView()
        
        let nibPost = UINib(nibName: "ExperiencePostCell", bundle: nil)
        self.collectionViewConsultancy.register(nibPost, forCellWithReuseIdentifier: "ExperienceCell")
        
        self.loadData()
        
        self.collectionViewConsultancy.showsVerticalScrollIndicator = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.typeViewPost = .experience
    }
    
    func setupCollectionView() {
        
        self.collectionViewConsultancy = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 44 - 49 - 36 - 20), collectionViewLayout: UICollectionViewFlowLayout())
        self.collectionViewConsultancy.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        self.view.addSubview(self.collectionViewConsultancy)
        
        self.collectionViewConsultancy.delegate = self
        self.collectionViewConsultancy.dataSource = self
        
        self.setupRefreshCollectionView(view: self.view, scrollView: self.collectionViewConsultancy, marginTop: 0.0)
    }
    
    func loadData() {
        let id = self.appDelegate.user?.id
        self.baseRefresh.startRefreshing()
        APIClient.sharedInstance.getPostExperience(id: id!, page: 1, completionHandle: { (success, arrPost, likeds) in
            
            self.arrPost = arrPost
            self.collectionViewConsultancy.reloadData()
            self.baseRefresh.endRefreshing()
        }) { (error) in
            print(error)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AdvisoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let post = self.arrPost[indexPath.item] as! Experience
        let height = Helper.sharedInstance.getHeightTextForPost(width: widthDynamicTextPost, text: "Kinh nghiệm\n" + post.content!, sizeFont: 14)
        if post.images == nil {
            if height <= 238 {
                return CGSize(width: widthCellCollectionViewPost, height: height + 106 - 8)
            } else {
                return CGSize(width: widthCellCollectionViewPost, height: 238 + 106 + 17 - 10)
            }
        } else {
            if height <= 136 {
                return CGSize(width: widthCellCollectionViewPost, height: height + 410 - 4 + 4)
            } else {
                return CGSize(width: widthCellCollectionViewPost, height: 136 + 410 + 17)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, sizeEdgeInsetsMakePost, 8, sizeEdgeInsetsMakePost)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionViewConsultancy.dequeueReusableCell(withReuseIdentifier: "ExperienceCell", for: indexPath) as! ExperiencePostCell
        cell.currentIndexPath = indexPath as NSIndexPath!
        cell.delegate = self
        cell.setupViewExperienceWithPost(post: self.arrPost[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.item)
    }
}

extension AdvisoryViewController: ExperiencePostCellDelegate {
    
    func openCommentAtIndexFromExperience(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewComment(post: self.arrPost[indexPath.item], viewController: self)
    }
}

