//
//  TopStartupViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 7/6/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class TopStartupViewController: BaseViewController {
    
    @IBOutlet weak var collectionViewTopStartup: UICollectionView!
    var arrPost = [Post]()
    var arrLiked = [Bool]()
    
    let dismissButton = UIButton(type: .system)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Top dự án khởi nghiệp"
        self.setupLeftBarButton()
        
        let nidHeader = UINib(nibName: "TopStartupHeaderView", bundle: nil)
        self.collectionViewTopStartup.register(nidHeader, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderTopStatupViewId")
        
        let nibPost = UINib(nibName: "StartupPostViewCell", bundle: nil)
        self.collectionViewTopStartup.register(nibPost, forCellWithReuseIdentifier: "cell")
        self.collectionViewTopStartup.backgroundColor = UIColor(white: 0.9, alpha: 1)
        self.collectionViewTopStartup.bounces = false
        
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadData() {
        let id = self.appDelegate.user?.id
        APIClient.sharedInstance.getTopPostStartup(id: id!, completionHandle: { (success, arrPost, likeds) in
            self.arrPost = arrPost
            self.arrLiked = likeds
            self.collectionViewTopStartup.reloadData()
        }) { (error) in
            print(error)
        }
    }
    
    func setupLeftBarButton() {
        self.dismissButton.setImage(#imageLiteral(resourceName: "ic_back"), for: .normal)
        self.dismissButton.frame = CGRect(x: 0, y: 0, width: 54, height: 34)
        self.dismissButton.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -32, bottom: 0, right: 0)
        self.dismissButton.addTarget(self, action: #selector(self.handleDismiss), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.dismissButton)
    }

    func handleDismiss() {
        self.navigationController?.popViewController(animated: true)
    }

}

extension TopStartupViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = self.collectionViewTopStartup.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderTopStatupViewId", for: indexPath) as! TopStartupHeaderView
        return header
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: self.view.frame.width, height: self.view.frame.width)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, sizeEdgeInsetsMakePost, 8, sizeEdgeInsetsMakePost)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let post = self.arrPost[indexPath.item] as! Startup
        let height = Helper.sharedInstance.getHeightTextForPost(width: widthDynamicTextPost, text: "Describe Startup\n" + post.idea!, sizeFont: 14)
        if (height + 423) > 554 {
            return CGSize(width: widthCellCollectionViewPost, height: 554)
        } else {
            return CGSize(width: widthCellCollectionViewPost, height: height + 423)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionViewTopStartup.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StartupPostViewCell
        cell.delegate = self
        cell.currentIndexPath = indexPath as NSIndexPath!
        cell.setupView(isDetail: false, post: self.arrPost[indexPath.item], liked: self.arrLiked[indexPath.item])
        cell.setupSliderShow(arrImage: self.arrPost[indexPath.item].images!)
        cell.viewFullScreen = self
        return cell
    }
    
}

extension TopStartupViewController: StartupPostViewCellDelegate {
    func openInputReviewFromStartup(indexPath: NSIndexPath) {
        
    }
    
    func openViewProfileFromStartup(indexPath: NSIndexPath) {
        let user = self.arrPost[indexPath.item].user
        if user?.id != self.appDelegate.user?.id {
            let vcPerson = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "PersonViewControllerIdentifier", storyboardName: "Dashboard") as! PersonViewController
            vcPerson.user = user!
            self.navigationController?.pushViewController(vcPerson, animated: true)
        } else {
            StoryboardManager.sharedInstance.pushViewProfile(viewController: self)
        }
    }
    
    func openDetailAtIndex(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewDetail(post: self.arrPost[indexPath.item], liked: self.arrLiked[indexPath.item], viewController: self)
    }
    
    func openCommentAtIndexFromStartup(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewComment(post: self.arrPost[indexPath.item], viewController: self)
    }
}
