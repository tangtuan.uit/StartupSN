//
//  StartupViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/6/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class StartupViewController: BaseViewController {
    
    var arrPost = [Post]()
    var arrLiked = [Bool]()
    var collectionViewStartup: UICollectionView!
    
    var page: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupCollectionView()

        let nibPost = UINib(nibName: "StartupPostViewCell", bundle: nil)
        self.collectionViewStartup.register(nibPost, forCellWithReuseIdentifier: "cell")
        
        self.loadData(page: self.page)
        
        self.collectionViewStartup.showsVerticalScrollIndicator = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData), name: NSNotification.Name(rawValue: tReloadStartup), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.typeViewPost = .startup
    }
    
    func setupCollectionView() {
        
        self.collectionViewStartup = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 44 - 49 - 36 - 20), collectionViewLayout: UICollectionViewFlowLayout())
        self.collectionViewStartup.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        self.view.addSubview(self.collectionViewStartup)
        
        self.collectionViewStartup.delegate = self
        self.collectionViewStartup.dataSource = self
        
        self.setupRefreshCollectionView(view: self.view, scrollView: self.collectionViewStartup, marginTop: 0.0)
    }
    
    func reloadData() {
        self.page = 1
        self.loadData(page: self.page)
    }
    
    func loadData(page: Int) {
        let id = self.appDelegate.user?.id
        self.baseRefresh.startRefreshing()
        APIClient.sharedInstance.getPostStartup(id: id!, page: page, completionHandle: { (success, arrPost, likeds) in
            self.arrPost = arrPost
            self.arrLiked = likeds
            self.collectionViewStartup.reloadData()
            self.baseRefresh.endRefreshing()
        }) { (error) in
            print(error)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StartupViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let post = self.arrPost[indexPath.item] as! Startup
        let height = Helper.sharedInstance.getHeightTextForPost(width: widthDynamicTextPost, text: "Describe Startup\n" + post.idea!, sizeFont: 14)
        if (height + 423) > 554 {
            return CGSize(width: widthCellCollectionViewPost, height: 554)
        } else {
            return CGSize(width: widthCellCollectionViewPost, height: height + 423)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, sizeEdgeInsetsMakePost, 8, sizeEdgeInsetsMakePost)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionViewStartup.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StartupPostViewCell
        cell.delegate = self
        cell.currentIndexPath = indexPath as NSIndexPath!
        cell.setupView(isDetail: false, post: self.arrPost[indexPath.item], liked: self.arrLiked[indexPath.item])
        cell.setupSliderShow(arrImage: self.arrPost[indexPath.item].images!)
        cell.viewFullScreen = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.item)
    }
    
}

extension StartupViewController: StartupPostViewCellDelegate {
    
    func openInputReviewFromStartup(indexPath: NSIndexPath) {
        let inputView = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "InputReviewViewControllerIdentifier", storyboardName: "Dashboard") as! InputReviewViewController
        inputView.hidesBottomBarWhenPushed = true
        inputView.idPost = self.arrPost[indexPath.item].id
        self.navigationController?.pushViewController(inputView, animated: true)
    }

    func openViewProfileFromStartup(indexPath: NSIndexPath) {
        let user = self.arrPost[indexPath.item].user
        if user?.id != self.appDelegate.user?.id {
            let vcPerson = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "PersonViewControllerIdentifier", storyboardName: "Dashboard") as! PersonViewController
            vcPerson.user = user!
            self.navigationController?.pushViewController(vcPerson, animated: true)
        } else {
            StoryboardManager.sharedInstance.pushViewProfile(viewController: self)
        }
    }
    
    func openDetailAtIndex(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewDetail(post: self.arrPost[indexPath.item], liked: self.arrLiked[indexPath.item], viewController: self)
    }
    
    func openCommentAtIndexFromStartup(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewComment(post: self.arrPost[indexPath.item], viewController: self)
    }
}
