//
//  InputViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/30/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class InputViewController: BaseViewController {

    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    @IBOutlet weak var textViewInput: RSKGrowingTextView!
    
    let cancelButton = UIButton(type: .system)
    let doneButton = UIButton(type: .system)
    
    var typeText: String!
    var typePost: TypePost!
    
    let fk = FollowKeyboard()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLeftBarButton()
        self.setupRightBarButton()
        
        self.textViewInput.placeholder = "Mô tả ý tưởng của bạn một cách rõ ràng giúp tăng 87% khả năng thu hút nhà đầu tư..."
        self.textViewInput.becomeFirstResponder()
        
        self.setTypeTextView(typePost: self.typePost, typeText: self.typeText)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fk.followKeyboard(withAnimations: { (keyboardFrame, duration, type) in
            switch type {
            case .Show:
                self.bottomConstraint.constant = keyboardFrame.height
                self.view.layoutIfNeeded()
            case .Hide:
                self.bottomConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
            
        }) { (finished) in
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.fk.unfollowKeyboard()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupLeftBarButton() {
        
        let titleButton = NSAttributedString(string: "Cancel", attributes: [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 16)])
        self.cancelButton.setAttributedTitle(titleButton, for: .normal)
        self.cancelButton.frame = CGRect(x: 0, y: 0, width: 54, height: 34)
        self.cancelButton.addTarget(self, action: #selector(InputViewController.handleButtonCancel), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.cancelButton)
    }
    
    func setupRightBarButton() {
        
        let titleButton = NSAttributedString(string: "Done", attributes: [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 16)])
        self.doneButton.setAttributedTitle(titleButton, for: .normal)
        self.doneButton.frame = CGRect(x: 0, y: 0, width: 54, height: 34)
        self.doneButton.addTarget(self, action: #selector(InputViewController.handleButtonDone), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.doneButton)
    }
    
    func handleButtonCancel() {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func handleButtonDone() {
        self.doneInputTextForPost(typePost: self.typePost, typeText: self.typeText)
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func setTypeTextView(typePost: TypePost?, typeText: String?) {
        if typePost == .startup {
            if typeText == kSUIdea {
                self.textViewInput.text = postStartup?.idea
            } else if typeText == kSUTeam {
                self.textViewInput.text = postStartup?.team
            } else if typeText == kSUFields {
                self.textViewInput.text = postStartup?.fields
            } else if typeText == kSUUser {
                self.textViewInput.text = postStartup?.userObject
            } else if typeText == kSUDevelopment {
                self.textViewInput.text = postStartup?.orientedDevelopment
            } else if typeText == kSUProduct {
                self.textViewInput.text = postStartup?.describeProduct
            }
        } else if typePost == .event {
            self.textViewInput.text = postEvent?.describeEvent
        } else if typePost == .experience {
            self.textViewInput.text = postExperience?.content
        } else if typePost == nil {
            self.textViewInput.text = self.appDelegate.user?.about
        } else if typePost == .review {
            self.textViewInput.text = reviewStartup
        }
    }
    
    func doneInputTextForPost(typePost: TypePost?, typeText: String?) {
        if typePost == .startup {
            if typeText == kSUIdea {
                postStartup?.idea = self.textViewInput.text
            } else if typeText == kSUTeam {
                postStartup?.team = self.textViewInput.text
            } else if typeText == kSUFields {
                postStartup?.fields = self.textViewInput.text
            } else if typeText == kSUUser {
                postStartup?.userObject = self.textViewInput.text
            } else if typeText == kSUDevelopment {
                postStartup?.orientedDevelopment = self.textViewInput.text
            } else {
                postStartup?.describeProduct = self.textViewInput.text
            }
        } else if typePost == .event {
            postEvent?.describeEvent = self.textViewInput.text
        } else if typePost == .experience {
            postExperience?.content = self.textViewInput.text
        } else if typePost == nil {
            self.appDelegate.user?.about = self.textViewInput.text
        } else if typePost == .review {
            reviewStartup = self.textViewInput.text
        }
    }


}
