//
//  PostViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/6/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import TZImagePickerController
import Fusuma

class PostViewController: BaseViewController {
    
    @IBOutlet weak var collectionViewInput: UICollectionView!
    
    let buttonCategoryPost = UIButton(type: .system)

    let arrCategory = [kMCStartup, kMCEvent, kMCShare, kMCService, kMCStatus]
    
    var typePost: TypePost = .startup
    
    var typeSubPost: String!
    
    var arrImage: [UIImage] = [UIImage]()
    
    var idUser: String!
    
//    weak var delegate: PostViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNavigation()
        
        self.idUser = self.appDelegate.user?.id
        
        let nibStartupInput = UINib(nibName: "StartupViewInput", bundle: nil)
        self.collectionViewInput.register(nibStartupInput, forCellWithReuseIdentifier: "StartupCell")
        
        let nibEventInput = UINib(nibName: "EventViewInput", bundle: nil)
        self.collectionViewInput.register(nibEventInput, forCellWithReuseIdentifier: "EventCell")
        
        let nibExperienceInput = UINib(nibName: "ExperienceViewInput", bundle: nil)
        self.collectionViewInput.register(nibExperienceInput, forCellWithReuseIdentifier: "ExperienceCell")
        
//        self.collectionViewInput.backgroundColor = .lightGray
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configDownMenu() {
        let config = FTConfiguration.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor.lightGray
        config.menuSeparatorColor = UIColor.lightGray
        config.textAlignment = .center
        config.textFont = UIFont.systemFont(ofSize: 14)
        config.menuRowHeight = 40
        config.menuWidth = 96
        config.cornerRadius = 6
    }
    
    func setupNavigation() {
        
        // set left button item
        let buttonDismiss = UIButton(type: .system)
        buttonDismiss.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        buttonDismiss.setImage(#imageLiteral(resourceName: "ic_dismiss").withRenderingMode(.alwaysOriginal), for: .normal)
        buttonDismiss.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -16, bottom: 0, right: 0)
        buttonDismiss.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0) // t l b r
        buttonDismiss.addTarget(self, action: #selector(PostViewController.handleButtonDismiss), for: .touchUpInside)
        //buttonDismiss.backgroundColor = UIColor.darkGray
        
        self.buttonCategoryPost.frame = CGRect(x: 0, y: 0, width: Helper.sharedInstance.getWidthButtonCategoryPost(arrString: self.arrCategory), height: 36)
        self.buttonCategoryPost.setTitle(self.arrCategory[0], for: .normal)
        self.buttonCategoryPost.setImage(#imageLiteral(resourceName: "ic_down").withRenderingMode(.alwaysOriginal), for: .normal)
        self.buttonCategoryPost.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        self.buttonCategoryPost.contentHorizontalAlignment = .left
        //buttonCategoryPost.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -16, bottom: 0, right: 0)
        self.buttonCategoryPost.addTarget(self, action: #selector(PostViewController.handleButtomCategoryPost), for: .touchUpInside)
        //buttonCategoryPost.backgroundColor = UIColor.darkGray
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: buttonDismiss), UIBarButtonItem(customView: buttonCategoryPost)]
        
        // set left button item
        let buttonDone = UIButton(type: .system)
        buttonDone.frame = CGRect(x: 0, y: 0, width: 40, height: 36)
        buttonDone.setTitle("Done", for: .normal)
        buttonDone.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        buttonDone.addTarget(self, action: #selector(PostViewController.handleButtonDone), for: .touchUpInside)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: buttonDone)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
//        self.hidesBottomBarWhenPushed = true
        self.configDownMenu()
        self.createPost(type: self.typePost)
        self.collectionViewInput.reloadData()
    }
    
    func handleButtonDismiss() {
        
        let alert = FCAlertView()
        alert.delegate = self

        alert.titleColor = alert.flatRed
        alert.subTitleColor = alert.flatGray
        alert.titleFont = UIFont.boldSystemFont(ofSize: 18)
        alert.subtitleFont = UIFont.systemFont(ofSize: 16)
        alert.firstButtonBackgroundColor = UIColor(white: 0.95, alpha: 1)
        alert.secondButtonBackgroundColor = alert.flatRed
        alert.secondButtonTitleColor = .white
        alert.cornerRadius = 8
        alert.makeAlertTypeWarning()
        alert.dismissOnOutsideTouch = false
        alert.hideSeparatorLineView = true
        alert.hideDoneButton = true
        alert.showAlert(withTitle: "Xóa bài viết",
                        withSubtitle: "Bạn có muốn xóa nội dung bài viết đang thực hiện không?",
                        withCustomImage: nil,
                        withDoneButtonTitle: nil,
                        andButtons: ["Bỏ qua", "Xóa"])
    }
    
    func handleButtonDone() {
        
        if self.typePost == .startup {
            
            APIClient.sharedInstance.uploadPostStartup(self.idUser, postStartup!, self.arrImage, completionHandle: { (success, post) in
                
                self.successPost(success: success)
                
            }, failure: { (error) in
                print(error)
            })
            
        } else if self.typePost == .event {
            self.collectionViewInput.reloadData()
            APIClient.sharedInstance.uploadPostEvent(self.idUser, postEvent!, self.arrImage, completionHandle: { (success, post) in
                
                self.successPost(success: success)
                
            }, failure: { (error) in
                print(error)
            })
        } else if self.typePost == .experience {
            APIClient.sharedInstance.uploadPostExperience(self.idUser, postExperience!, self.arrImage, completionHandle: { (success, post) in
                
                self.successPost(success: success)
                
            }, failure: { (error) in
                print(error)
            })
        }
        
        self.dismissPostViewController()
        
    }
    
    func successPost(success: Bool) {
        
        if success {
            Helper.sharedInstance.showMessagesSuccess("Đăng bài viết thành công!", completionHandle: { (complete) in
                print("Hide")
            })
        } else {

        }
    }
    
    func handleButtomCategoryPost() {
        FTPopOverMenu.showForSender(sender: self.buttonCategoryPost, with: self.arrCategory, done: { (index) in
            self.buttonCategoryPost.setTitle(self.arrCategory[index], for: .normal)
            self.typePost = Helper.sharedInstance.setTypePostWithString(type: self.arrCategory[index])
            self.createPost(type: self.typePost)
            self.arrImage = [UIImage]()
            self.collectionViewInput.reloadData()
        }) { 
            print("cancel")
        }
    }
    
    func createPost(type: TypePost) {
        if type == .startup {
            if postStartup == nil {
                postStartup = Startup()
                postStartup?.type = kStartup
            }
        } else if type == .event {
            if postEvent == nil {
                postEvent = Event()
                postEvent?.type = kEvent
            }
        } else if type == .experience {
            if postExperience == nil {
                postExperience = Experience()
                postExperience?.type = kExperience
            }
        }
    }
    
    func deletePost(type: TypePost) {
        if type == .startup {
            postStartup = nil
        } else if type == .event {
            postEvent = nil
        } else if type == .experience {
            postExperience = nil
        }
        self.arrImage = [UIImage]()
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vcInput = segue.destination as! InputViewController
        vcInput.typePost = self.typePost
        vcInput.typeText = self.typeSubPost
    }
 
    func openImagePicker() {
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func openInputView(type: String) {
        self.typeSubPost = type
        self.performSegue(withIdentifier: "SegueInputViewController", sender: nil)
    }
}

extension PostViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.typePost == .startup {
            let size = ((self.view.frame.width - 16) - 12) / 4 //355 247
            let height = Helper.sharedInstance.getHeightStartupPost(post: postStartup!, width: self.view.frame.width - 16) + size + 247 + 8
            return CGSize(width: self.view.frame.width, height: height + 16)
        } else if self.typePost == .event {
            let height = Helper.sharedInstance.getHeightEventPost(post: postEvent!, width: self.view.frame.width - 16)
            if self.arrImage.count > 0 {
                return CGSize(width: self.view.frame.width, height: 324 - 36 + 196 + height + 24)
            } else {
                return CGSize(width: self.view.frame.width, height: 324 - 36 + height + 24)
            }
        } else {
            let height = Helper.sharedInstance.getHeightEventExperience(post: postExperience!, width: self.view.frame.width - 16)
            return CGSize(width: self.view.frame.width, height: 46 + height + 16)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.typePost == .startup {
            let cell = self.collectionViewInput.dequeueReusableCell(withReuseIdentifier: "StartupCell", for: indexPath) as! StartupViewInput
            cell.delegate = self
            cell.setTextForStartupPost(post: postStartup!)
            cell.setImageForPost(images: self.arrImage)
            return cell
        } else if self.typePost == .event {
            let cell = self.collectionViewInput.dequeueReusableCell(withReuseIdentifier: "EventCell", for: indexPath) as! EventViewInput
            cell.viewConrtoller = self
            cell.delegate = self
            cell.setupHeightImageCover(images: self.arrImage)
            cell.setDataForInput()
            return cell
        } else {
            let cell = self.collectionViewInput.dequeueReusableCell(withReuseIdentifier: "ExperienceCell", for: indexPath) as! ExperienceViewInput
            cell.delegate = self
            cell.setTextForExperiencePost(post: postExperience!)
            cell.setImageForPost(images: self.arrImage)
            return cell
        }
    }
}

extension PostViewController: StartupViewInputDelegate {
    
    func openInputViewControllerFromStartup(type: String) {
        self.openInputView(type: type)
    }
    
    func openImagePickerViewController() {
        let imagePicker = TZImagePickerController(maxImagesCount: 9, columnNumber: 3, delegate: self)
        imagePicker?.allowPickingOriginalPhoto = false
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func deleteImagePost(index: Int) {
        self.arrImage.remove(at: index)
        self.collectionViewInput.reloadData()
    }
}

extension PostViewController: EventViewInputDelegate {
    func openImagePickerForEvent() {
        self.openImagePicker()
    }
    
    func openInputViewControllerFormEvent(type: String) {
        self.openInputView(type: type)
    }
}

extension PostViewController: ExperienceViewInputDelegate {
    func openImagePickerFromExperience() {
        self.openImagePicker()
    }
    
    func openInputViewControllerFormExperience(type: String) {
        self.openInputView(type: type)
    }
}

extension PostViewController: TZImagePickerControllerDelegate {
    func imagePickerController(_ picker: TZImagePickerController!, didFinishPickingPhotos photos: [UIImage]!, sourceAssets assets: [Any]!, isSelectOriginalPhoto: Bool) {
        self.arrImage += photos
        self.collectionViewInput.reloadData()
    }
}

extension PostViewController: FCAlertViewDelegate {
    func fcAlertView(_ alertView: FCAlertView!, clickedButtonIndex index: Int, buttonTitle title: String!) {
        
        if title == "Xóa" {
            self.dismissKeyboard()
            self.dismissPostViewController()
        }
    }
    
    func dismissPostViewController() {
        self.deletePost(type: self.typePost)
        self.arrImage = [UIImage]()
        self.tabBarController?.selectedIndex = Helper.sharedInstance.getTabBarSelected(typeView: tabBarSelected)
    }
}

extension PostViewController: FusumaDelegate {
    
    func fusumaImageSelected(_ image: UIImage) {
        var images = [UIImage]()
        images.append(image)
        self.arrImage = images
        self.collectionViewInput.reloadData()
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
    }
    
    func fusumaDismissedWithImage(_ image: UIImage) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
    }
    
    func fusumaClosed() {
        
    }
}

