//
//  NewMessageViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/27/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class NewMessageViewController: BaseViewController {

    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let dismissButton = UIButton(type: .system)
    
    var arrUser: [User] = [User]()
    var page: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Tin nhắn mới"
        
        self.setupLeftBarButton()
        
        let nib = UINib(nibName: "NewMessageViewCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "NewMessageCell")
        
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData() {
        let id = self.appDelegate.user?.id
        APIClient.sharedInstance.getFollowing(id!, page, completionHandle: { (success, users) in
            if success {
                self.arrUser = users
                self.collectionView.reloadData()
            } else {
                
            }
        }) { (error) in
            print(error)
        }
    }
    
    func setupLeftBarButton() {
        self.dismissButton.setImage(#imageLiteral(resourceName: "ic_back"), for: .normal)
        self.dismissButton.frame = CGRect(x: 0, y: 0, width: 54, height: 34)
        self.dismissButton.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -40, bottom: 0, right: 0)
        self.dismissButton.addTarget(self, action: #selector(CommentViewController.handleDismiss), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.dismissButton)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func handleDismiss() {
        _ = self.navigationController?.popViewController(animated: true)
    }

}

extension NewMessageViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrUser.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width, height: 56.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "NewMessageCell", for: indexPath) as! NewMessageViewCell
        cell.setupUserForCell(user: self.arrUser[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dbSB = UIStoryboard(name: "Dashboard", bundle: nil)
        let chatVC = dbSB.instantiateViewController(withIdentifier: "ChatViewControllerId") as! ChatViewController
        chatVC.user = self.arrUser[indexPath.row]
        chatVC.isSetupGroup = true
        chatVC.arrIDUser = [(self.appDelegate.user?.id)!, (self.arrUser[indexPath.row].id)!]
        self.navigationController?.pushViewController(chatVC, animated: true)
        
//        self.navigationController?.push(chatVC, animated: true, completion: {
//            _ = self.navigationController?.popViewController(animated: false)
//        })
        
    }
}
