//
//  FollowingViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/6/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import CarbonKit

class FollowingNewsFeedViewController: BaseViewController, UIScrollViewDelegate {
    
    var collectionView: UICollectionView!
    
    var arrPost = [Post]()
    var arrLiked = [Bool]()
    var arrSize = [CGSize]()
    var page: Int = 1
    
    private var numberOfItemsPerSection = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
//        self.arrImg.append(self.img3)

        self.setupCollectionView()
        
        Helper.sharedInstance.setupCellPostForCollectionView(collectionView: self.collectionView)
        
        self.collectionView.showsVerticalScrollIndicator = false

        self.loadData(page: self.page)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FollowingNewsFeedViewController.reloadData), name: NSNotification.Name(rawValue: tReloadFollowing), object: nil)
        
        self.collectionView.loadControl = UILoadControl(target: self, action: #selector(FollowingNewsFeedViewController.loadMore(sender:)))
        self.collectionView.loadControl?.heightLimit = 80.0
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: tReloadFollowing), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.typeViewPost = .following
    }
    
    func setupCollectionView() {
        
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 44 - 49 - 36 - 20), collectionViewLayout: UICollectionViewFlowLayout())
        self.collectionView.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        self.view.addSubview(self.collectionView)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.setupRefreshCollectionView(view: self.view, scrollView: self.collectionView, marginTop: 0.0)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.loadControl?.update()
    }
    
    func loadData(page: Int) {
        let id = self.appDelegate.user?.id
        self.baseRefresh.startRefreshing()
        APIClient.sharedInstance.getPost(id: id!, page: page, completionHandle: { (success, arrPost, likeds) in
            
            self.arrPost = arrPost
            self.arrLiked = likeds
            self.arrSize = Helper.sharedInstance.setHeightForPostCell(posts: arrPost)
            self.collectionView.reloadData()
            self.baseRefresh.endRefreshing()
        }) { (error) in
            print(error)
        }
    }
    
    func reloadData() {
        self.page = 1
        self.loadData(page: self.page)
    }
    
    func loadMore(sender: AnyObject?) {
        self.page += 1
        let id = self.appDelegate.user?.id
        self.baseRefresh.startRefreshing()
        APIClient.sharedInstance.getPost(id: id!, page: page, completionHandle: { (success, arrPost, likeds) in
            
            self.arrPost += arrPost
            self.arrLiked += likeds
            self.arrSize += Helper.sharedInstance.setHeightForPostCell(posts: arrPost)
            self.collectionView.loadControl?.endLoading()
            self.collectionView.reloadData()
            self.baseRefresh.endRefreshing()
        }) { (error) in
            print(error)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FollowingNewsFeedViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.arrSize[indexPath.item]
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, sizeEdgeInsetsMakePost, 8, sizeEdgeInsetsMakePost)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = Helper.sharedInstance.setCellForPost(viewController: self, collectionView: self.collectionView, posts: self.arrPost, likes: self.arrLiked, indexPath: indexPath as NSIndexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.arrPost[indexPath.item].type == "startup" {
            StoryboardManager.sharedInstance.pushViewDetail(post: self.arrPost[indexPath.item], liked: self.arrLiked[indexPath.item], viewController: self)
        }
        if self.arrPost[indexPath.item].type == "event" {
            StoryboardManager.sharedInstance.pushViewDetail(post: self.arrPost[indexPath.item], liked: self.arrLiked[indexPath.item], viewController: self)
        }
    }
    
}


extension FollowingNewsFeedViewController: StartupPostViewCellDelegate {
    
    func openInputReviewFromStartup(indexPath: NSIndexPath) {
        let inputView = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "InputReviewViewControllerIdentifier", storyboardName: "Dashboard") as! InputReviewViewController
        inputView.hidesBottomBarWhenPushed = true
        inputView.idPost = self.arrPost[indexPath.item].id
        self.navigationController?.pushViewController(inputView, animated: true)
    }

    
    func openDetailAtIndex(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewDetail(post: self.arrPost[indexPath.item], liked: self.arrLiked[indexPath.item], viewController: self)
    }
    
    func openCommentAtIndexFromStartup(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewComment(post: self.arrPost[indexPath.item], viewController: self)
    }
    
    func openViewProfileFromStartup(indexPath: NSIndexPath) {
        let user = self.arrPost[indexPath.item].user
        if user?.id != self.appDelegate.user?.id {
            let vcPerson = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "PersonViewControllerIdentifier", storyboardName: "Dashboard") as! PersonViewController
            vcPerson.user = user!
            self.navigationController?.pushViewController(vcPerson, animated: true)
        } else {
            StoryboardManager.sharedInstance.pushViewProfile(viewController: self)
        }
    }
}

extension FollowingNewsFeedViewController: ExperiencePostCellDelegate {
    func openCommentAtIndexFromExperience(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewComment(post: self.arrPost[indexPath.item], viewController: self)
    }
}

extension FollowingNewsFeedViewController: EventPostViewCellDelegate {
    
    func openViewPeopleJoin(indexPath: NSIndexPath) {
        //UsersViewControllerIdentifier
        let vcUsers = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "UsersViewControllerIdentifier", storyboardName: "Dashboard") as! UsersViewController
        self.addChildViewController(vcUsers)
        vcUsers.view.frame = self.view.frame
        self.view.addSubview(vcUsers.view)
        vcUsers.didMove(toParentViewController: self)
    }
}



