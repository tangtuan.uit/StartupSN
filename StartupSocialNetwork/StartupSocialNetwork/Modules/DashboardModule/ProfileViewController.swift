//
//  ProfileViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/1/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    @IBOutlet weak var collectionViewProfile: UICollectionView!
    
    var transferView: String!
    
    let dismissButton = UIButton(type: .system)
    
    var arrPost: [Post] = [Post]()
    var arrLike: [Bool] = [Bool]()
    var sizes: [CGSize] = [CGSize]()
    
    var isTab: Bool = true
    
    var page: Int = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nidHeader = UINib(nibName: "HeaderProfileView", bundle: nil)
        self.collectionViewProfile.register(nidHeader, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderProfileViewId")
        
        self.collectionViewProfile.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")

        self.setupNavigation()
        
        //self.navigationController?.navigationBar.lt_setBackgroundColor(UIColor.clear)
        
        self.setupRefreshCollectionView(view: self.view, scrollView: self.collectionViewProfile, marginTop: 64.0)
        
        self.collectionViewProfile.backgroundColor = UIColor(white: 0.95, alpha: 1)
        
        Helper.sharedInstance.setupCellPostForCollectionView(collectionView: self.collectionViewProfile)
        
        APIClient.sharedInstance.getPostFromUser(id: (self.appDelegate.user?.id)!, page: page, user: self.appDelegate.user!, completionHandle: { (success, posts, likeds) in
            
            self.arrPost = posts
            self.arrLike = likeds
            self.sizes = Helper.sharedInstance.setHeightForPostCell(posts: posts)
            self.collectionViewProfile.reloadData()
            
        }) { (error) in
            print(error)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarSelected = .profile
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let vcOption = segue.destination as! OptionsViewController
        vcOption.hidesBottomBarWhenPushed = true
    }
    
    
    func setupLeftBarButton() {
        self.dismissButton.setImage(#imageLiteral(resourceName: "ic_back"), for: .normal)
//        self.dismissButton.backgroundColor = .darkGray
        self.dismissButton.frame = CGRect(x: 0, y: 0, width: 20, height: 34)
        self.dismissButton.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -4, bottom: 0, right: 0)
        self.dismissButton.addTarget(self, action: #selector(ProfileViewController.handleDismiss), for: .touchUpInside)
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.dismissButton)
    }
    
    func setupNavigation() {

        let colorBorder = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.25)
        let colorText = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        // set left bar button item
        let buttonSearch = UIButton(type: .system)
        if self.isTab {
            buttonSearch.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 72, height: 30)
        } else {
            buttonSearch.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 96, height: 30)
        }
        buttonSearch.setImage(#imageLiteral(resourceName: "ic_search "), for: .normal)
        buttonSearch.contentHorizontalAlignment = .left
        let attribute = [ NSForegroundColorAttributeName: colorText, NSFontAttributeName: UIFont.systemFont(ofSize: 16)]
        let titleButton = NSMutableAttributedString(string: "Search", attributes: attribute)
        buttonSearch.setAttributedTitle(titleButton, for: .normal)
        buttonSearch.imageView?.contentMode = .scaleAspectFit
        buttonSearch.imageEdgeInsets = UIEdgeInsetsMake(6, 0, 6, 6)
        //buttonBack.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -16, bottom: 0, right: 0)
        buttonSearch.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 1, 0)
        buttonSearch.addTarget(self, action: #selector(ProfileViewController.handleButtonSearch), for: .touchUpInside)
        buttonSearch.addBorder(side: .Bottom, color: colorBorder, width: 1)
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: buttonSearch)
        
        if self.isTab {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: buttonSearch)
        } else {
            self.setupLeftBarButton()
            self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: self.dismissButton), UIBarButtonItem(customView: buttonSearch)]
        }
        
        // set right bar button item
        let buttonOptions = UIButton(type: .system)
        buttonOptions.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonOptions.setImage(#imageLiteral(resourceName: "ic_more"), for: .normal)
        buttonOptions.addTarget(self, action: #selector(ProfileViewController.handleButtonOptions), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: buttonOptions)
    }

    func handleButtonSearch() {
        let vcSearch: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "SearchViewControllerIdentifier", storyboardName: "Dashboard")
        vcSearch.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vcSearch, animated: false)
    }
    
    func handleButtonOptions() {
//        self.performSegue(withIdentifier: "SegueOptionsViewController", sender: nil)
        KeyChain.removeKeyChainStore()
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleDismiss() {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension ProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = self.collectionViewProfile.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderProfileViewId", for: indexPath) as! HeaderProfileView
        header.delegate = self
        header.setupHeaderForUser(user: self.appDelegate.user!)
        return header
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        
//        return CGSize(width: self.view.frame.width, height: Helper.sharedInstance.setHeigthViewPhotosProfile() + 384.0 )
//    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 8, 8, 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.sizes[indexPath.item]
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = Helper.sharedInstance.setCellForPost(viewController: self, collectionView: self.collectionViewProfile, posts: self.arrPost, likes: self.arrLike, indexPath: indexPath as NSIndexPath)
        return cell
    }
    
}

extension ProfileViewController: HeaderProfileViewDelegate {
    func openUpdateProfileViewController() {
        let vc: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "UpdateViewControllerIdentifier", storyboardName: "Dashboard")
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}








