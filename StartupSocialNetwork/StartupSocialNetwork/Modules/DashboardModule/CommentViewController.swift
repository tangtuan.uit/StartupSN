//
//  CommentViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/22/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import NextGrowingTextView
import Mantle

class CommentViewController: BaseViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var collectionViewComment: UICollectionView!
    @IBOutlet weak var bottomViewInput: NSLayoutConstraint!
    @IBOutlet weak var textViewInput: RSKGrowingTextView!
    
    let dismissButton = UIButton(type: .system)
    
    var post: Post!
    var arrComment = [Comment]()
    
    let fk = FollowKeyboard()
    
    var page: Int = 1
    
    private var isVisibleKeyboard = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.tabBarController?.tabBar.isHidden = true

        let nibComment = UINib(nibName: "CommentViewCell", bundle: nil)
        self.collectionViewComment.register(nibComment, forCellWithReuseIdentifier: "CommentCell")
        
        self.setupLeftBarButton()
        
        self.setupLongPressGestureRecognizer()
        
        let usernameJSON = [
            "email": self.appDelegate.user?.email,
            "id_post": self.post.id
        ]
        self.appDelegate.socket.emit("join-comment", usernameJSON)
        
        APIClient.sharedInstance.getComment(self.post.id!, page, completionHandle: { (success, comments) in
            if (success) {
                self.arrComment = comments
                self.collectionViewComment.reloadData()
            } else {
                
            }
        }) { (error) in
            print(error)
        }
        self.listenComment()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fk.followKeyboard(withAnimations: { (keyboardFrame, duration, type) in
            
            switch type {
            case .Show:
                self.bottomViewInput.constant = keyboardFrame.height
                self.view.layoutIfNeeded()
            case .Hide:
                self.bottomViewInput.constant = 0
                self.view.layoutIfNeeded()
            }
            
        }) { (finished) in
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.fk.unfollowKeyboard()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupLeftBarButton() {
        
//        let titleButton = NSAttributedString(string: "Cancel", attributes: [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 16)])
//        self.dismissButton.setAttributedTitle(titleButton, for: .normal)
        //cancelButton.setTitle("Cancel", for: .normal)
        self.dismissButton.setImage(#imageLiteral(resourceName: "ic_back"), for: .normal)
        self.dismissButton.frame = CGRect(x: 0, y: 0, width: 54, height: 34)
        self.dismissButton.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -32, bottom: 0, right: 0)
        self.dismissButton.addTarget(self, action: #selector(CommentViewController.handleDismiss), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.dismissButton)
    }
    
    @IBAction func handleTapGestureRecognizer(_ sender: Any) {
        self.textViewInput.resignFirstResponder()
    }

    @IBAction func handleSent(_ sender: Any) {
        if !self.textViewInput.isEmpty {
            let comment = Comment()
            comment?.post = self.post.id
            comment?.user = self.appDelegate.user
            comment?.content = self.textViewInput.text
            
            self.arrComment.append(comment!)
            self.collectionViewComment.reloadData()
            
            self.textViewInput.text = nil
            self.textViewInput.resignFirstResponder()
            
            let indexPath = NSIndexPath(item: self.arrComment.count - 1, section: 0)
            self.collectionViewComment.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: false)
            
            APIClient.sharedInstance.postCommet(comment!, completionHandle: { (success, result) in
                
                if success {
                    self.arrComment[self.arrComment.count - 1] = result
                    self.collectionViewComment.reloadData()
                    
                    let data: Any = [
                        "comment": [
                            "id": result.id,
                            "id_post": result.post,
                            "id_user": result.user?.id,
                            "content": result.content,
                            "create_date": result.time
                        ],
                        "user": [
                            "image": result.user?.image,
                            "email": result.user?.email,
                            "name": result.user?.name
                        ]
                    ]
                    
                    self.appDelegate.socket.emit("post-comment", with: [data])
                } else {
                    
                }
                
            }, failure: { (error) in
                print(error)
            })
        }
    }
    
    func listenComment() {
        self.appDelegate.socket.on("server-send-comment") { (data, ack) in
            var comment: Comment = Comment()
            let json = data[0] as AnyObject
            let commentObject = json.value(forKey: kComment) as! Dictionary<String,AnyObject>
            do {
                comment = try MTLJSONAdapter.model(of: Comment.self, fromJSONDictionary: commentObject) as! Comment
            } catch let err as NSError {
                print(err)
            }
            
            let userObject = json.value(forKey: kUser) as! Dictionary<String,AnyObject>
            do {
                let user = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as! User
                comment.user = user
            } catch let err as NSError {
                print(err)
            }
            
            self.arrComment.append(comment)
            self.collectionViewComment.reloadData()
        }
    }
    
    func handleDismiss() {
        let commentJSON = [
            "id_post": self.post.id
        ]
        self.appDelegate.socket.emit("dismiss-comment", commentJSON)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func setupLongPressGestureRecognizer() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(CommentViewController.longPress(_:)))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.collectionViewComment.addGestureRecognizer(longPressGesture)
    }
    
    func longPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            
            let touchPoint = longPressGestureRecognizer.location(in: self.view)
            if let index = self.collectionViewComment.indexPathForItem(at: touchPoint) {
                
                print(index.item)
            }
        }
    }
}

extension CommentViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrComment.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, sizeEdgeInsetsMake, 0, sizeEdgeInsetsMake)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = self.arrComment[indexPath.item].content
        let height = Helper.sharedInstance.getHeightTextForPost(width: widthDynamicText + 16 - 64, text: text!, sizeFont: 13)
        
        if (height + 48) <= 64 {
            return CGSize(width: widthCellCollectionView, height: 64)
        } else {
            return CGSize(width: widthCellCollectionView, height: height + 48)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionViewComment.dequeueReusableCell(withReuseIdentifier: "CommentCell", for: indexPath) as! CommentViewCell
        cell.setupCommentForCell(comment: self.arrComment[indexPath.item])
        return cell
    }
    
}
