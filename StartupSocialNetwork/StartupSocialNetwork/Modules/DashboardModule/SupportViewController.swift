//
//  SupportViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/6/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class ServiceViewController: BaseViewController {

    var collectionViewService: UICollectionView!
    
    var arrPost = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupCollectionView()
        
        let nibPost = UINib(nibName: "ServicePostCell", bundle: nil)
        self.collectionViewService.register(nibPost, forCellWithReuseIdentifier: "ServiceCell")
        
        self.loadData()
        
        self.collectionViewService.showsVerticalScrollIndicator = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.typeViewPost = .service
    }
    
    func setupCollectionView() {
        
        self.collectionViewService = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 44 - 49 - 36 - 20), collectionViewLayout: UICollectionViewFlowLayout())
        self.collectionViewService.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        self.view.addSubview(self.collectionViewService)
        
        self.collectionViewService.delegate = self
        self.collectionViewService.dataSource = self
        
        self.setupRefreshCollectionView(view: self.view, scrollView: self.collectionViewService, marginTop: 0.0)
    }
    
    func loadData() {
        let id = self.appDelegate.user?.id
        self.baseRefresh.startRefreshing()
        APIClient.sharedInstance.getPostService(id: id!, page: 1, completionHandle: { (success, arrPost, likeds) in
            
            self.arrPost = arrPost
            self.collectionViewService.reloadData()
            self.baseRefresh.endRefreshing()
        }) { (error) in
            print(error)
        }
    }

}

extension ServiceViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let post = self.arrPost[indexPath.item] as! Service
        let height = Helper.sharedInstance.getHeightTextForPost(width: widthDynamicTextPost, text: post.describeService!, sizeFont: 14)
        if height <= 136 {
            return CGSize(width: widthCellCollectionViewPost, height: height + 448)
        } else {
            return CGSize(width: widthCellCollectionViewPost, height: 136 + 448 + 17)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, sizeEdgeInsetsMakePost, 8, sizeEdgeInsetsMakePost)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionViewService.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath) as! ServicePostCell
        //            cell.currentIndexPath = indexPath as NSIndexPath!
        //            cell.delegate = self
        cell.setupViewServicePost(post: self.arrPost[indexPath.item])
        cell.setupSliderShow(arrImage: arrPost[indexPath.item].images!)
        cell.viewFullScreen = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.item)
    }
}

