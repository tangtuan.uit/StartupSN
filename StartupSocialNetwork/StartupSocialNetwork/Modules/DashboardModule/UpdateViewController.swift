//
//  UpdateProfileViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/7/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import Fusuma

class UpdateViewController: BaseViewController, UITextFieldDelegate, WWCalendarTimeSelectorProtocol {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageCover: UIImageView!
    @IBOutlet weak var imageAvatar: UIImageView!
    
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!

    @IBOutlet weak var buttonBirthDay: UIButton!
    @IBOutlet weak var textFieldArea: UITextField!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var buttonGender: UIButton!
    @IBOutlet weak var labelAbout: UILabel!
    
    var image: UIImage?
    var birthDay: String?
    var gender: Bool = true
    
    let cancelButton = UIButton(type: .system)
    let doneButton = UIButton(type: .system)
    
    let arrCategory = ["Nam", "Nữ"]
    
    private lazy var toolbar: FormToolbar = {
        return FormToolbar(inputs: self.inputs)
    }()
    
    private var inputs: [FormInput] {
        return [self.textFieldName as FormInput, self.textFieldEmail as FormInput, self.textFieldPhone as FormInput,  self.textFieldArea as FormInput]
    }
    
    private weak var activeInput: FormInput?
    
    fileprivate var singleDate: Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLeftBarButton()
        self.setupRightBarButton()
        self.setupTouchImage()
        self.setupLabelAbout()
        self.configDownMenu()
        self.setUserForField()
        

        self.imageAvatar.setCircularImage()
        
        self.navigationController?.navigationBar.isHidden = false
        
        self.view.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        self.textFieldName.delegate = self
        self.textFieldEmail.delegate = self
        self.textFieldPhone.delegate = self
        self.textFieldArea.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        self.toolbar = FormToolbar(inputs: [self.textFieldName, self.textFieldEmail, self.textFieldPhone, self.textFieldArea])
        
        if self.appDelegate.user?.image != nil {
            let url =  Helper.sharedInstance.setUrlForImage(path: (self.appDelegate.user?.image)!)
            self.imageAvatar.sd_setImage(with: url as URL!)
            self.imageCover.sd_setImage(with: url as URL!)
        }
        
        self.imageCover.addBlurEffect()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Helper.sharedInstance.setTextForFieldStartup(text: self.appDelegate.user?.about, title: kTitleAbout, colorTitle: UIColor.black, label: self.labelAbout)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupLeftBarButton() {
        
        let titleButton = NSAttributedString(string: "Cancel", attributes: [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 16)])
        self.cancelButton.setAttributedTitle(titleButton, for: .normal)
        self.cancelButton.frame = CGRect(x: 0, y: 0, width: 54, height: 34)
        self.cancelButton.addTarget(self, action: #selector(UpdateViewController.handleButtonCancel), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.cancelButton)
    }
    
    func setupRightBarButton() {
        
        let titleButton = NSAttributedString(string: "Done", attributes: [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 16)])
        self.doneButton.setAttributedTitle(titleButton, for: .normal)
        self.doneButton.frame = CGRect(x: 0, y: 0, width: 54, height: 34)
        self.doneButton.addTarget(self, action: #selector(UpdateViewController.handleButtonDone), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.doneButton)
    }
    
    func setupTouchImage() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UpdateViewController.handleInputImageTap))
        self.imageAvatar.isUserInteractionEnabled = true
        self.imageAvatar.addGestureRecognizer(tap)
    }
    
    func setupLabelAbout() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UpdateViewController.handleLabelAboutTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelAbout.isUserInteractionEnabled = true
        self.labelAbout.addGestureRecognizer(gestureRecognizer)
        
    }
    
    func configDownMenu() {
        let config = FTConfiguration.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor.lightGray
        config.menuSeparatorColor = UIColor.lightGray
        config.textAlignment = .center
        config.textFont = UIFont.systemFont(ofSize: 14)
        config.menuRowHeight = 36
        config.menuWidth = 64
        config.cornerRadius = 6
    }
    
    func setUserForField() {
        
        self.textFieldName.borderStyle = .none
        self.textFieldName.text = self.appDelegate.user?.name
        
        self.textFieldEmail.borderStyle = .none
        self.textFieldEmail.text = self.appDelegate.user?.email
        
        self.textFieldPhone.borderStyle = .none
        self.textFieldPhone.text = self.appDelegate.user?.phone
        
        self.textFieldArea.borderStyle = .none
        self.textFieldArea.text = self.appDelegate.user?.area
        
        if self.appDelegate.user?.birthday != nil {
            let userBirthDay = Helper.sharedInstance.stringToDate(date: (self.appDelegate.user?.birthday)!)
            self.buttonBirthDay.setTitle(userBirthDay.birthDayDisplay(), for: .normal)
        }

        if (self.appDelegate.user?.gender)! {
            self.buttonGender.setTitle("Nam", for: .normal)
        } else {
            self.buttonGender.setTitle("Nữ", for: .normal)
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        toolbar.update()
        activeInput = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        toolbar.goForward()
        return true
    }
    
    @objc func controlDidChange(_ control: UISegmentedControl) {
        toolbar.direction = control.selectedSegmentIndex == 0 ? .leftRight : .upDown
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 16.0, right: 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.scrollView.contentInset = .zero
        self.scrollView.scrollIndicatorInsets = .zero
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // Handle event tap button
    func handleButtonCancel() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func handleButtonDone() {
        self.setInfoUser()
        APIClient.sharedInstance.uploadAvatar(self.image, completionHandle: { (success, path) in
            if success {
                self.appDelegate.user?.image = path
                self.callAPIUpdateInfoUser()
            } else {
                self.callAPIUpdateInfoUser()
                print("Upload avatar for user unsuccess!")
            }
        }) { (error) in
            print(error)
        }
        
    }
    
    func handleInputImageTap() {
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        self.present(fusuma, animated: true, completion: nil)
    }
    
    func handleLabelAboutTap() {
        let vcUpdate: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "InputViewControllerIdentifier", storyboardName: "Dashboard")
        self.navigationController?.pushViewController(vcUpdate, animated: false)
    }
    
    @IBAction func handleButtonBirthDay(_ sender: Any) {
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        self.present(selector, animated: false, completion: nil)
    }
    
    @IBAction func handleButtonGender(_ sender: Any) {
        FTPopOverMenu.showForSender(sender: sender as! UIView, with: self.arrCategory, done: { (index) in
            switch index {
            case 0:
                self.buttonGender.setTitle("Nam", for: .normal)
                self.gender = true
            case 1:
                self.buttonGender.setTitle("Nữ", for: .normal)
                self.gender = false
            default:
                break
            }
        }) {
            print("cancel")
        }
    }
    
    func callAPIUpdateInfoUser() {
        APIClient.sharedInstance.update(self.appDelegate.user!, completionHandle: { (success, result) in
            if success {

                Helper.sharedInstance.showMessagesSuccess("Cập nhật thành công.", completionHandle: { (completion) in
                    
                    self.appDelegate.user = result
                    
                })
            } else {
                print("Updare infomation for user unsuccess!")
            }
        }) { (error) in
            print(error)
        }
    }
    
    
    func setInfoUser() {
        if !(self.textFieldName.text?.isEmpty)! {
            self.appDelegate.user?.name = self.textFieldName.text
        }
        if !(self.textFieldEmail.text?.isEmpty)! {
            self.appDelegate.user?.email = self.textFieldEmail.text
        }
        if !(self.textFieldPhone.text?.isEmpty)! {
            self.appDelegate.user?.phone = self.textFieldPhone.text
        }
        if !(self.textFieldArea.text?.isEmpty)! {
            self.appDelegate.user?.area = self.textFieldArea.text
        }
        if self.birthDay != nil {
            self.appDelegate.user?.birthday = self.birthDay
        }
        self.appDelegate.user?.gender = self.gender
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        self.buttonBirthDay.setTitle(date.birthDayDisplay(), for: .normal)
        self.birthDay = Helper.sharedInstance.getiso8601ForDate(date: date)
    }
}

extension UpdateViewController: FusumaDelegate {
    
    func fusumaImageSelected(_ image: UIImage) {
        self.image = image
        self.imageAvatar.image = image
        self.imageCover.image = image
        self.imageCover.addBlurEffect()

    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
        
    }
    
    func fusumaDismissedWithImage(_ image: UIImage) {
        
    }
    
    func fusumaCameraRollUnauthorized() {
        
    }
    
    func fusumaClosed() {
        
    }
}
