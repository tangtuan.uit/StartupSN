//
//  ReviewViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 5/22/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class ReviewViewController: BaseViewController {
    
    var collectionView: UICollectionView!
    
    var arrPost = [Startup]()
    var arrSize = [CGSize]()
    var page: Int = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupCollectionView()
        
        let nibReview = UINib(nibName: "StartupReviewCell", bundle: nil)
        self.collectionView.register(nibReview, forCellWithReuseIdentifier: "cell")
        
        self.loadData(page: self.page)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReviewViewController.reloadData), name: NSNotification.Name(rawValue: tReloadReview), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupCollectionView() {
        
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 44 - 49 - 36 - 20), collectionViewLayout: UICollectionViewFlowLayout())
        self.collectionView.backgroundColor = UIColor(white: 0.9, alpha: 1)
        self.collectionView.showsVerticalScrollIndicator = false
        self.view.addSubview(self.collectionView)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.setupRefreshCollectionView(view: self.view, scrollView: self.collectionView, marginTop: 0.0)
    }
    
    func reloadData() {
        self.page = 1
        self.loadData(page: self.page)
    }
    
    func loadData(page: Int) {
        let id = self.appDelegate.user?.id
        self.baseRefresh.startRefreshing()
        APIClient.sharedInstance.getPostReview(id: id!, page: page, completionHandle: { (success, posts) in
            
            self.arrPost = posts
            self.collectionView.reloadData()
            self.baseRefresh.endRefreshing()
        }) { (error) in
            print(error)
        }
    }
    
//    func setUserForReview(posts: [Post], users: [Users]) -> [Startup] {
//        var startups = [Startup]()
//        for post in posts {
//            let startup = post as! Startup
//            for user in users {
//                startup.review.
//            }
//        }
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ReviewViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let post = self.arrPost[indexPath.item]
        let height = Helper.sharedInstance.getHeightTextForPost(width: widthDynamicTextPost, text: "Describe Startup\n" + post.idea!, sizeFont: 14)
        if (height + 519) > 667 {
            return CGSize(width: widthCellCollectionViewPost, height: 667)
        } else {
            return CGSize(width: widthCellCollectionViewPost, height: height + 519)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, sizeEdgeInsetsMakePost, 8, sizeEdgeInsetsMakePost)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StartupReviewCell
        cell.setupView(isDetail: false, post: self.arrPost[indexPath.item])
        cell.delegate = self
        cell.currentIndexPath = indexPath as NSIndexPath!
        cell.setupSliderShow(arrImage: self.arrPost[indexPath.item].images!)
        cell.viewFullScreen = self
        return cell
    }
    
    
}

extension ReviewViewController: StartupReviewCellDelegate {
    func openViewProfileFromStartup(indexPath: NSIndexPath) {
        let user = self.arrPost[indexPath.item].user
        if user?.id != self.appDelegate.user?.id {
            let vcPerson = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "PersonViewControllerIdentifier", storyboardName: "Dashboard") as! PersonViewController
            vcPerson.user = user!
            self.navigationController?.pushViewController(vcPerson, animated: true)
        } else {
            StoryboardManager.sharedInstance.pushViewProfile(viewController: self)
        }
    }
    
    func openDetailAtIndex(indexPath: NSIndexPath) {
//        StoryboardManager.sharedInstance.pushViewDetail(post: self.arrPost[indexPath.item], liked: self.arrLiked[indexPath.item], viewController: self)
    }
    
    func openCommentAtIndexFromStartup(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewComment(post: self.arrPost[indexPath.item], viewController: self)
    }
}

