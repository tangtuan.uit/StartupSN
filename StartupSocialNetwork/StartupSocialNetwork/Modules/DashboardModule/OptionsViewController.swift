//
//  OptionsViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/2/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class OptionsViewController: BaseViewController {

    @IBOutlet weak var collectionViewOption: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nibOptionCell = UINib(nibName: "OptionViewCell", bundle: nil)
        self.collectionViewOption.register(nibOptionCell, forCellWithReuseIdentifier: "OptionCell")
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OptionsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 66.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionViewOption.dequeueReusableCell(withReuseIdentifier: "OptionCell", for: indexPath) as! OptionViewCell
        cell.setupCellForProfile(user: self.appDelegate.user!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            let vcDashboard: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "UpdateViewControllerIdentifier", storyboardName: "Dashboard")
            self.navigationController?.pushViewController(vcDashboard, animated: true)
        }
    }
    
}
