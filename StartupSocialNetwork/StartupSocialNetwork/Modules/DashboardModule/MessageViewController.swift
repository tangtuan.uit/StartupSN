//
//  MessageViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/23/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import Mantle

class MessageViewController: BaseViewController {
    
    @IBOutlet weak var collectionviewViewMessage: UICollectionView!
    
    let buttonMessage: UIButton = {
        var button = UIButton()
        
        let menuButtonSize: CGSize = CGSize(width: 56.0, height: 56.0)
        button = UIButton(frame: CGRect(origin: CGPoint.zero, size: menuButtonSize))
        button.setImage(UIImage(named: "ic_add_message"), for: UIControlState.normal)
        button.center = CGPoint(x: widthDevice - 36.0, y: heightDevice - 150.0)
        button.addTarget(self, action: #selector(MessageViewController.actionButtonNewMessage), for: UIControlEvents.touchUpInside)
        return button
    }()
    
    var contentOffset: CGFloat = 0
    var arrGroup: [Group] = [Group]()
    var idRoom: Int!
    var page: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Tin nhắn"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        
        let nibMessage = UINib(nibName: "MessageViewCell", bundle: nil)
        self.collectionviewViewMessage.register(nibMessage, forCellWithReuseIdentifier: "cell")
        self.view.addSubview(self.buttonMessage)
        
        self.collectionviewViewMessage.backgroundColor = UIColor(white: 0.9, alpha: 1)
        self.loadMessage()
        self.listenerMessage()
        
        NotificationCenter.default.addObserver(self, selector: #selector(MessageViewController.handleNotificationSentMessage(_:)), name: Notification.Name.sentMessage, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarSelected = .message
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vcChat = segue.destination as! ChatViewController
        vcChat.group = self.arrGroup[self.idRoom]
//        vcChat.arrMessage.append(self.arrGroup[self.idRoom].message!)
        vcChat.user = self.arrGroup[self.idRoom].user
        vcChat.hidesBottomBarWhenPushed = true
    }
    
    func loadMessage() {
        APIClient.sharedInstance.getGroup((self.appDelegate.user?.id)!, page, completionHandle: { (success, groups) in
            self.arrGroup = groups
            self.collectionviewViewMessage.reloadData()
        }) { (error) in
            print(error)
        }
    }
    
    func listenerMessage() {
        self.appDelegate.socket.on("server_send_message") { (data, ack) in
            var group: Group = Group()
            let json = data[0] as AnyObject
            
            let groupObject = json.value(forKey: kGroup) as! Dictionary<String,AnyObject>
            do {
                group = try MTLJSONAdapter.model(of: Group.self, fromJSONDictionary: groupObject) as! Group
                
                let jsonGroup = json.value(forKey: kGroup) as AnyObject
                let messageObject = jsonGroup.value(forKey: kMessage) as! Dictionary<String,AnyObject>
                do {
                    group.message = try MTLJSONAdapter.model(of: Message.self, fromJSONDictionary: messageObject) as? Message
                } catch let error as NSError {
                    print(error)
                }
                
            } catch let error as NSError {
                print(error)
            }
            
            let userObject = json.value(forKey: kUser) as! Dictionary<String,AnyObject>
            do {
                let user = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as? User
                group.user = user
            } catch let error as NSError {
                print(error)
            }
            
            let foundGroup = Helper.sharedInstance.searchGroupChat(groups: self.arrGroup, id: group.id!)
            
            if foundGroup.find {
                self.arrGroup.remove(at: foundGroup.index)
            }
            self.arrGroup.insert(group, at: 0)
            self.collectionviewViewMessage.reloadData()
        }
    }
    
    func handleNotificationSentMessage(_ notification: NSNotification) {
        
    }
    
    func actionButtonNewMessage() {
        let vcNewMessage: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "NewMessageViewControllerIdentifier", storyboardName: "Dashboard") as! NewMessageViewController
        vcNewMessage.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vcNewMessage, animated: true)
    }


}

extension MessageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrGroup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: widthCellCollectionView, height: 66.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, sizeEdgeInsetsMake, 0, sizeEdgeInsetsMake)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionviewViewMessage.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MessageViewCell
        cell.setupGroupForCell(group: self.arrGroup[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.idRoom = indexPath.row
        self.performSegue(withIdentifier: "SegueChatViewControler", sender: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if self.contentOffset < scrollView.contentOffset.y {
            if self.contentOffset > 0 {
                UIView.animate(withDuration: 0.3) {
                    //self.buttonMessage.isHidden = true
                    self.buttonMessage.alpha = 0
                }
            }
        } else {
            if self.contentOffset < maximumOffset {
                UIView.animate(withDuration: 0.3) {
                    //self.buttonMessage.isHidden = true
                    self.buttonMessage.alpha = 1
                }
            }
        }
        self.contentOffset = scrollView.contentOffset.y
    }

}
