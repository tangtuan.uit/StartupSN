//
//  PersonViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 5/5/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class PersonViewController: BaseViewController {
    
    @IBOutlet weak var collectionViewPerson: UICollectionView!

    var user: User = User()
    var posts: [Post] = [Post]()
    var sizes: [CGSize] = [CGSize]()
    var likeds: [Bool] = [Bool]()
    var idFollow: String = ""
    var page: Int = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionViewPerson.backgroundColor = UIColor(white: 0.95, alpha: 1)

        let nidHeader = UINib(nibName: "HeaderPersonView", bundle: nil)
        self.collectionViewPerson.register(nidHeader, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderPersonViewId")
        
        Helper.sharedInstance.setupCellPostForCollectionView(collectionView: self.collectionViewPerson)
        
        APIClient.sharedInstance.getPerson(idUser: (self.appDelegate.user?.id)!, idPerson: self.user.id!, page: page, completionHandle: { (success, user, posts, likeds, follow) in
            
            if (success) {
                self.user = user
                self.idFollow = follow
                self.posts = posts
                self.likeds = likeds
                self.sizes = Helper.sharedInstance.setHeightForPostCell(posts: posts)
                self.collectionViewPerson.reloadData()
            }
            
        }) { (error) in
            print(error)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PersonViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = self.collectionViewPerson.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderPersonViewId", for: indexPath) as! HeaderPersonView
        header.delegate = self
        header.setupHeaderForUser(user: self.user, follow: self.idFollow)
        return header
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.sizes[indexPath.item]
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, sizeEdgeInsetsMakePost, 8, sizeEdgeInsetsMakePost)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = Helper.sharedInstance.setCellForPost(viewController: self, collectionView: self.collectionViewPerson, posts: self.posts, likes: self.likeds, indexPath: indexPath as NSIndexPath)
        return cell
    }
}

extension PersonViewController: HeaderPersonViewDelegate {
    
    func openChatViewController(user: User) {
        let dbSB = UIStoryboard(name: "Dashboard", bundle: nil)
        let chatVC = dbSB.instantiateViewController(withIdentifier: "ChatViewControllerId") as! ChatViewController
        chatVC.user = self.user
        chatVC.isSetupGroup = true
        chatVC.arrIDUser = [(self.appDelegate.user?.id)!, (user.id)!]
        chatVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
}

extension PersonViewController: StartupPostViewCellDelegate {
    func openInputReviewFromStartup(indexPath: NSIndexPath) {
        
    }

    
    func openDetailAtIndex(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewDetail(post: self.posts[indexPath.item], liked: self.likeds[indexPath.item], viewController: self)
    }
    
    func openCommentAtIndexFromStartup(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewComment(post: self.posts[indexPath.item], viewController: self)
    }
    
    func openViewProfileFromStartup(indexPath: NSIndexPath) {
        
    }
    
}

