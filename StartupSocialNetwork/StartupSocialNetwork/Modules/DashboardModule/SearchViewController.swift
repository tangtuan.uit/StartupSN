//
//  SearchViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/28/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class SearchViewController: BaseViewController {
    
    let textFieldSearch = DesignableTextField()
    let cancelButton = UIButton(type: .system)

    var posts = [Post]()
    var likeds = [Bool]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        
        self.setupNavigationBar()
        
        self.textFieldSearch.becomeFirstResponder()
        
        let nib = UINib(nibName: "OptionViewCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "cell")
        
        self.collectionView.backgroundColor = UIColor(white: 0.9, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupNavigationBar() {
    
        self.textFieldSearch.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 92, height: 34)
        self.textFieldSearch.leftImage = #imageLiteral(resourceName: "ic_search ")
        self.textFieldSearch.bottomBorderColor = colorTextFieldPlaceholder
        self.textFieldSearch.topPadding = 1
        self.textFieldSearch.leftPadding = 3
        self.textFieldSearch.tintColor = colorTextFieldPlaceholder
        self.textFieldSearch.font = UIFont.systemFont(ofSize: 16)
        self.textFieldSearch.textColor = UIColor.white
        
        // set placeholder for text field search
        let str = NSAttributedString(string: "Search", attributes: [NSForegroundColorAttributeName: colorTextFieldPlaceholder])
        self.textFieldSearch.attributedPlaceholder = str
        
        self.textFieldSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.textFieldSearch)
        //
        self.setupRightBarButton()


    }
    
    func setupRightBarButton() {
        
        let titleButton = NSAttributedString(string: "Cancel", attributes: [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 16)])
        self.cancelButton.setAttributedTitle(titleButton, for: .normal)
        //cancelButton.setTitle("Cancel", for: .normal)
        self.cancelButton.frame = CGRect(x: 0, y: 0, width: 54, height: 34)
        self.cancelButton.addTarget(self, action: #selector(SearchViewController.handleButtonCancel), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.cancelButton)
    }
    
    func handleButtonCancel() {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        APIClient.sharedInstance.searchPostStartup(id: (user_current?.id)!, text: textField.text!, page: 1, completionHandle: { (success, posts, liked) in
            if success {
                self.posts = posts
                self.likeds = liked
                self.collectionView.reloadData()
                
            }
        }) { (error) in
            print(error)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, sizeEdgeInsetsMakePost, 8, sizeEdgeInsetsMakePost)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: widthCellCollectionViewPost, height: 66.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OptionViewCell
        cell.setupCellForSearchStartup(post: self.posts[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        StoryboardManager.sharedInstance.pushViewDetail(post: self.posts[indexPath.item], liked: self.likeds[indexPath.item], viewController: self)
    }
    
}


