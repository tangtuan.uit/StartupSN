//
//  DetailViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/20/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class DetailViewController: BaseViewController {
    
    var collectionViewDetail: UICollectionView = {
        let f = UICollectionViewFlowLayout()
        f.scrollDirection = UICollectionViewScrollDirection.vertical
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: f)
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    var post: Post!
    var liked: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupCollectionView()
        
        self.collectionViewDetail.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        let nidStartup = UINib(nibName: "StartupPostViewCell", bundle: nil)
        self.collectionViewDetail.register(nidStartup, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "StartupDetailCell")
        
        let nidEvent = UINib(nibName: "EventViewDetail", bundle: nil)
        self.collectionViewDetail.register(nidEvent, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "EventDetailCell")
        
        self.collectionViewDetail.showsVerticalScrollIndicator = false
    }
    
    func setupCollectionView() {
        
        self.view.addSubview(self.collectionViewDetail)
        
        self.collectionViewDetail.dataSource = self
        self.collectionViewDetail.delegate = self
        
        self.view.addConstraint(NSLayoutConstraint(item: self.collectionViewDetail, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.collectionViewDetail, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.collectionViewDetail, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.collectionViewDetail, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if self.post.type == "startup" {
            let header = self.collectionViewDetail.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "StartupDetailCell", for: indexPath) as! StartupPostViewCell
            header.viewFullScreen = self
            header.setupView(isDetail: true, post: self.post, liked: self.liked)
            header.setupSliderShow(arrImage: post.images!)
            header.delegate = self
            header.currentIndexPath = indexPath as NSIndexPath
            return header
        } else {
            let header = self.collectionViewDetail.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "EventDetailCell", for: indexPath) as! EventViewDetail
            header.setupViewForPostEvent(post: post)
            return header
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if self.post.type == "startup" {
            let postStatup = self.post as! Startup
            let text = "Describe Startup:\n" + postStatup.idea! + "\n\nMy Team:\n" + postStatup.team! + "\n\nScope of Activities:\n" + postStatup.fields! + "\n\nUser Object:\n" + postStatup.userObject! + "\n\nOriented Development:\n" + postStatup.orientedDevelopment! + "\n\nDescribe Product:\n" + postStatup.describeProduct!
            return CGSize(width: self.view.frame.width, height: Helper.sharedInstance.getHeightTextForPost(width: self.view.frame.width, text: text, sizeFont: 14) + 423 )
        }
        
        return CGSize(width: self.view.frame.width, height: Helper.sharedInstance.getHeightTextForEventDetail(width: self.view.frame.width, post: self.post) + 345 + 24)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionViewDetail.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        return cell
    }
}

extension DetailViewController: StartupPostViewCellDelegate {
    func openInputReviewFromStartup(indexPath: NSIndexPath) {
        
    }


    func openViewProfileFromStartup(indexPath: NSIndexPath) {
        let user = self.post.user
        if user?.id != self.appDelegate.user?.id {
            let vcPerson = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "PersonViewControllerIdentifier", storyboardName: "Dashboard") as! PersonViewController
            vcPerson.user = user!
            self.navigationController?.pushViewController(vcPerson, animated: true)
        } else {
            StoryboardManager.sharedInstance.pushViewProfile(viewController: self)
        }

    }
    
    func openCommentAtIndexFromStartup(indexPath: NSIndexPath) {
        StoryboardManager.sharedInstance.pushViewComment(post: self.post, viewController: self)
    }
    
    func openDetailAtIndex(indexPath: NSIndexPath) {
        
    }
}

