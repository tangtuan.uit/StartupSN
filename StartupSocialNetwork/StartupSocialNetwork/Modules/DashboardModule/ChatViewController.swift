//
//  ChatViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/23/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import NextGrowingTextView
import Mantle

class ChatViewController: BaseViewController {

    @IBOutlet weak var collectionViewComment: UICollectionView!
    @IBOutlet weak var textViewComment: NextGrowingTextView!
    @IBOutlet weak var inputContainerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var inputContainerView: UIView!
    
    let dismissButton = UIButton(type: .system)
    
    let fk = FollowKeyboard()
    
    var arrMessage = [Message]()
    var arrIDUser: [String]!
    var user: User!
    var group: Group = Group()
    var page: Int = 1
    
    var isSetupGroup: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        
        self.setupLeftBarButton()
        
        self.textViewComment.backgroundColor = UIColor(white: 1, alpha: 1)
        self.textViewComment.textContainerInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.textViewComment.placeholderAttributedText = NSAttributedString(
            string: "Write a comment...",
            attributes: [
                NSFontAttributeName: UIFont.systemFont(ofSize: 14.0),
                NSForegroundColorAttributeName: UIColor.gray
            ])

        self.title = self.user.name
        
        self.collectionViewComment.register(ChatBubbleViewCell.self, forCellWithReuseIdentifier: "cell")
        
        self.setupGroup()
        
        self.listenerMessage()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fk.followKeyboard(withAnimations: { (keyboardFrame, duration, type) in
            
            switch type {
            case .Show:
                self.inputContainerViewBottom.constant = keyboardFrame.height
                self.view.layoutIfNeeded()
                if self.arrMessage.count > 0 {
                    self.setupScrollCollectionView(animated: false)
                }
            case .Hide:
                self.inputContainerViewBottom.constant = 0
                self.view.layoutIfNeeded()
            }
            
        }) { (finished) in
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.fk.unfollowKeyboard()
    }
    
    deinit {
//        NotificationCenter.default.removeObserver(<#T##observer: Any##Any#>, name: <#T##NSNotification.Name?#>, object: <#T##Any?#>)
    }

    
    func setupLeftBarButton() {
        self.dismissButton.setImage(#imageLiteral(resourceName: "ic_back"), for: .normal)
        self.dismissButton.frame = CGRect(x: 0, y: 0, width: 54, height: 34)
        self.dismissButton.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -40, bottom: 0, right: 0)
        self.dismissButton.addTarget(self, action: #selector(ChatViewController.handleDismiss), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: self.dismissButton)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadMessage(id: String) {
        APIClient.sharedInstance.getMessage(id, page, completionHandle: { (success, messages) in
            self.arrMessage = messages
            self.collectionViewComment.reloadData()
            self.setupScrollCollectionView(animated: false)
        }) { (error) in
            print(error)
        }
    }
    
    func handleDismiss() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func setupGroup() {
        if self.isSetupGroup {
            let sortedID = self.arrIDUser.sorted()
            APIClient.sharedInstance.createGroup(sortedID, completionHandle: { (success, group) in
                if (success) {
                    self.group = group
                    self.loadMessage(id: self.group.id!)
                } else {
                    print("Don't find group!")
                }
            }) { (error) in
                print(error)
            }
        } else {
            self.loadMessage(id: self.group.id!)
        }
    }
    
    @IBAction func handleSentMessage(_ sender: Any) {
        let message = Message()
        message?.id_group = self.group.id
        message?.user = self.appDelegate.user
        message?.content = self.textViewComment.text
        
        self.arrMessage.append(message!)
        self.collectionViewComment.reloadData()
        
        self.setupScrollCollectionView(animated: true)
        
        self.textViewComment.text = nil
        
        let chatJSON: Any = [
            "message": [
                "id_group": message?.id_group,
                "id_send": message?.user?.id,
                "content": message?.content
            ],
            "user": [
                "_id": self.appDelegate.user?.id,
                "name": self.appDelegate.user?.name,
                "image": self.appDelegate.user?.image
            ]
        ]
        self.appDelegate.socket.emit("client_send_message", with: [chatJSON])
        
//        NotificationCenter.default.post(name: Notification.Name.sentMessage, object: nil, userInfo: chatJSON as? [AnyHashable : Any])
        
    }
    
    func listenerMessage() {
        self.appDelegate.socket.on("server_send_message") { (data, ack) in
            var message: Message = Message()
            let json = data[0] as AnyObject
            let jsonGroup = json.value(forKey: kGroup) as AnyObject
            let messageObject = jsonGroup.value(forKey: kMessage) as! Dictionary<String,AnyObject>
            do {
                message = try MTLJSONAdapter.model(of: Message.self, fromJSONDictionary: messageObject) as! Message
            } catch let error as NSError {
                print(error)
            }
            
            let userObject = json.value(forKey: kUser) as! Dictionary<String,AnyObject>
            do {
                message.user = try MTLJSONAdapter.model(of: User.self, fromJSONDictionary: userObject) as? User
            } catch let error as NSError {
                print(error)
            }
            
            self.arrMessage.append(message)
            self.collectionViewComment.reloadData()
            
            self.setupScrollCollectionView(animated: true)
        
        }
    }
    
    func setupScrollCollectionView(animated: Bool) {
        if self.arrMessage.count > 0 {
            let indexPath = NSIndexPath(item: self.arrMessage.count - 1, section: 0)
            self.collectionViewComment.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: animated)
        }
    }
    

}

extension ChatViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrMessage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let messageText = self.arrMessage[indexPath.row].content {
            let size = CGSize (width: self.view.frame.width - 120, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
            
            return CGSize(width:self.view.frame.width, height: estimatedFrame.height + 12)
        }
        
        return CGSize(width: self.view.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionViewComment.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ChatBubbleViewCell

        cell.textViewMessage.text = self.arrMessage[indexPath.row].content
        
        if let messageText = self.arrMessage[indexPath.row].content {
            let size = CGSize (width: self.view.frame.width - 120, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
            
            if self.arrMessage[indexPath.row].user?.id != self.appDelegate.user?.id {
                cell.textViewMessage.frame = CGRect(x: 42 + 8 , y: -2, width: estimatedFrame.width, height: estimatedFrame.height + 16)
                cell.textBubbleView.frame = CGRect(x: 42 + 0, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 12)
                cell.profileImageView.frame = CGRect(x: 8, y: estimatedFrame.height + 12 - 26, width: 26, height: 26)
                cell.textBubbleView.backgroundColor = UIColor(white: 0.95, alpha: 1)
                cell.textViewMessage.textColor = UIColor.black
            } else {
                cell.textViewMessage.frame = CGRect(x: self.view.frame.width - estimatedFrame.width - 16 - 16 - 26 + 4 + 4, y: -2, width: estimatedFrame.width + 16 - 4 - 4, height: estimatedFrame.height + 16)
                
                cell.textBubbleView.frame = CGRect(x: self.view.frame.width - estimatedFrame.width - 16 - 16 - 26, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 12)
                
                cell.profileImageView.frame = CGRect(x: self.view.frame.width - 34, y: estimatedFrame.height + 12 - 26, width: 26, height: 26)
                cell.textViewMessage.textColor = UIColor.white
                cell.textBubbleView.backgroundColor = UIColor(red: 60/255, green: 179/255, blue: 113/255, alpha: 1)
            }
            if self.arrMessage[indexPath.row].user?.image != nil {
                cell.profileImageView.sd_setImage(with: Helper.sharedInstance.setUrlForImage(path: (self.arrMessage[indexPath.row].user?.image)!))
            } else {
                cell.profileImageView.image = #imageLiteral(resourceName: "default_avatar")
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 0, 8, 0)
    }
}
