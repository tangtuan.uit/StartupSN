//
//  InputReviewViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 7/5/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class InputReviewViewController: BaseViewController {
    
    @IBOutlet weak var labelScore: UILabel!
    @IBOutlet weak var labelReivew: PaddingLabel!
    
    var idPost: String!
    var vote: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNavigation()
        self.setupLabelReview()
        
        reviewStartup = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Helper.sharedInstance.setTextForFieldStartup(text: reviewStartup, title: kTitleReview, colorTitle: .darkGray, label: self.labelReivew)
    }
    
    func setupNavigation() {
        
        // set left button item
        let buttonDismiss = UIButton(type: .system)
        buttonDismiss.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        buttonDismiss.setImage(#imageLiteral(resourceName: "ic_dismiss").withRenderingMode(.alwaysOriginal), for: .normal)
        buttonDismiss.contentEdgeInsets = UIEdgeInsets.init(top: 0, left: -16, bottom: 0, right: 0)
        buttonDismiss.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0) // t l b r
        buttonDismiss.addTarget(self, action: #selector(InputReviewViewController.handleButtonDismiss), for: .touchUpInside)
        //buttonDismiss.backgroundColor = UIColor.darkGray
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: buttonDismiss)
        
        // set left button item
        let buttonDone = UIButton(type: .system)
        buttonDone.frame = CGRect(x: 0, y: 0, width: 40, height: 36)
        buttonDone.setTitle("Done", for: .normal)
        buttonDone.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        buttonDone.addTarget(self, action: #selector(InputReviewViewController.handleButtonDone), for: .touchUpInside)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: buttonDone)
    }
    
    func setupLabelReview() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(InputReviewViewController.handleLabelTap))
        gestureRecognizer.numberOfTapsRequired = 1
        self.labelReivew.isUserInteractionEnabled = true
        self.labelReivew.addGestureRecognizer(gestureRecognizer)
        
    }
    
    func handleButtonDismiss() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func handleButtonDone() {
        if self.vote != 0 && reviewStartup != "" {
            let review = Review()
            review?.post = self.idPost
            review?.user = self.appDelegate.user
            review?.content = reviewStartup
            review?.vote = self.vote
            
            APIClient.sharedInstance.postReview(review!, completionHandle: { (success) in
                if success {
                    if success {
                        Helper.sharedInstance.showMessagesSuccess("Đăng bài đánh giá thành công!", completionHandle: { (complete) in
                            print("Hide")
                        })
                    } else {
                        
                    }
                } else {
                    print("post review unsuccess")
                }
                
            }, failure: { (error) in
                print(error)
            })
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func handleLabelTap() {
        let inputView = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "InputViewControllerIdentifier", storyboardName: "Dashboard") as! InputViewController
        inputView.typePost = .review
        self.navigationController?.pushViewController(inputView, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func handleButtonVote(_ sender: Any) {
        let alert = FCAlertView()
        alert.delegate = self
        //234.0f/255.0f green:201.0f/255.0f blue:77.0f/255.0f alpha:1.0
        alert.titleColor = UIColor(red: 234/255, green: 201/255, blue: 77/255, alpha: 1.0)
        alert.subTitleColor = alert.flatGray
        alert.titleFont = UIFont.boldSystemFont(ofSize: 18)
        alert.subtitleFont = UIFont.systemFont(ofSize: 16)
        alert.firstButtonBackgroundColor = UIColor(red: 234/255, green: 201/255, blue: 77/255, alpha: 1.0)
        alert.firstButtonTitleColor = .white
        alert.cornerRadius = 8
        alert.dismissOnOutsideTouch = false
        alert.hideSeparatorLineView = true
        alert.hideDoneButton = true
        alert.showAlert(withTitle: "Xếp hạng",
                        withSubtitle: "Bạn có thể đánh giá dự án khởi nghiệp này",
                        withCustomImage: nil,
                        withDoneButtonTitle: nil,
                        andButtons: ["Đánh giá"])
        alert.makeAlertTypeRateStars { (int) in
            self.labelScore.text = String(int)
            self.vote = int
        }
    }
    
    


}

extension InputReviewViewController: FCAlertViewDelegate {
    
}
