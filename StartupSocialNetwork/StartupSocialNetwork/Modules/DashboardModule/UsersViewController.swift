//
//  UsersViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 6/7/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController {

    @IBOutlet weak var labelNumberJoin: UILabel!
    @IBOutlet weak var collectionViewUsers: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nibOptionCell = UINib(nibName: "OptionViewCell", bundle: nil)
        self.collectionViewUsers.register(nibOptionCell, forCellWithReuseIdentifier: "OptionCell")
        
        // Do any additional setup after loading the view.
        self.setupTapView()
        self.showAnimate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTapView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UsersViewController.handleViewTap))
        self.view.addGestureRecognizer(tap)
    }
    
    func handleViewTap() {
        self.removeAnimate()
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.25) { 
            self.view.alpha = 1
            self.view.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
        }) { (finished) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    
    @IBAction func handleClose(_ sender: Any) {
        self.removeAnimate()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UsersViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 24, 0, 24)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width - 64 - 48, height: 66.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionViewUsers.dequeueReusableCell(withReuseIdentifier: "OptionCell", for: indexPath) as! OptionViewCell
//        cell.setupCellForProfile(user: self.appDelegate.user!)
        cell.imageCell.setCircularImage()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
}
