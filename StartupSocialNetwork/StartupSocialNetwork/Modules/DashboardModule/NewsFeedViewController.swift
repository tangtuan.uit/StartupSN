//
//  NewsFeedViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/28/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import CarbonKit

class NewsFeedViewController: BaseViewController {
    
    //let textFieldSearch = DesignableTextField()
    //["Quan tâm","Khởi nghiệp","Sự kiện","Kinh nghiệm","Dịch vụ"]
    var items = [String]()
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.items.append("Quan tâm")
        
        self.title = "Startup"

        if self.appDelegate.user?.state == "expert" {
            self.items.append("Khởi nghiệp")
            self.items.append("Sự kiện")
            self.items.append("Kinh nghiệm")
            self.items.append("Đánh giá")
        } else if self.appDelegate.user?.state == "user" {
            self.items.append("Khởi nghiệp")
            self.items.append("Sự kiện")
            self.items.append("Kinh nghiệm")
            self.items.append("Dịch vụ")
        } else if self.appDelegate.user?.state == "investors" {
            self.items.append("Khởi nghiệp")
            self.items.append("Sự kiện")
            self.items.append("Kinh nghiệm")
            self.items.append("Đánh giá")
        } else if self.appDelegate.user?.state == "service" {
            self.items.append("Khởi nghiệp")
            self.items.append("Dịch vụ")
        }
        
        self.setupNavigationBar()
        
        self.carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: self.items, delegate: self)
        self.carbonTabSwipeNavigation.insert(intoRootViewController: self)
        //let colorSelected = UIColor(red: 221/255, green: 76/255, blue: 87/255, alpha: 1)
        let colorSelected = UIColor(red: 60/255, green: 179/255, blue: 113/255, alpha: 1)
        self.carbonTabSwipeNavigation.setIndicatorColor(colorSelected)
        self.carbonTabSwipeNavigation.setIndicatorHeight(2)
        self.carbonTabSwipeNavigation.setNormalColor(UIColor.black, font: UIFont.boldSystemFont(ofSize: 14))
        self.carbonTabSwipeNavigation.setSelectedColor(colorSelected, font: UIFont.boldSystemFont(ofSize: 14))
        self.carbonTabSwipeNavigation.setTabBarHeight(36)
        self.carbonTabSwipeNavigation.carbonTabSwipeScrollView.bounces = false
        //self.carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        
        self.view.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarSelected = .newsFeed
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
    }

    func setupNavigationBar() {
        
        // set left navigation button
        let buttonSearch = UIButton(type: .custom)
        buttonSearch.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        buttonSearch.setImage(#imageLiteral(resourceName: "ic_search ").withRenderingMode(.alwaysOriginal), for: .normal)
        buttonSearch.addTarget(self, action: #selector(NewsFeedViewController.handleButtonSearch), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: buttonSearch)
        
        let buttonOptions = UIButton(type: .system)
        buttonOptions.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        buttonOptions.setImage(#imageLiteral(resourceName: "ic_chart"), for: .normal)
        buttonOptions.addTarget(self, action: #selector(NewsFeedViewController.handleButtonTop), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: buttonOptions)
    }
    
    func handleButtonSearch() {
        let vcDashboard: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "SearchViewControllerIdentifier", storyboardName: "Dashboard")
        vcDashboard.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vcDashboard, animated: false)
//        self.dismiss(animated: true, completion: nil)

    }
    
    let menuChart = MenuChartView()
    
    func handleButtonTop() {
        self.menuChart.delegate = self
        self.menuChart.handleShowMenuChart()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewsFeedViewController: CarbonTabSwipeNavigationDelegate {
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        let select = Int(index)
        switch self.items[select] {
        case "Quan tâm":
            return FollowingNewsFeedViewController()
        case "Khởi nghiệp":
            return StartupViewController()
        case "Sự kiện":
            return EventsViewController()
        case "Kinh nghiệm":
            return AdvisoryViewController()
        case "Dịch vụ":
            return ServiceViewController()
        default:
            return ReviewViewController()
        }
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        
    }
    
    func barPosition(for carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
        return UIBarPosition.top
    }

}

extension NewsFeedViewController: MenuChartViewDelegate {
    
    func openTrendViewController() {
        let trendVC = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "TrendViewControllerIdentifier", storyboardName: "Dashboard") as! TrendViewController
        trendVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(trendVC, animated: true)
    }
    
    func openTopStartupViewController() {
        let topView = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "TopStartupViewControllerIdentifier", storyboardName: "Dashboard") as! TopStartupViewController
        topView.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(topView, animated: true)
    }
}
