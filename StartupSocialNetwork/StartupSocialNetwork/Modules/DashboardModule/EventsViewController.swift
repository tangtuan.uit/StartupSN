//
//  StartupViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 3/6/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class EventsViewController: BaseViewController {
    
    var collectionViewEvents: UICollectionView!
    
    var arrPost = [Post]()
    var arrLiked = [Bool]()
    var page: Int = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupCollectionView()
        
        let nibPost = UINib(nibName: "EventPostViewCell", bundle: nil)
        self.collectionViewEvents.register(nibPost, forCellWithReuseIdentifier: "EventCell")
        
        self.loadData(page: self.page)
        
        self.collectionViewEvents.showsVerticalScrollIndicator = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData), name: NSNotification.Name(rawValue: tReloadEvent), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.typeViewPost = .event
    }
    
    func setupCollectionView() {
        
        self.collectionViewEvents = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 44 - 49 - 36 - 20), collectionViewLayout: UICollectionViewFlowLayout())
        self.collectionViewEvents.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        self.view.addSubview(self.collectionViewEvents)
        
        self.collectionViewEvents.delegate = self
        self.collectionViewEvents.dataSource = self
        
        self.setupRefreshCollectionView(view: self.view, scrollView: self.collectionViewEvents, marginTop: 0.0)
    }
    
    func reloadData() {
        self.page = 1
        self.loadData(page: self.page)
    }
    
    func loadData(page: Int) {
        let id = self.appDelegate.user?.id
        self.baseRefresh.startRefreshing()
        APIClient.sharedInstance.getPostEvent(id: id!, page: page, completionHandle: { (success, arrPost, likeds) in
            
            self.arrPost = arrPost
            self.arrLiked = likeds
            self.collectionViewEvents.reloadData()
            self.baseRefresh.endRefreshing()
        }) { (error) in
            print(error)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EventsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrPost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: widthCellCollectionViewPost, height: 370 - 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, sizeEdgeInsetsMakePost, 8, sizeEdgeInsetsMakePost)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionViewEvents.dequeueReusableCell(withReuseIdentifier: "EventCell", for: indexPath) as! EventPostViewCell
        cell.delegate = self
        cell.indexPath = indexPath as NSIndexPath
        cell.setupViewEventWithPost(post: self.arrPost[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        StoryboardManager.sharedInstance.pushViewDetail(post: self.arrPost[indexPath.item], liked: self.arrLiked[indexPath.item], viewController: self)
    }
    
}

extension EventsViewController: EventPostViewCellDelegate {
    
    func openViewPeopleJoin(indexPath: NSIndexPath) {
        //UsersViewControllerIdentifier
        let vcUsers = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "UsersViewControllerIdentifier", storyboardName: "Dashboard") as! UsersViewController
        self.addChildViewController(vcUsers)
        vcUsers.view.frame = self.view.frame
        self.view.addSubview(vcUsers.view)
        vcUsers.didMove(toParentViewController: self)
    }
}
