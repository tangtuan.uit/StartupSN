//
//  BaseViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/3/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import SocketIO
import CarbonKit

import UICKeyChainStore

class BaseViewController: UIViewController {
    
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var baseRefresh: CarbonSwipeRefresh!
    
    var typeViewPost: TypeViewPost!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        user_current = self.appDelegate.user

        //Change status bar color
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2)
        }
        
        //self.hideKeyboardWhenTappedAround()
        
//        KeyChain.removeKeyChainStore()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupRefreshCollectionView(view: UIView, scrollView: UIScrollView, marginTop: CGFloat) {
        
        self.baseRefresh = CarbonSwipeRefresh.init(scrollView: scrollView)
        self.baseRefresh.colors = [UIColor(red: 60/255, green: 179/255, blue: 113/255, alpha: 1)]
        self.baseRefresh.addTarget(self, action: #selector(BaseViewController.handleRefresh), for: .valueChanged)
        self.baseRefresh.setMarginTop(marginTop)
        view.addSubview(self.baseRefresh)
        
        
        
    }
    
    func handleRefresh() {
        let textNotification = Helper.sharedInstance.getViewReload(type: self.typeViewPost)
        let when = DispatchTime.now() + 0 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: textNotification), object: nil)
        }
    }
    
    func endRefreshing() {
        self.baseRefresh.endRefreshing()
    }
    
    func connectToServer() {
        self.appDelegate.socket.on("connect", callback: { (data, ack) in
            let usernameJSON = [
                "id": self.appDelegate.user?.id,
                "email": self.appDelegate.user?.email
            ]
            self.appDelegate.socket.emit("id-client", usernameJSON)
        })
        self.appDelegate.socket.connect()
    }

}
