//
//  SplashViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 2/3/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit
import UICKeyChainStore

class SplashViewController: BaseViewController {
    
    @IBOutlet weak var labelText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupText()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.perform(#selector(SplashViewController.checkLogin), with: nil, afterDelay: 1)
    }
    
    func setupText() {
        let myAttrString = NSMutableAttributedString()
        let attTitle = NSMutableAttributedString(string: "Startup", attributes: [ NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.init(name: "Milkshake", size: 56)! ])
        myAttrString.append(attTitle)
        //        let textContent = "Supporting startup community"
        let textContent = "Hỗ trợ cộng đồng khởi nghiệp"
        let attTitleContent = NSMutableAttributedString(string: "\n" + textContent, attributes: [ NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 13)])
        myAttrString.append(attTitleContent)
        self.labelText.attributedText = myAttrString
    }
    
    func checkLogin() {
        if (KeyChain.checkNilKeyChain()) {
            let vcLogin: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "SecurityNavigationIdentifier", storyboardName: "Security")
            self.present(vcLogin, animated: true, completion: nil)
        } else {
            let id = UICKeyChainStore.string(forKey: kIdUser)
            APIClient.sharedInstance.getUser(id!, completionHandle: { (success, user) in
                if success {
                    self.appDelegate.user = user
                    if !KeyChain.checkKeyChainStore(user: user) {
                        KeyChain.saveKeyChainStore(user: user)
                    }
                    self.connectToServer()
                            
                    let vcDashboard: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "DashboardTabbarIdentifier", storyboardName: "Dashboard")
                    self.present(vcDashboard, animated: true, completion: nil)
                } else {
                    
                }
            }, failure: { (error) in
                print(error)
            })
        }
    }
}
