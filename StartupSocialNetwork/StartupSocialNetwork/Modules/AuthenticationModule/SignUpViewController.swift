//
//  SignUpViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/13/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewInputName: AMInputView!
    @IBOutlet weak var viewInputEmail: AMInputView!
    @IBOutlet weak var viewInputPassword: AMInputView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 60.0, right: 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.scrollView.contentInset = .zero
        self.scrollView.scrollIndicatorInsets = .zero
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func handleButtonLogin(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func handleButtonSignUp(_ sender: Any) {
        
        let user = User()
        user?.name = self.viewInputName.textFieldView.text
        user?.email = self.viewInputEmail.textFieldView.text
        let password = self.viewInputPassword.textFieldView.text
        
        APIClient.sharedInstance.signup(user!, password!, completionHandle: { (success, result) in
            
            if success {
                self.appDelegate.user = result
                self.connectToServer()
                self.openDashBoard()
            } else {
                
            }
            
        }) { (error) in
            print(error)
        }
    }
    
    func openDashBoard() {
        let vcDashboard: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "DashboardTabbarIdentifier", storyboardName: "Dashboard")
        self.navigationController?.present(vcDashboard, animated: true, completion: {
            _ = self.navigationController?.popViewController(animated: false)
        })
    }

}
