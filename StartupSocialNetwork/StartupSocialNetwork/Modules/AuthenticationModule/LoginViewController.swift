//
//  LoginViewController.swift
//  StartupSocialNetwork
//
//  Created by Tang Tuan on 4/12/17.
//  Copyright © 2017 Tang Tuan. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldPassword: AMInputView!
    @IBOutlet weak var textFieldUsername: AMInputView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func handleButtonSignUp(_ sender: Any) {
        self.performSegue(withIdentifier: "SegueSignUpViewController", sender: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 60.0, right: 0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.scrollView.contentInset = .zero
        self.scrollView.scrollIndicatorInsets = .zero
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func handleButtonLogin(_ sender: Any) {
        let email = self.textFieldUsername.textFieldView.text
        let password = self.textFieldPassword.textFieldView.text
        
//        let email = "tangtuan.uit@gmail.com"
//        let password = "123456"
        
        APIClient.sharedInstance.login(email!, password!, completionHandle: { (success, result) in
            
            if success {
                
                self.textFieldUsername.textFieldView.text = nil
                self.textFieldPassword.textFieldView.text = nil
                
                self.appDelegate.user = result
                self.connectToServer()
                self.openDashBoard()
            } else {
                
            }
            
        }) { (error) in
            print(error)
        }
    }

    @IBAction func handleForgotPassword(_ sender: Any) {
        
    }
    
    func openDashBoard() {
        let vcDashboard: UIViewController = StoryboardManager.sharedInstance.getViewControllerWithIdentifierFromStoryboard(viewControllerIdentifier: "DashboardTabbarIdentifier", storyboardName: "Dashboard")
        self.present(vcDashboard, animated: true, completion: nil)
    }
}
